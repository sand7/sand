
## Package Base

```` bash
src/sand
````

## functional modules

```` bash
src/sand/$functional_modul/$function.py
src/sand/transformer/pipeline/image.py # pipeline node who transformes images (perspective transformation)
src/sand/transformer/util.py # random functions
src/sand/transformer/focal.py # functions and classes to handle focal lenght
````

to organize the project in functional modules, who represent a node in the
pipeline or some other functionallity like the node registry.
The Module contains the pipeline nodes and all functions and classes who are
only used in this Module.

## util module

```` bash
src/sand/util/$util.py
src/sand/util/color.py # colors for boxes, lines, levels as enums or wrapper functions
src/sand/util/boxes.py # functions to handle boxes
````

All functions and classes who are wide spread used over the project are in
this module.

## registry module

```` bash
src/sand/registry
````

For controlling the pipeline processes and threads we are using in the
publishing part. See doc/architecture.png, the blue block.

## datatypes module

```` bash
src/sand/datatypes/$datatype.py
````

We use many Custom datatypes to type our dataflow. Some are typedefs
(```types.py```),
but we have also many classes to abstract the data.

**example:**
``` aerialmap.py ```

contains the datatype (class) for the aerialmap.

## interfaces module

```` bash
src/sand/interfaces/$interfaces.py
````

We nee interfaces to communicate, isolate, syncronize and configurate the
threads and processes.
