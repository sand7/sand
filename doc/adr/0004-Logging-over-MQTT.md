# 4. Logging over MQTT

Date: 2021-08-12

## Status

Accepted

## Context

* multiple sub-processes -> async log-file
* in future maybe different systemd-starters
* in future maybe multiple systems

## Decision

* Logging-Handler that sends strings over MQTT
* Logging-Listener that collects everything and writes to file and console

## Consequences

* not too much code change as we are using the normal logging package
* possibility to support multiple systems, only important location is the broker
* only see log-messages when the listener is running
* maybe in future one long-running listener completely separate from
  everything else
* log-levels are difficult to support, i.e. suppressing levels
