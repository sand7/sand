# 8. Retire metric feature

Date: 2023-04-13

## Status

Accepted

## Context

The metric feature was necessary and useful when we had a system that we needed
to keep an eye on constantly. But for a lot of reasons this didn't get the
attention that it should have and the feature degraded in quality quite a lot.

Now as open source project we don't see a reason to still keep it around as it
also makes the code quite complicated. If we want to reintroduce this, this
needs time and care that we don't have at the moment.

## Decision

Remove anything concerning the metric feature

## Consequences

* Easier to remove the constraint for always having mqtt around
* Less code to maintain, for which we don't really have good testing
