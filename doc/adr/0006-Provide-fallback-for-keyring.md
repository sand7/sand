# 6. Provide fallback for keyring

Date: 2021-09-24

## Status

Accepted

## Context

Every `poetry` command fails with this error if executed on one of our crane-pcs
in a `ssh`-session:

```sh
$ poetry shell

  DBusErrorResponse

  [org.freedesktop.DBus.Error.UnknownMethod] ('No such interface “org.freedesktop.DBus.Properties” on object at path /org/freedesktop/secrets/collection/login',)

  at ~/.local/share/pypoetry/venv/lib/python3.8/site-packages/jeepney/wrappers.py:214 in unwrap_msg
      210│     This is to be used with replies to method_call messages, which may be
      211│     method_return or error.
      212│     """
      213│     if msg.header.message_type == MessageType.error:
    → 214│         raise DBusErrorResponse(msg)
      215│
      216│     return msg.body
      217│

The following error occurred when trying to handle this error:


  ItemNotFoundException

  Item does not exist!

  at ~/.local/share/pypoetry/venv/lib/python3.8/site-packages/secretstorage/util.py:49 in send_and_get_reply
       45│ 		try:
       46│ 			return self._connection.send_and_get_reply(msg, unwrap=True)
       47│ 		except DBusErrorResponse as resp:
       48│ 			if resp.name in (DBUS_UNKNOWN_METHOD, DBUS_NO_SUCH_OBJECT):
    →  49│ 				raise ItemNotFoundException('Item does not exist!') from resp
       50│ 			elif resp.name in (DBUS_SERVICE_UNKNOWN, DBUS_EXEC_FAILED,
       51│ 			                   DBUS_NO_REPLY):
       52│ 				data = resp.data
       53│ 				if isinstance(data, tuple):

The following error occurred when trying to handle this error:


  PromptDismissedException

  Prompt dismissed.

  at ~/.local/share/pypoetry/venv/lib/python3.8/site-packages/secretstorage/collection.py:145 in create_collection
      141│ 	if len(collection_path) > 1:
      142│ 		return Collection(connection, collection_path, session=session)
      143│ 	dismissed, result = exec_prompt(connection, prompt)
      144│ 	if dismissed:
    → 145│ 		raise PromptDismissedException('Prompt dismissed.')
      146│ 	signature, collection_path = result
      147│ 	assert signature == 'o'
      148│ 	return Collection(connection, collection_path, session=session)
      149│

The following error occurred when trying to handle this error:


  InitError

  Failed to create the collection: Prompt dismissed..

  at ~/.local/share/pypoetry/venv/lib/python3.8/site-packages/keyring/backends/SecretService.py:63 in get_preferred_collection
       59│                 collection = secretstorage.Collection(bus, self.preferred_collection)
       60│             else:
       61│                 collection = secretstorage.get_default_collection(bus)
       62│         except exceptions.SecretStorageException as e:
    →  63│             raise InitError("Failed to create the collection: %s." % e)
       64│         if collection.is_locked():
       65│             collection.unlock()
       66│             if collection.is_locked():  # User dismissed the prompt
       67│                 raise KeyringLocked("Failed to unlock the collection!")
```

## Decision

Needs additional work, probably manually:

`${HOME}/.config/python_keyring/keyringrc.cfg`:
```cfg
[backend]
default-keyring=keyring.backends.fail.Keyring
```

## Consequences

Installation can be more difficult, we might need to add an ansibled solution
for this as its not really obvious what to do.
