# 2. Switch to multiprocessing

Date: 2021-07-15

## Status

Accepted

## Context

* Python can only run one thread in parallel -> GIL
* We need more performance as we have many cameras, which will get even more
* Our new PC is has a thread-ripper which is more geared towards parallel
* We maxed out our HW options on the old PC (i.e. no faster single-core
  processor possible)

## Decision

We will split up our current system into multiple processes. How big or what
they will contain is still up for debate, but because of the data transfer
between processes it will probably be a mixed system with one part that
needs faster data-transfer with high throughput and the other one being able
to work with slower data. (i.e. Publisher)

## Consequences

* We can use all cores
* We need a data-transfer middleware for:
  * Image-Data
  * Box-Data
  * Metrics
* Currently being evaluated:
  * RabbitMQ (AMQP)
  * Mosquitto (MQTT)
  * Combinations with Redis
  * <del>ZeroMQ</del>
