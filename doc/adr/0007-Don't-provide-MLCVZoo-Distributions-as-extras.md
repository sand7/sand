# 7. Don't provide MLCVZoo distributions as extras

Date: 2022-11-25

## Status

Accepted

## Context

`mlcvzoo-yolox>5,<6` specifies that we have to use `pyton==~3.8`. But we and
possible users don't necessarily want to be pinned by us to a quite old version.

We provide `mlcvzoo-yolox` as one of 2 possible neural extra types but in the
source code we really don't use it. Our current code is completely working with
only the base package installed as we only use the API and the actual
instatiation is in a MLCVZoo configuration.

## Decision

We do not provide extras for `yolox` or `mmdetection` anymore.

## Consequences

This might become a nuisance for our users at first if they expect us to still
provide the extras. Therefore the next version has to become a major release
(which it will be anyway). At the current time this will be the version `2.0.0`.

Now our users have to consciously decide for one of the `mlcvzoo` derivatives.

Their `requirements.txt` could look something like this in the future:

```txt
sand>=2.0.0
mlcvzoo-yolox>5,<6
```
