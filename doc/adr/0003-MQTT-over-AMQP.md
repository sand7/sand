# 3. MQTT over AMQP

Date: 2021-07-29

## Status

Accepted

## Context

* AMQP via RabbitMQ for Multiprocessing is complex
* RabbitMQ provides a feature called Streams, which would be what we need, i.
  e. retain last sent value
* We found no python client (or the correct documentation) for this feature
* MQTT has a retain policy
* MQTT in python has a simpler structure and is easier to manage

## Decision

* Implementation via MQTT and eclipse-mosquitto

## Consequences

* implementation is easier
* topic management/distribution requires more intelligence
