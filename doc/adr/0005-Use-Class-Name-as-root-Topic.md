# 5. Use class name as root topic

Date: 2021-08-30

## Status

Accepted

## Context

Specification of topics makes it easier to know what topics you should
subscribe to and how they are named.

## Decision

We will use the class-name of the sender in unaltered writing as the root
topic. The complete definition is as follows:

```
{SenderClass.__name__}/{device or "all"}/{"data" or "metric"}/{"frame" or what you are actually sending}
```

## Consequences

None
