# System Scope and Context

## Content

- [Business Context](#business-context)
- [Technical/Distribution Context](#technicaldistribution-context)
- [External Interfaces](#external-interfaces)

## Business Context

Generally we are a RND-Project. Therefore, production-readiness is not the main
focus and we can take some shortcuts that make a proof-of-concept easier to do.

We are generally developing the soft- and hardware for a German
in-land-terminal. Therefore, our target is the normal middle-european
temperature range and weather.

## Technical/Distribution Context

Referenzkran + PBI SPS/System

## External Interfaces

Besides a debug interface for publishing what the system currently sees, there
are two external interfaces currently necessary.

* backup
* logs
* netz deployment

### System actively provides to others

The crane control, that moves the crane and does stuff like lifting and setting
down containers needs to get an "all-clear" signal from us. Our current
specification is that we send this signal every 1 sec.

### System consumes from others

The crane control needs to provide an interface where the system can get/ask for
the current position of the spreader. We need this position to be able to
correctly put the recognitions of the speader cameras into our map. As the
spreader can move freely there is no fix-point we can use to do this without the
information.
