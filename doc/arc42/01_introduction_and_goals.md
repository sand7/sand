# Introduction and Goals

## Content

- [Requirements Overview](#requirements-overview)
- [Quality Goals](#quality-goals)
- [Stakeholder](#stakeholder)

## Requirements Overview

* be able to detect biomass with high accuracy
* be able to locate the biomass in a map-like space to determine where in
  relation to the camera the person actually is
* use consumer hardware to keep the initial costs of the system low
* detect humans fast enough to be able to react to the location (<1s)
* be modular and flexible enough to be able to adapt to different requirements (
  different cameras, modules, triggers)
* be open-source-able, understandable, usable, extensible by
  open-source-community

biomass: humans, dogs

## Quality Goals

* high accuracy (80<)
* ADR -> Tool für architecture decisions
* boundaries für metrics

## Stakeholder

| Role          | Contact           | E-Mail                                                                            |
| ------------- | ----------------- | --------------------------------------------------------------------------------- |
| Organization  | Michael Herbold   | [herbold@synyx.de](mailto:herbold@synyx.de)                                       |
| Developer     | Maximilian Otten  | [maximilian.otten@iml.fraunhofer.de](mailto:maximilian.otten@iml.fraunhofer.de)   |
| Developer     | David Bauer       | [bauer@synyx.de](mailto:bauer@synyx.de)                                           |
| Developer     | Moritz Sauter     | [sauter@synyx.de](mailto:sauter@synyx.de)                                         |
