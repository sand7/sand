# Solution Strategy

## CI / CD

We have a CI system which will run general code quality and style checks, as
well as tests.

There are plans to also implement a CD system where every change to the code
will be deployed to our crane-demo-system, to ensure early feedback on our
changes.

## Testing

We agreed that we want to have tests for new functionality and also for
bug-fixes that will come up.

## Reviews

To ensure the software quality and distribute the knowledge what the system does
and what generally happens, we only change the code after a review.

## Vor-Ort Strategie

Hardware
echte schnittstellen
Netz/Internet
