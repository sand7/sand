# COLA arc42

**About this template**

This is a COLA-specific, modified version of arc42 (attributed below). For
further information see
[the wiki page](https://projects.contargo.net/projects/softwaredev/wiki/Architekturdokumentation)
.

**About arc42**

arc42, the Template for documentation of software and system architecture.

By Dr. Gernot Starke, Dr. Peter Hruschka and contributors.

Template Revision: 7.0 EN (based on asciidoc), January 2017

© We acknowledge that this document uses material from the arc 42 architecture
template, <http://www.arc42.de>. Created by Dr. Peter Hruschka & Dr. Gernot
Starke.

## Content

- [Introduction and Goals](./01_introduction_and_goals.md)
- [Architecture Constraints](./02_architecture_constraints.md)
- [System Scope and Context](./03_system_scope_and_context.md)
- [Solution Strategy](./04_solution_strategy.md)
- [Building Block View](./05_building_block_view.md)
- [Concepts](./06_concepts.md)
- [Design Decisions](./07_design_decisions.md)
- [Risks](./08_risks.md)
- [Glossary](./09_glossary.md)
