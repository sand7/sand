# Risks

Over the course of the project we identified some sources of potential problems
in a production environment, especially in the planned case of recognizing
humans for safety reasons.

Generally these are risks we recognized, but are not actively mitigating.
Reasons for this are mostly time, and the research focus of the current project.
Nevertheless, we want to document the risks we see for potential follow-up
projects.

## System Choice

Our current system uses Ubuntu 20.04 as operating system with many of the
default user setups to make maintenance easy for ourselves. This led to
automatic security updates being active on the system. One of those security
updates was an update to the nvidia gpu driver (on 2021-01-06) which in turn led
to an incompatibility between the driver and CUDA. It also prevented the access
to the GPU itself. In production this would severely limit what the system can
do with the data.

On 2021-06-10 we decided to completely remove the unattended-upgrades service as
it again broke our nvidia and cuda configuration. Doing this should be combined
with an explicit maintenance strategy but for our research project doing this
now and then is sufficient.

## External Manipulation

On 2021-02-02 we had a person hanging a cloth over one of our lower hanging
cameras. This prevents the camera to see in a way that is not easily
automatically detectable, as no device is technically malfunctioning. We had
some ideas to detect this via change-detection and this changing too much too
fast, but this never left the idea-stage and because of the mentioned research
focus this was never revisited.

## Camera Failures

On 2021-05-21 one of the used cameras failed and got stuck in a bootloop. If
this happens in a production environment, the system needs to be able to
recognize this, shut operations down and inform about the failure. For the
operator of the system it is also advisable to have spare cameras on hand for
easy/faster replacement (also for every other device that can fail).

(Monitoring is here, we recognize failures)
