# Architecture Constraints

## Content

- [Technical Constraints](#technical-constraints)
- [Organizational Constraints](#organizational-constraints)

## Technical Constraints

The system potentially needs a lot of performance. The current system is one PC
with two GPUs. Depending on the final camera count and what we need to do with
these cameras this can potentially be too small of a system. While the PC can
still be upgraded via better components there is the possibility that we also
have a second system and therefore split the work on two PCs. The architecture
needs to support this or at least cannot actively prevent us from having to work
with two nodes.

Another requirement that actually has the potential to make things easier is to
use a tool called "MLCVZoo" that was developed by the Fraunhofer Institute to
make working with object detection or Machine-Learning in general easier.

A big constraint is the usage of consumer hardware. We don't want to have a
setup in the end that costs a lot of money. We want to make it easy to set the
whole system up and get it running with minimal cost.

## Organizational Constraints

As we cannot change the setup on the crane ourselves we are constrained by the
building time and also the shipping time of components. As the project hit the
high time of COVID and also the global chip-shortage we also had some problems
to get more powerful PC parts and also parts in general.

Additionally, we don't develop the parts that actually control the crane
directly ourselves. This will be done by a sub-contractor.

Finally, as every project we have a limited budget to achieve at least a
proof-of-concept. As we are a research project, we don't have to arrive at
something that can be deployed into production right away, but the closer we
are the better.
