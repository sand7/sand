# Glossary

<dl>
  <dt>module</dt>
  <dd>
    As module we generally mean a python module or an element in our pipeline,
    i.e. transformer is a module. It generally fulfills one specific function
    and has a data input and a data output
  </dd>
</dl>
