# Concepts

## Content

- [Persistence](#persistence)
- [Communication and Integration with other IT Systems](#communication-and-integration-with-other-it-systems)
- [Management of the System & Administrability](#management-of-the-system--administrability)
- [Logging, Tracing and Protocols](#logging-tracing-and-protocols)
- [Configurability](#configurability)
- [Parallelization and Threading](#parallelization-and-threading)
- [Scaling and Clustering](#scaling-and-clustering)

## Persistence

One of the first things we needed to do for this project was to start collecting
video data for later evaluation and extraction of training data for a neural net
object detector.

In favor of speed (in development and on the system itself) we chose to save the
raw video datastream on two external 4 TB Drives which we changed regularly
(basically everytime the drives are full). To increase our capacity to save
data (i.e. record longer), we added a converter module which encodes the saved
video-data in the more efficient h264-Codec.

The converter does this completely separate of the main system, to ensure easy
deactivation if the performance impact gets to heavy.

For the video data we currently use a resolution of 2560x1440 which when we
tried it initially was the maximum our system could handle if it's scaled up to
16+ cameras. This is one of the points which we can adapt if we need more
performance.

We record the LIDAR messages directly to a binary file, with a seperator between
those messages.

## Communication and Integration with other IT Systems

__TODO__: Complete when the interface to LASE/PBI is planned

Lego-Kran API

## Management of the System & Administrability

The system gets deployed on a demo-crane that was equipped with one computer and
a multitude of cameras.

We designed the system to generally work with consumer-grade-hardware and more
or less consumer-grade-software. We are currently using a LTS Ubuntu 20.04 as a
base system.

To be able to use our Neural Networks we currently have a hard-dependency on a
functioning Nvidia GPU & CUDA combination. On the demo system we currently have
2 2080Ti with CUDA 11.3 installed.

On the crane we are using a completely encapsulated net with the following
structure:

* POE Switches: 10.192.0.2-9
* PCs: 10.192.0.10-19
* Axis-Cameras: 10.192.0.20-49
* Mobotix-Cameras: 10.192.0.50-79
* LIDAR: 10.192.0.80-99

This image shows an overview of the network wiring on the crane itself:

![network overview](../network.png)

We also have names for the cameras that tell us where they are located. The
general scheme is:

```
f[1-4]_l[1-3]_([vt][1-9]|li|la)
```

- `f[1-4]`: which foot of the crane it is on, starting with the one we equipped
  first
- `l[1-3]`: which height the camera is on, 3 being the highest
- `[vt]`: `v` for visual, `t` for thermal
- `v[1-9]`: if in one location there are multiple cameras
- `li|la`: we have two LIDARs per foot, one closer to the inside (`li`) and one
  closer to the outside (`la`), inside being the inside of the crane

__TODO__: Paths to neural nets

To manage the system application itself we decided to have a simple systemd
starter which also can easily be replaced by other means.

Needed for Recording:

* Camera that provides a protocol that opencv can understand, we use rtsp for
  that
* Location where to record to with enough space left (according to config)
* config that deactivates everything that is not needed for recording, esp.
  Neural Nets and activates the writers

__TODO__: Recording Requirements (if recording is needed)
__TODO__: Necessary to describe also automodal-gpu/net-pipeline here; yes here

## Logging, Tracing and Protocols

The system should also be working, if there is no internet connection available
or no systems outside of the direct net are accessible. Therefore, we decided to
log only to a local file and when necessary also to `stdout`.

The general logging format we chose is configured (currently mostly hardcoded)
[here](../../src/system.py) (~line 150).

We also decided to send our log-records over MQTT wich makes it possible to
collect log messages from multiple completely independent sources in one single
file. Be it different processes or entirely different systems.

The axis-cameras we use, provide us with a rtsp streams for the video streams.

The LIDAR devices we use, actively send UDP packages to our system, provided
they are configured correctly. They can also do a broadcast sending to every
device in the network, but as we only have one (or at max two) system that need
to work with the data, we can reduce the stress on the network structure by
going to direct communication.

## Configurability

For the configuration we created a [yaml file](../../config/sand_config.yaml)
where all the different options for the system can be viewed and changed. This
file concerns mostly the overall system.

Where more configurability is necessary, in our case the neural network
configurations are also in the [config-folder](../../config).

## Parallelization and Threading

The general rule in this project is, that every module you see will have _at
least_ one thread that represents the main working thread. Additional threads
can also be created by our dependencies and sub-libraries, especially opencv.

## Scaling and Clustering

The system is currently planned as a single node system. Scaling in this case is
limited to improving the CPU or GPU capacity here.

This is obviously subject to change, if we come to the result, that the current
power of the system is not enough to fulfill the task of biomass recognition. If
that happens, the interfaces between the modules of the pipeline will need to be
reworked to a networking interface. The different modules can then be
distributed across multiple nodes.
