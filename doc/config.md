# config



## config folder

Instead of many an arg per config we choose to use a config directory with a
fixed layout. Beside the configs there are also all related images for
calibration and map building process.

### config folder example
```bash
├── calibration
│   ├── images
│   │   └── cam1.jpg
│   ├── lidar
│   │   └── lidar1.velo
│   └── map
│       └── map.yaml
├── detector_models {optional}
│   └── yolox
│       └── yolox_x_coco
│           ├── yolox_x_coco.py
│           └── yolox_x_coco.yaml
├── masks
│   ├── image
│   │   └── cam1.jpg
│   └── map
│       └── cam1.jpg
├── mlcvzoo
│   └── replacement.yaml
├── sand.yaml
└── zones.svg
```
