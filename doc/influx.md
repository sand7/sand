### delete all data in bucket

````
docker exec -ti sand_influxdb2 /usr/local/bin/influx delete --org automodal
--bucket sand --start '1970-01-01T00:00:00Z' --stop $(date
+"%Y-%m-%dT%H:%M:%SZ")
````

## use V1 Databases with a influxv2

https://docs.influxdata.com/influxdb/v2.0/upgrade/v1-to-v2/manual-upgrade/

### get into container
```
docker exec -ti sand_influxdb2 /bin/bash
```
### get bucket list
```
influx bucket list

influx config default
```
### get bucket ID
```
influx bucket list | grep sand | cut -c1-16
```
### copy bucket id and add in following command
```
influx v1 dbrp create \
  --db sand \
  --rp autogen \
  --bucket-id `influx bucket list | grep sand | cut -c1-16` \
  --default
```

### ALL IN ONE dbrp mapping
```
docker exec -ti sand_influxdb2 /usr/local/bin/influx v1 dbrp create --db sand
--rp autogen --bucket-id $(docker exec -ti sand_influxdb2 /usr/local/bin/influx
bucket list | grep sand | cut -c1-16) --default
```

### ALL IN ONE set user account

asks for password

```
docker exec -ti sand_influxdb2 /usr/local/bin/influx v1 auth create \
  --read-bucket $(docker exec -ti sand_influxdb2 /usr/local/bin/influx bucket
  list | grep sand | cut -c1-16)  \
  --write-bucket $(docker exec -ti sand_influxdb2 /usr/local/bin/influx bucket
  list | grep sand | cut -c1-16)  \
  --username sand
```

