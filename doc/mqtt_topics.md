
## Camera System

```` bash
camera_system/$CAMERA_NAME/data/enriched
````

## Neural Network

```` bash
neural/$CAMERA_NAME/data/boxes
neural/$CAMERA_NAME/data/interesting
````

## Box Transformer

```` bash
btrans/$CAMERA_NAME/data/boxes
````


## Sensor Fusion

```` bash
fusion/all/data/collision
fusion/all/data/status
````

## Lidar

```` bash
lidar/$LIDAR_NAME/data/pointcloud
lidar/all/data/3dpointcloud
lidar/all/data/2dpointcloud
````

## Publisher

```` bash
publisher/all/data/active
````

## Shutdown

$MODUL = the sender modul
$UUID = uuid of the ShutdownAbles, generated on system startup

```` bash
$MODUL/$UUID/data/register
main/all/data/shutdown
````

## Config

$IDENT = camera_name or lidar_name or all \
$MODUL = the sender modul \
$CONFIG_OPTION = the option name from the yaml config file

```` bash
$MODUL/$IDENT/config/raw
$MODUL/$IDENT/config/$CONFIG_OPTION
````


