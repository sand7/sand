### install BFG
https://rtyley.github.io/bfg-repo-cleaner/

```
yay -Syu bfg
```

### clean

```
git clone --mirror git@gitlab.contargo.net:automodal/sand.git
bfg --delete-files "f{1,2,3,4}_l{1,2,3,4}_v{1,2}.{jpg,png}" sand.git
bfg --delete-files "google_maps_screenshot.jpg" sand.git
cd sand.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
git push --force
```
