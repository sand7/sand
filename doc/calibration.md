# calibration

To get correct transformation of images and detection boxes we need a
calibration of the camera images to the "map".
The Map is the the 2d represtation of the area we want to secure. Like a
Terminal or a Building.

Just follow the instructions or help of the tools

$CAMERA is the name of the camera (for example f1_l3_v1)

## install

```
poetry install
```

## calibration map

the calibration map will be generated from a defintion file.
The Values are in pixel.
For example if your 2d area is 10x10m and your map 1000x1000 pixel, every
pixel represents 1x1cm.
If you want to use a different nullpoint you can defined it also.

* **map** the size of the map
* **rectangulars** & **lines** are just for orientation.
* **calibration_points** will be also used in image_calibration.

example:
```
map:
    horizontal: 100
    vertical: 100
nullpoint:
    horizontal: 0
    vertical: 0
rectangulars:
    - name: cube
      width: 10
      height: 10
      horizontal: 10
      vertical: 20
lines:
    - "(10,10),(60,60)" # "(from),(to)"
calibration_points:
    - horizontal: 10
      vertical: 10
calibration_points:
    - horizontal: 90
      vertical: 90
calibration_points:
    - horizontal: 10
      vertical: 90
calibration_points:
    - horizontal: 90
      vertical: 10
```

#### run generate_calibration_image.py

```
?> generate_calibration_image -d /path/to/map/definition

ARGUMENTS
  -h, --help            show this help message and exit
  -ppm PIXELPERMETER, --pixelpermeter PIXELPERMETER
                        pixel per meter
  -f FILE, --file FILE  filename
  -d DEFINTION, --defintion DEFINTION
                        path to the map definition
  -s, --show            show generated IMAGE

```
the calibration map will be saved at ```images/calibration.jpg```


## focal undistortion value

run focal.py for all cameras and add the focal value to the config

```
?> image_focal -c $CAMERA

ARGUMENTS
  -h, --help                        show this help message and exit
  -c CAMERA, --camera CAMERA        camera name from config
  --config CONFIG                   path to config file

```

keys:
```
    s       decrement focal by 100
    w       increment focal by 100
    ESC     exit
```

For a good focal calibration try to focus on straight lines on you image.
Verify the straightness with a painting tool or a screenshot tool like
flameshot who can also draw lines.

image from camera            |  good focal correction
:-------------------------:|:-------------------------:
![](images/focal_default.png)  | ![](images/focal_good.png)


## transformation points

For calibration we need 4 reference points on the source (image from camera)
and on the target (calibration image).
The points are numbered and each number
pair must represent the same physical point on target and source.

Use `Mouse` to set calibration points in the Images.
Use `WASD` to move the last Point in source Image.
On target Image the point will be automatically the nearest crosshair.

The calibration points will be printed in the console. Copy it to your config.

```
?> image_calibration -c $CAMERA -s --map-definition /path/to/map/definition

ARGUMENTS
  -h, --help                        show this help message and exit
  -c CAMERA, --camera CAMERA        camera name from config
  -s, --show                        show the transformed image after calibration
  --config CONFIG                   path to config file
  --mapdefinition MAPDEFINITION     path to map definition
```

keys:
```
    wasd    move active calibration point
    ESC     exit
```

## map calibration

For better calibration, you can use ```map_calibration``` to manually modify
the calibration in a fancy map view.

```
?> map_calibration

ARGUMENTS
  -h, --help                        show this help message and exit
  -c CONFIG, --config CONFIG        path to config file
  -p PATH, --path PATH              path to segment for lidar data
  -m, --nomap                       don't show the generated map in the
  background
  -s SCALE, --scale SCALE           scale of map, default 0.1
```

keys:
```
LIDAR
    m or .  change active lidar
    o or u  rotate active lidar
    ijkl    move lidar null point

CAMERA:
    c or y  change active camera
    1 or 3  change focal for active camera
    e or q  change calibration point of active camera
    wasd    move active calibration point
    ESC     exit
```

