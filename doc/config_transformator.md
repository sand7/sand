# config transformator

in our case we needet a way to modify the camera calibrations/transformations.
We get a position of a camera group and calculate the offset for our
perspective.

## group position transformation

To change a group position we send the actual position of the group via mqtt
on topic ```{sender}/{group}/data/position"```.


### example code to send the position
```python
publish_properties = Properties(PacketTypes.PUBLISH)
# to make sure we can pickle everything and make it easier here, performance here is irrelevant
publish_properties.UserProperty = ("datatype", "pickle")
self.client.publish(
    topic=f"bridge/katz/data/position",
    payload=pickle.dumps(
        {
            "y_position": katz,
            "x_position": crane,
            "z_position": spreader,
        }
    ),
    properties=publish_properties
)
```

### scaling

We send in mm, our
Calibration map is generated in cm. So we need a scale option.
In our Case we set scale to 0.1.

```python
pixel_position_y = group_position_y * 0.1
```

### offset

We have 2 coordinate perspektives.
1. the real live measurement perspektive
2. our digital perspektive

For the calculation we need now also the real live measurment at same
position as the calibration images are taken. We call it just offset.

```python
pixel_position_y = group_position_y * 0.1 - offset_y
```

### config example
```yaml
config_transformer:
    active: true
    scale: 0.1

groups:
    -   name: "katze"
        offset_x: 0
        offset_y: 0
        offset_z: 0
        transform_x: True
        transform_y: False
        transform_z: False
```
