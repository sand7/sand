# dangerzones

for now we have 2 different zones to secure. One only for Persons and an
zone for all movable Objects.

## edit/add zones

Open images/area.svg with inkscape. Please only use Inkscape.
Move oder copy the zones and save it with the same name.
All Zones for Persons must have the colorcode #000080 and for object #ff0000
for the outline. Filling will be ignored.

