# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Changelog
- Ability to scale images at the start/end of every pipeline step, which breaks
  some APIs

### Changed
- API of `NeuralNetwork` now works with `Collectable`s instead of
  `CameraSystem`s directly
- Clarified actual exports vs implicit exports according to
  [PEP484](https://peps.python.org/pep-0484/)
- API of `CameraReader` to be a `ConfigurationManager` to be able to change the
  config on the fly with the `config_changer`
- Some types of SAND are now based on the `opencv` types to make type-checking
  easier
- Stopped listening to SIGTERM, use MQTT shutdown or SIGING for a
  KeyboardInterrupt
- Remove metrics feature for now
- Remove need for `override`s as runtime dependency
- Remove thread naming via `prctl`

## [1.1.1] - 2022-07-21
### Fixed
- Error in publisher with multiple double declared script-tags which made it
  impossible to use the publisher with more than one camera

## [1.1.0] - 2022-07-15
### Added
- More robust ip address handling in Publisher
- Release to PyPI

### Changed
- Updated dependencies

### Fixed
- Wrong types in config-schemas

## [1.0.0] - 2022-06-24
### Added
- Initial Release
