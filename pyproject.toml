[tool.poetry]
name = "python-sand"
version = "2.0.0a4"
homepage = "https://gitlab.com/sand7/sand"
repository = "https://gitlab.com/sand7/sand"
documentation = "https://gitlab.com/sand7/sand/-/tree/main/doc"
description = "Processing sensor and video data made easy"
authors = [
    "Moritz Sauter <sauter@synyx.de>",
    "David Bauer <bauer@synyx.de>",
    "Maximilian Otten <maximilian.otten@iml.fraunhofer.de>",
]
maintainers = [
    "Moritz Sauter <sauter@synyx.de>",
    "David Bauer <bauer@synyx.de>",
]
license = "MIT"
readme = "README.md"
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Environment :: GPU",
    "Intended Audience :: Developers",
    "Natural Language :: English",
    "Natural Language :: German",
    "Operating System :: POSIX :: Linux",
    "Topic :: Multimedia :: Video :: Capture",
    "Topic :: Multimedia :: Video :: Conversion",
    "Topic :: Multimedia :: Video :: Display",
    "Topic :: Scientific/Engineering :: Artificial Intelligence",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Typing :: Typed",
]

packages = [{ include = "sand", from = "src" }]

[tool.poetry.scripts]
image_downloader = "sand.calibration.image_download:main"

# calibration
image_focal = "sand.calibration.image_focal:main"
image_calibration = "sand.calibration.image_calibration:main"
map_calibration = "sand.calibration.map_calibration:main"
generate_calibration_image = "sand.calibration.generate_calibration_image:main"

# cli tools
config_changer = "sand.cli.config_changer:run"
mqtt_listener = "sand.cli.mqtt_listener:run"
shared_memory_cleaner = "sand.cli.shared_memory_cleaner:run"

sand = "sand.cli:run"

[tool.poetry.dependencies]
python = "^3.8"

numpy = "^1.21.4"
yaml-config-builder = "^8.0.0"
paho-mqtt = "^1.6.1"
opencv-contrib-python = "^4.7.0.72"

flask = { version = "^2.1.2", optional = true }
flask-socketio = { version = "^5.2.0", optional = true }
eventlet = { version = "^0.35.0", optional = true }

mlcvzoo-base = { version = "^5.3.0", optional = true }

[tool.poetry.extras]
neural = ["mlcvzoo_base"]
publisher = ["flask", "flask-socketio", "eventlet"]

[tool.poetry.group.dev.dependencies]
adr-tools-python = "^1.0.3"
black = "^23.1.0"
mypy = "^1.0.1"
pytest = "^7.2.0"
pytest-cov = "^4.0.0"
pytest-mock = "^3.10.0"
pyupgrade = "^3.2.2"
types-flask = "^1.1.6"
types-pyyaml = "^6.0.12.2"
types-toml = "^0.10.8.1"
types-pkg-resources = "^0.1.3"
ruff = "^0.0.285"
pre-commit = "^3.2.2"

[build-system]
requires = ["poetry_core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.pytest.ini_options]
markers = [
    "real_threads: Internal test marker for using real threads instead of mocks",
]

[tool.ruff]
select = [
    "F",
    "E",
    "W",
    "I",
    "ANN",
    "C4",
    "YTT",
    "NPY",
    "RUF",
    "UP",
    "EXE",
    "ISC",
    "INP",
    "PIE",
    "T20",
    "PYI",
    "PT",
    "Q",
    "RSE",
    "RET",
    "SIM",
    "TID",
    "ARG",
    "ERA",
    "PGH",
    "PLC",
    "TRY",
    # "PL",
    "PTH",
    # "TCH",
    # "SLF",
    # "G",
    "A",
    "B",
    # "DTZ",
]
ignore = [
    "E501",    # line length
    "ANN101",  # type `self`
    "ANN102",  # type `cls`
    "ANN401",  # disallow any-type
    "PLC0414", # useless import alias
    "TRY003",  # raise-vanilla-args
    "TRY300",  # try-consider-else
]

target-version = "py38"

[tool.ruff.per-file-ignores]
"src/*" = ["PT"]                   # pytest rule set
"src/sand/cli/*" = ["T20"]         # print detection
"src/sand/calibration/*" = ["T20"] # print detection
"tests/*" = ["T20"]                # print detection
"examples/*" = ["INP", "T20"]
"config/*" = ["INP", "T20"]

[tool.ruff.isort]
required-imports = ["from __future__ import annotations"]

[tool.mypy]
files = ["examples", "src"]
show_column_numbers = true
show_error_codes = true
pretty = true

# additional warnings
strict = true
warn_return_any = true
warn_unused_ignores = true
warn_unused_configs = true
warn_redundant_casts = true
warn_no_return = true

no_implicit_optional = true
warn_unreachable = true
disallow_untyped_defs = true
disallow_incomplete_defs = true
# disallow_any_explicit = true
disallow_any_generics = true
disallow_untyped_calls = true

enable_error_code = ["ignore-without-code"]

# ignores that library has no typing information with it
[[tool.mypy.overrides]]
module = [
    "cv2",                  # https://github.com/opencv/opencv/issues/14590
    "cv2.fisheye",
    "related",
    "paho.*",
    "open3d",
    "open3d.visualization",
]
ignore_missing_imports = true
