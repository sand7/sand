from __future__ import annotations

from sand.cli import SandStarter
from sand.cli.arguments import Arguments


class CustomSandStarter(SandStarter):
    def start_system(self, arguments: Arguments) -> None:
        print("We just want to say hello in a non performant way :)")
        print("You can call me with:\n")
        print(
            "PYTHONPATH=${PYTHONPATH:-}:examples/overriden_starter sand --custom-start-module custom_starter"
        )
        print()
        print(f"And tell you what we got: {arguments=}")
        print("\nGoodbye!\n")
