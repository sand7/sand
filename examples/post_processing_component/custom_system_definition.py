from __future__ import annotations

from pathlib import Path
from typing import cast

from config_builder import ConfigBuilder
from sand.config.config import ConstantConfig, SandConfig
from sand.neural.pipeline.neural import NeuralNetwork
from sand.reader.video.camera_system import CameraSystem

from .custom_post_processing import CustomPostProcessing


def _start_multi_gpu_camera_system(
    sand_config: SandConfig,
) -> None:
    camera_systems = [
        CameraSystem(camera, sand_config, playback=False)
        for camera in sand_config.cameras
    ]

    readers = [cs.reader for cs in camera_systems]
    post_processers = [
        CustomPostProcessing(reader, reader.config) for reader in readers
    ]

    NeuralNetwork(post_processers, sand_config)


def define_system(
    config: Path,
) -> None:
    print(f"{config=}")

    ConstantConfig()
    config_builder = ConfigBuilder(
        class_type=SandConfig, yaml_config_path=config.as_posix()
    )
    sand_config = cast(SandConfig, config_builder.configuration)

    _start_multi_gpu_camera_system(sand_config)
