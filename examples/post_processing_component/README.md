# Post Processing Component

This example shows how we intended that new functionality and components are
added to the system in general.

The task that we want to solve in this example is the following scenario:

We recognized if we focal-correct our images before giving them to the
`NeuralNetwork` we improve the accuracy. Therefore we want to add a component
in front of the neural-component in the pipeline.

This is a rather big change and may not immediately obvious to implement. For
this one we need a couple of things as contrary to the [overridden_starter
example](../overridden_starter) SAND doesn't have an API for this specific issue.

The idea behind SAND is one of little custom components that if put together
become a bigger system. If you've never heard of it, we very much follow the
pipe-and-filter architecture ideas.

So for the custom post processing we need our own
[system_definition](custom_system_definition.py). This one we kept very simple,
the [default for SAND](../../src/sand/definition/system_definition.py) is quite
complex compared to this one. If you have to (or want to) build your own we
recommend taking a look at the SAND one, especially for the usage of
[`Isolator`s](../../src/sand/interfaces/synchronization/isolator.py) as it is
not the most straight forward.

Back to topic though, we created our own
[`CustomPostProcessing`](custom_post_processing.py) that only focal-corrects
`EnrichedFrame`s. The API is not too difficult we just have to fulfill the
interface for the component in front of us and the one after us. In our case the
one in front of us is the [`CameraReader`]() and the one after us is the
[`NeuralNetwork`](). For the `CameraReader` we need to be an
`EnrichedSubscriber` and for the `NeuralNetwork` we need to be a
`NamedCollectAble[EnrichedSubscriber]`.

The current implementation aims to be completely transparent for both other
components but you can totally be free with your interface if you want to
implement something more complex or multistaged.
