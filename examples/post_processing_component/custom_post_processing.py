from __future__ import annotations

from sand.config import CameraConfig
from sand.datatypes import EnrichedFrame, Scale
from sand.interfaces.util import EnrichedSubscriber, NamedCollectAble
from sand.transformer.focal import FocalNormalizer


class CustomPostProcessing(NamedCollectAble[EnrichedSubscriber], EnrichedSubscriber):
    """Will do custom post processing in between a CameraReader and a NeuralNetworkself.

    Depending on your performance requirements you have to take care what you do here,
    especially if you are using it early in the pipeline.
    """

    def __init__(
        self,
        collectable: NamedCollectAble[EnrichedSubscriber],
        config: CameraConfig,
    ) -> None:
        # NamedCollectAble does some basic subscriber mechanics for us
        NamedCollectAble.__init__(self)

        # as we get from NamedCollectAble, our subscribers probably expect also one
        self._name = collectable.get_name()

        # for our example we need additional values from the config
        scale = Scale(
            config.transformation_source_resolution,
            config.stream_resolution,
        )

        # our example post processing, but you can do everything here,
        # depending on your needs
        self._focal = FocalNormalizer(config.focal, scale)

        # we want to get something from our collectable
        collectable.subscribe(self)

    def get_name(self) -> str:
        # give the same name than before so we are absolutely transparent to
        # the follower components
        return self._name

    def _post_process(self, frame: EnrichedFrame) -> EnrichedFrame:
        # execute our simple focal normalization
        return EnrichedFrame(
            frame.camera_name,
            frame.timestamp,
            self._focal.normalize_enriched_frame(frame),
            frame.id,
        )

    def push_frame(self, frame: EnrichedFrame) -> None:
        # we just want to immediately push a frame on, this can also be
        # uncoupled for performance reasons, the SandNode might help you here
        # if you want to. For this example we stick to simple things though
        for subscriber in self.subscribers:
            subscriber.push_frame(self._post_process(frame))
