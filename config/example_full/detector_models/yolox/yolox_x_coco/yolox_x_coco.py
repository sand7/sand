from __future__ import annotations

import logging

from mlcvzoo_yolox.configuration import YOLOXConfig
from mlcvzoo_yolox.exp.custom_base_yolox_exp import CustomBaseYOLOXExp

logger = logging.getLogger(__name__)


class CustomYOLOXExp(CustomBaseYOLOXExp):
    def __init__(self, configuration: YOLOXConfig) -> None:
        """ """
        CustomBaseYOLOXExp.__init__(self, configuration=configuration)
