# Detector Models

These are some examples for configurations of neural nets. We tried quite a few
in our time and one that worked quite well was [yolox][https://github.com/Megvii-BaseDetection/YOLOX].

This project currently specifies that it has to use `python3.8` which is
becoming quite old now. Therefore we don't specify this as an extra in our
dependency list anymore (while we did up until `2.0.0a4`).

If you want to use this example you will probably need to use `3.8` or try to
work around it, but YMMV.

In the end the intended way to use SAND with models from MLCVZoo is to declare
us as a dependency and one of the other modules MLCVZoo provides, take a look
[here](https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo).

### YOLOX

To use our examples directly without adaption we used `mlcvzoo-yolox=="^5.0.0"`
to create those (at least at the time of writing).
