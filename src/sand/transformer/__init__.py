"""This module takes the information from the :mod:`neural`-Module and transforms the images into an arial view."""
from __future__ import annotations

from .pipeline import BoxTransformer as BoxTransformer
from .pipeline import ImageTransformer as ImageTransformer
