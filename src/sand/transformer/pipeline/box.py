from __future__ import annotations

from collections import deque
from threading import Lock
from time import sleep

from cv2 import perspectiveTransform
from cv2.fisheye import undistortPoints
from numpy import array, expand_dims, float32, float64
from numpy.typing import NDArray

from sand.config import SandConfig, TransformerCombinationConfig
from sand.datatypes import Box, Dimensions, Point, SandBoxes, Topic, TransformedBoxes
from sand.datatypes.scale import Scale
from sand.datatypes.third_party.mlcvzoo import BoundingBox
from sand.interfaces.config import ConfigurationManager, find_config
from sand.interfaces.synchronization import SandNode
from sand.transformer.focal import FocalNormalizer
from sand.transformer.transformation import Transformation
from sand.util.boxes import bounding_box_to_point_list


class BoxTransformer(SandNode, ConfigurationManager[TransformerCombinationConfig]):
    def __init__(
        self,
        camera_name: str,
        global_config: SandConfig,
        playback: bool,
    ) -> None:
        SandNode.__init__(self, global_config.communication)
        ConfigurationManager.__init__(
            self,
            self,
            global_config,
            update_pattern=f"{ConfigurationManager.__name__}/{camera_name}/data/cameras/transformation_target_points",
        )

        self.camera_name = camera_name
        self.queue: deque[SandBoxes] = deque(maxlen=1)
        self.queue_lock = Lock()
        self.playback = playback
        self.source = "NeuralNetwork"

        self.subscribe_topic(
            f"{self.source}/{self.config.camera.name}/data/boxes",
            self.push_neural_frame,
        )
        output_dimension = Dimensions(
            self.config.transformer.box.output_width,
            self.config.transformer.box.output_height,
        )
        self.transformation = Transformation(self.config.camera, output_dimension)

        self.log.d(f"DAVID {self.transformation.get_scale_informations()=}", "__init__")

        self.focal_scale = Scale(
            self.config.camera.transformation_source_resolution,
            self.config.camera.stream_resolution,
        )
        self.focal = FocalNormalizer(self.config.camera.focal, self.focal_scale)

        if self.config.transformer.box.active:
            self.create_thread(
                target=self.work,
                args=(),
                name=self.get_name(),
                start=True,
            )

    def select_config(self, global_config: SandConfig) -> TransformerCombinationConfig:
        camera_config = find_config(
            device_name=self.camera_name, config_list=global_config.cameras
        )

        assert camera_config is not None

        return TransformerCombinationConfig(
            camera=camera_config,
            transformer=global_config.transformer,
        )

    def config_has_updated(self) -> None:
        self.transformation.set_cal_points(self.config.camera.transformation)

    def push_neural_frame(self, _: Topic, boxes: SandBoxes) -> None:
        if (
            any(
                group in self.config.transformer.box.groups
                for group in self.config.camera.groups
            )
            or len(self.config.transformer.box.groups) == 0
        ):
            with self.queue_lock:
                self.queue.append(boxes)

    def get_name(self) -> str:
        return f"bt_{self.config.camera.name}"

    def _transform_bounding_box(self, bounding_box: BoundingBox) -> Box | None:
        # use the focal distortion map to undistort the points
        box_points = bounding_box_to_point_list(
            bounding_box, self.config.camera.stream_resolution
        )
        focal_points = undistortPoints(
            distorted=expand_dims(
                array([box_points], dtype=float32).reshape(4, 2), axis=1
            ),
            K=self.focal.camera_intrinsic_matrix,
            D=self.focal.distortion_coefficients,
            P=self.focal.camera_intrinsic_matrix,
        )
        focal_points = focal_points.reshape(4, 2)
        asd = []
        for focal_point in focal_points:
            asd.append(Point(focal_point[0], focal_point[1]))

        # the transformation
        # we need the right array format for transformation
        np_points = array([asd], dtype="float32")
        if np_points.shape[1] != 4:
            self.log.d(
                f"points are not len 4 - {np_points=}, {focal_points=}, {box_points=}, {bounding_box=}",
                "_transform_bounding_box",
            )
            return None

        transformed_points: NDArray[float64] = perspectiveTransform(
            np_points, self.transformation.get_matrix()
        )
        # scaling is not necessary, the transformation matrix is generated with the right scale
        int_points = transformed_points.astype(int)[0]
        output_points = [Point(pt[0], pt[1]) for pt in int_points]
        if len(output_points) != 4:
            self.log.d(
                f"transformed points are not len 4 - len(output_points): {len(output_points)}",
                "_transform_bounding_box",
            )
            return None
        return Box(*output_points)

    def _transform_cal_points(self, point_list: list[Point]) -> list[Point] | None:
        if len(point_list) == 0:
            return None
        # the transformation
        # we need the right array format for transformation
        transformed_points: NDArray[float32] = perspectiveTransform(
            array([point_list], dtype="float32"), self.transformation.get_matrix()
        )
        # scaling is not necessary, the transformation matrix is generated with the right scale
        int_points = transformed_points.astype(int)[0]
        return [Point(pt[0], pt[1]) for pt in int_points]

    def _get_calibration_points(self) -> list[Point] | None:
        return self._transform_cal_points(
            self.config.camera.transformation.source_points
        )

    def _transform_boxes(self, boxes: list[BoundingBox]) -> list[Box]:
        output = []
        for box in boxes:
            transformed_box = self._transform_bounding_box(box)
            if transformed_box is not None:
                output.append(transformed_box)
        return output

    def work(self) -> None:
        self.log.i("Starting to work", "work")

        while not self.shutdown_event.is_set():
            try:
                with self.queue_lock:
                    box = self.queue.popleft()

                transformed_boxes = (
                    self._transform_boxes(box.boxes) if len(box.boxes) > 0 else []
                )
                result = TransformedBoxes(
                    box.frame_id,
                    box.timestamp,
                    box.camera_name,
                    box.boxes,
                    transformed_boxes,
                )
                self.publish(
                    payload=result,
                    topic=f"{BoxTransformer.__name__}/{self.config.camera.name}/data/transformed_boxes",
                )
            except IndexError as exception:
                if "pop from an empty deque" not in exception.args:
                    # exception gives stacktrace
                    self.log.exception(str(exception.args), "work")
                sleep(1 / self.config.camera.fps)
