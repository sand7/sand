"""The recorder module provides services to record datastreams from the :mod:`reader`-module to the local hard-drive.

As other modules it is divided into, :mod:`lidar` / 3D and :mod:`video` / visual data.
"""
