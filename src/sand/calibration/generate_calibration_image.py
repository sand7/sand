from __future__ import annotations

import re
from argparse import ArgumentParser
from pathlib import Path
from typing import Any

import yaml
from cv2 import WINDOW_NORMAL, imshow, imwrite, line, namedWindow, waitKey
from numpy import uint8, zeros

from sand.datatypes import Color, Image, Point
from sand.util.image import draw_horizontal_line, draw_vertical_line, draw_x

IMAGE = zeros((1, 1, 3), uint8)


def mm_to_pixel(millimeter: int, pixel_per_meter: int = 100) -> int:
    return int(pixel_per_meter * millimeter / 1000)


def generate_calibration_image(map_config: Any, middlelines: bool = False) -> Image:
    line_width = 5
    color_lines = Color(50, 50, 50)
    color_nullpoint_lines = Color(255, 200, 255)
    color_points = Color(0, 0, 255)

    map_size_x = map_config["map"]["horizontal"]
    map_size_y = map_config["map"]["vertical"]
    nullpoint_x = map_config["nullpoint"]["horizontal"]
    nullpoint_y = map_config["nullpoint"]["vertical"]

    image = zeros((map_size_y, map_size_x, 3), uint8)
    image[:] = (255, 255, 255)

    if middlelines:
        image = draw_horizontal_line(image, nullpoint_y, color_nullpoint_lines)
        image = draw_vertical_line(image, nullpoint_x, color_nullpoint_lines)

    for line_str in map_config["lines"]:
        line_list = [
            (
                int(float(match.split(",")[0])),
                int(float(match.split(",")[1])),
            )
            for match in re.findall(
                r"\(([-]?[0-9]*[.]?[0-9]*,[-]?[0-9]*[.]?[0-9]*)\)", line_str
            )
        ]
        image = line(
            image,
            (line_list[0][0] + nullpoint_x, line_list[0][1] + nullpoint_y),
            (line_list[1][0] + nullpoint_x, line_list[1][1] + nullpoint_y),
            color_lines,
            line_width,
        )

    point_list = []
    for point_config in map_config["calibration_points"]:
        point = Point(
            point_config["horizontal"] + nullpoint_x,
            point_config["vertical"] + nullpoint_y,
        )
        point_list.append(point)
        image = draw_x(image, point.x, point.y, color_points, size=50, line_width=10)

    for rect in map_config["rectangulars"]:
        image = line(
            image,
            (rect["horizontal"] + nullpoint_x, rect["vertical"] + nullpoint_y),
            (
                rect["horizontal"] + nullpoint_x,
                rect["vertical"] + rect["height"] + nullpoint_y,
            ),
            color_lines,
            line_width,
        )
        image = line(
            image,
            (
                rect["horizontal"] + nullpoint_x,
                rect["vertical"] + rect["height"] + nullpoint_y,
            ),
            (
                rect["horizontal"] + rect["width"] + nullpoint_x,
                rect["vertical"] + rect["height"] + nullpoint_y,
            ),
            color_lines,
            line_width,
        )
        image = line(
            image,
            (rect["horizontal"] + nullpoint_x, rect["vertical"] + nullpoint_y),
            (
                rect["horizontal"] + rect["width"] + nullpoint_x,
                rect["vertical"] + nullpoint_y,
            ),
            color_lines,
            line_width,
        )
        image = line(
            image,
            (
                rect["horizontal"] + rect["width"] + nullpoint_x,
                rect["vertical"] + nullpoint_y,
            ),
            (
                rect["horizontal"] + rect["width"] + nullpoint_x,
                rect["vertical"] + rect["height"] + nullpoint_y,
            ),
            color_lines,
            line_width,
        )
    return image


def main() -> None:
    parser = ArgumentParser(description="yolo")
    parser.add_argument(
        "-ppm", "--pixelpermeter", type=int, default=100, help="pixel per meter"
    )
    parser.add_argument(
        "-f",
        "--file",
        default=False,
        action="store_true",
        help="write calibration image?",
    )
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config folder",
    )
    parser.add_argument(
        "-s",
        "--show",
        default=False,
        action="store_true",
        help="show generated IMAGE",
    )
    parser.add_argument(
        "-m",
        "--middlelines",
        default=False,
        action="store_true",
        help="generate middle lines",
    )

    parser.add_argument(
        "-g",
        "--gmaps",
        default=False,
        action="store_true",
        help="use gmaps as background",
    )

    args = parser.parse_args()
    map_definition = Path(args.config).joinpath("calibration/map/map.yaml")
    calibration_image_path = Path(args.config).joinpath("calibration/calibration.jpg")
    with map_definition.open(encoding="utf-8") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)

    write_file = args.file

    print("generation with config file:", map_definition)
    print("horizontal map size:        ", config["map"]["horizontal"])
    print("vertical map size:          ", config["map"]["vertical"])

    image = generate_calibration_image(config, middlelines=args.middlelines)

    if args.show:
        window_name = "calibration_image"
        namedWindow(window_name, WINDOW_NORMAL)
        imshow(window_name, image)
        print("press any key on image to exit")
        waitKey(0)
    if write_file:
        print("write to file:", calibration_image_path.as_posix())
        imwrite(calibration_image_path.as_posix(), image)

    if not args.show and not write_file:
        print()
        print("please provide and output file or use --show")


if __name__ == "__main__":
    main()
