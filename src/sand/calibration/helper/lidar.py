from __future__ import annotations

from sand.config import SandConfig
from sand.reader.lidar import LidarSystem


def init_lidar(config: SandConfig) -> list[LidarSystem]:
    lidar_systems: list[LidarSystem] = []
    for lidar_config in config.lidars:
        lidar_systems.append(
            LidarSystem(lidar_config, is_playback=True, sand_config=config)
        )
    return lidar_systems


def shutdown_lidar(lidar_systems: list[LidarSystem]) -> None:
    for lidar in lidar_systems:
        lidar.reader.shutdown()
        lidar.collector.shutdown()
