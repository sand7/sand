from __future__ import annotations

from sand.config import SandConfig
from sand.datatypes import Color, Image, Point
from sand.reader.lidar import LidarSystem
from sand.util.image import draw_x


def draw_active_calibration_point(map_image: Image, point: Point) -> None:
    draw_x(map_image, point.x, point.y, Color(255, 0, 0), line_width=2)


def init_lidar(config: SandConfig) -> list[LidarSystem]:
    lidar_systems: list[LidarSystem] = []
    for lidar_config in config.lidars:
        lidar_systems.append(
            LidarSystem(lidar_config, is_playback=True, sand_config=config)
        )
    return lidar_systems


def shutdown_lidar(lidar_systems: list[LidarSystem]) -> None:
    for lidar in lidar_systems:
        lidar.reader.shutdown()
        lidar.collector.shutdown()
