from __future__ import annotations

import threading
from argparse import ArgumentParser
from pathlib import Path
from socket import AF_INET, SOCK_DGRAM, socket

from sand.config import LidarConfig, get_config


class Vlp16Reader:
    def __init__(self, config: LidarConfig) -> None:
        self.config = config

        self.soc = socket(AF_INET, SOCK_DGRAM)
        self.soc.bind(("", self.config.data_port))
        self.soc.settimeout(1)

    def get_packet(self) -> bytes:
        return self.soc.recv(1206)

    def close(self) -> None:
        self.soc.close()


def write_packet(data: list[bytes], path: Path, lidar_name: str) -> None:
    with path.joinpath(f"{lidar_name}.velo").open("bw+") as file:
        for packet in data:
            # the packet is the original packet from the lidar, nothing changed
            file.write(packet)  # 1206 bytes long
            # i think its cool to check for an keyword after every package.
            file.write(b"DUDE")  # 4 bytes long


def record_lidar(config: LidarConfig, path: Path) -> None:
    data: list[bytes] = []
    lidar = Vlp16Reader(config)
    for _ in range(10 * 16):
        data.append(lidar.get_packet())
    lidar.close()
    write_packet(data, path, config.name)


def main() -> None:
    parser = ArgumentParser(
        description="Tool to download/extract images from your configured "
        "stream in your config."
    )

    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config folder",
    )

    parser.add_argument(
        "-i",
        "--images-path",
        type=str,
        default="images/lidar",
        help="Where to store the retrieved points.",
    )

    args = parser.parse_args()

    print(
        """
Attention!

This tool reads from the config. For calibration and focal correction it is
advised to use the _maximum_ resolution possible that your camera can provide,
regardless of what resolution you want to use in the production system.

Please make sure that your config contains streams to the maximum resolution
before executing this downloader-tool.
"""
    )

    images_path = Path(args.images_path)
    config_path = Path(args.config)
    sand_config = get_config(config_path)
    threads: list[threading.Thread] = []
    for _, lidar_config in enumerate(sand_config.lidars):
        thread = threading.Thread(target=record_lidar, args=(lidar_config, images_path))
        threads.append(thread)
        thread.start()
    for _, thread in enumerate(threads):
        thread.join()


if __name__ == "__main__":
    main()
