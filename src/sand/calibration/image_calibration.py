from __future__ import annotations

import sys
from argparse import ArgumentParser
from copy import copy
from pathlib import Path
from typing import Any

import yaml
from cv2 import (
    BORDER_CONSTANT,
    EVENT_LBUTTONDOWN,
    EVENT_MBUTTONDOWN,
    EVENT_RBUTTONDOWN,
    LINE_AA,
    WINDOW_NORMAL,
    copyMakeBorder,
    destroyAllWindows,
    getPerspectiveTransform,
    imread,
    imshow,
    namedWindow,
    setMouseCallback,
    startWindowThread,
    waitKey,
)
from numpy import float32

from sand.calibration.generate_calibration_image import generate_calibration_image
from sand.config import CameraConfig, get_camera_id, get_config
from sand.datatypes import CalPoints, Color, Dimensions, Image, Matrix, Point
from sand.datatypes.scale import Scale
from sand.transformer.focal import FocalNormalizer
from sand.transformer.transformation import Transformation
from sand.util.camera import get_path_from_camera_name
from sand.util.image import add_text_to_image, draw_x, split_image

CAMERA_NAME = ""
CAMERA_ID = 0
FILE_PATH = ""
point_list: list[Point] = []


class Counter:
    source_click: int = 0
    target_click: int = 0


def calc_matrix(points: CalPoints) -> Matrix:
    if len(points.target_points) == 4 and len(points.source_points) == 4:
        return getPerspectiveTransform(  # type: ignore[no-any-return]
            float32(points.source_points),  # type: ignore[arg-type]
            float32(points.target_points),  # type: ignore[arg-type]
        )
    print("error")
    print(f"points.target_points {points.target_points}")
    print(f"points.source_points {points.source_points}")
    sys.exit(2)


def print_calibration_data(points: CalPoints) -> None:
    print("")
    print("----------------------------")
    print("cameras data")
    print("")
    print(f"source points: {points.source_points}")
    print(f"target points: {points.target_points}")
    print(f"transformation matrix: {calc_matrix(points)}")
    print("----------------------------")


def find_nearest_point(left: int, top: int) -> Point:
    nearest_point = Point(-99999, -99999)
    for point in point_list:
        diff_x = abs(point.x - left)
        diff_y = abs(point.y - top)
        if diff_x <= abs(nearest_point.x - left) and diff_y <= abs(
            nearest_point.y - top
        ):
            nearest_point = point
    print(f"found nearest {nearest_point} for Input({left},{top})")

    diff_x = abs(nearest_point.x - left)
    diff_y = abs(nearest_point.y - top)
    if diff_x > 100 or diff_y > 100:
        return Point(left, top)
    return nearest_point


def callback_source(
    event: int,
    left: int,
    top: int,
    _flags: int,
    param: tuple[CalPoints, Counter, int],
) -> None:
    points, counter, border_size = param
    if event == EVENT_LBUTTONDOWN and counter.source_click < 4:
        points.source_points.append(Point(left - border_size, top - border_size))
        counter.source_click += 1
    if (event in (EVENT_MBUTTONDOWN, EVENT_RBUTTONDOWN)) and counter.source_click:
        print("remove last point", points.source_points.pop())
        counter.source_click -= 1


def callback_target(
    event: int,
    left: int,
    top: int,
    _flags: int,
    param: tuple[CalPoints, Counter, int],
) -> None:
    points, counter, _ = param
    if event == EVENT_LBUTTONDOWN and counter.target_click < 4:
        points.target_points.append(find_nearest_point(left, top))
        counter.target_click += 1
    if (event in (EVENT_MBUTTONDOWN, EVENT_RBUTTONDOWN)) and counter.target_click:
        print("remove last point", points.target_points.pop())
        counter.target_click -= 1


def draw_image(
    name: str,
    image: Image,
    points: list[Point],
    had_border: bool = False,
    border_size: int = 0,
) -> None:
    for pid, point in enumerate(points):
        pointy = point.y
        pointx = point.x
        if had_border:
            pointy = pointy + border_size
            pointx = pointx + border_size
        image = draw_x(image, pointx, pointy, Color(0, 0, 0), line_width=3, size=50)
        image = add_text_to_image(
            image,
            str(pid),
            Point(pointx + 7, pointy - 10),
            font_scale=4,
            color=Color(0, 0, 255),
            thickness=7,
            line_type=LINE_AA,
        )
    imshow(name, image)


def move_crosshair(points: list[Point], key: int) -> None:
    inc = 1
    if len(points) == 0 or key == -1:
        return
    point = points[len(points) - 1]
    if key == ord("a"):
        point = Point(point.x - inc, point.y)
    if key == ord("d"):
        point = Point(point.x + inc, point.y)
    if key == ord("w"):
        point = Point(point.x, point.y - inc)
    if key == ord("s"):
        point = Point(point.x, point.y + inc)
    points[len(points) - 1] = point


def map_image(
    camera_config: CameraConfig,
    image: Image,
    points: CalPoints,
    counter: Counter,
    border_size: int = 0,
) -> None:
    startWindowThread()

    namedWindow("source", WINDOW_NORMAL)
    namedWindow("target", WINDOW_NORMAL)
    setMouseCallback("source", callback_source, param=(points, counter, border_size))
    setMouseCallback("target", callback_target, param=(points, counter, border_size))
    source_image = imread(FILE_PATH)
    width = int(source_image.shape[1] + 2 * border_size)
    height = int(source_image.shape[0] + 2 * border_size)
    output_dimension = Dimensions(width=width, height=height)
    focal = FocalNormalizer(
        camera_config.focal, Scale(output_dimension, output_dimension)
    )
    if CAMERA_NAME[6] == "t":
        left, _right = split_image(source_image)
        source_image = left

    border_image = copyMakeBorder(
        source_image,
        top=border_size,
        bottom=border_size,
        left=border_size,
        right=border_size,
        borderType=BORDER_CONSTANT,
        value=[0, 0, 0],
    )
    transformed_image = focal.normalize_image(border_image)

    while True:
        draw_image(
            "source",
            copy(transformed_image),
            points.source_points,
            had_border=True,
            border_size=border_size,
        )
        draw_image("target", copy(image), points.target_points)
        key = waitKey(50)
        if key in (27, 13):  # esc or enter
            break
        move_crosshair(points.source_points, key)

    counter.source_click = 0
    counter.target_click = 0
    destroyAllWindows()


def show_image(
    file_path: str, calpoints: CalPoints, camera_config: CameraConfig
) -> None:
    namedWindow("only warp", WINDOW_NORMAL)
    namedWindow("undistort and warp", WINDOW_NORMAL)

    source_image = imread(file_path)
    width = int(source_image.shape[1])
    height = int(source_image.shape[0])
    output_dimension = Dimensions(width=width, height=height)
    transformation = Transformation(camera_config, output_dimension)
    transformation.set_cal_points(calpoints)

    only_warp = transformation.focal.normalize_image(source_image)
    imshow("only warp", only_warp)

    transformed = transformation.transform_image(source_image)
    imshow("undistort and warp", transformed)

    waitKey(0)


def cleanup_matrix(matrix_array: Matrix) -> str:
    matrix_string = str(matrix_array)
    matrix_string = matrix_string.replace("[", "")
    matrix_string = matrix_string.replace("]", "")
    return matrix_string.replace("\n", "")


def get_args() -> Any:
    parser = ArgumentParser(description="yolo")
    parser.add_argument(
        "-n",
        "--name",
        type=str,
        required=True,
        help="camera name to calibrate",
    )

    parser.add_argument(
        "-s",
        "--show",
        default=False,
        action="store_true",
        help="show the transformed image after calibration",
    )

    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="path to config folder",
    )

    return parser.parse_args()


def get_calibration_test_points() -> CalPoints:
    point = CalPoints(
        target_points=[
            Point(1, 1),
            Point(1, 2),
            Point(2, 2),
            Point(2, 1),
        ],
        source_points=[
            Point(1, 1),
            Point(1, 2),
            Point(2, 2),
            Point(2, 1),
        ],
    )
    return point  # noqa: RET504


def point_to_str(points: list[Point]) -> str:
    point_str = ""
    for point in points:
        point_str += f"({point.x},{point.y}),"
    return point_str[:-1].replace(",", ", ")


def main() -> None:
    global CAMERA_NAME, CAMERA_ID, FILE_PATH
    args = get_args()

    CAMERA_NAME = args.name
    config_path = Path(args.config)
    map_definition_path = config_path.joinpath("calibration/map/map.yaml")
    calibration_image_path = config_path.joinpath("calibration/calibration.jpg")

    sand_config = get_config(config_path)

    with map_definition_path.open(encoding="utf-8") as map_definition_file:
        map_config = yaml.load(map_definition_file, Loader=yaml.FullLoader)

    if calibration_image_path.exists():
        print(f"read calibration image from file {calibration_image_path.as_posix()}")
        target_image = imread(calibration_image_path.absolute().as_posix())
    else:
        print("generate calibration image")
        target_image = generate_calibration_image(map_config)
    CAMERA_ID = get_camera_id(sand_config, CAMERA_NAME)
    camera_image_path = get_path_from_camera_name(
        CAMERA_NAME, folder=config_path.joinpath("calibration/camera")
    )
    if not camera_image_path.exists():
        print("camera image doesnt exist")
        sys.exit(1)
    FILE_PATH = (
        get_path_from_camera_name(
            CAMERA_NAME, folder=config_path.joinpath("calibration/camera")
        )
        .absolute()
        .as_posix()
    )
    source_image = imread(FILE_PATH)
    border_size = int(source_image.shape[0] / 10)

    camera_config = sand_config.cameras[CAMERA_ID]
    counter = Counter()

    nullpoint_x = map_config["nullpoint"]["horizontal"]
    nullpoint_y = map_config["nullpoint"]["vertical"]

    for point_config in map_config["calibration_points"]:
        point = Point(
            point_config["horizontal"] + nullpoint_x,
            point_config["vertical"] + nullpoint_y,
        )
        point_list.append(point)

    print(f"file_path: {FILE_PATH}")
    image = imread(FILE_PATH)
    if image is None:
        print(f"image can't be read {image=}")
        sys.exit()
    print(f"FILE DIMENSIONS: {image.shape}")

    if CAMERA_ID < 0:
        print("no helper for camera found")
        sys.exit()

    cal_points = CalPoints(target_points=[], source_points=[])
    print(f"run map_image with camera {CAMERA_NAME}")
    print("")
    print("use Mouse to set calibration points in the Images")
    print("use WASD to move the last Point in source Image")
    print("on target Image the point will be automaticly the nearest crosshair")
    print("")
    map_image(camera_config, target_image, cal_points, counter, border_size)

    if args.show:
        show_image(FILE_PATH, cal_points, camera_config)

    destroyAllWindows()
    print()
    print("calibration data is not written to config file. Please do it yourself :-)")
    print()
    print(f'transformation_source_points: "[{point_to_str(cal_points.source_points)}]"')
    print(f'transformation_target_points: "[{point_to_str(cal_points.target_points)}]"')

    height, width = source_image.shape[:2]
    print(f"transformation_source_resolution_str: {width}x{height}")
    print()
    print()


if __name__ == "__main__":
    main()
