from __future__ import annotations

import time
from collections import defaultdict
from collections.abc import Sequence
from pathlib import Path
from threading import Lock
from time import sleep
from typing import Dict, cast

import yaml
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.model import Model, ObjectDetectionModel
from mlcvzoo_base.models.model_registry import ModelRegistry

from sand.config import ConstantConfig, NeuralNetworkConfig, SandConfig
from sand.datatypes import EnrichedFrame, SandBoxes
from sand.interfaces.config import ConfigurationManager
from sand.interfaces.synchronization import SandNode
from sand.interfaces.util import EnrichedSubscriber, NamedCollectAble
from sand.registry import get_singleton_node
from sand.util.time_management import TimeManagement


class StreamList:
    def __init__(self, streams: list[str]) -> None:
        self.all_streams = streams
        self.__current_index = 0

    def next(self) -> str:  # noqa: A003
        result = self.all_streams[self.__current_index]
        # round-robin
        self.__current_index = (self.__current_index + 1) % len(self.all_streams)
        return result


class NeuralNetwork(
    EnrichedSubscriber, SandNode, ConfigurationManager[NeuralNetworkConfig]
):
    def __init__(
        self,
        collectables: Sequence[NamedCollectAble[EnrichedSubscriber]],
        global_config: SandConfig,
    ) -> None:
        SandNode.__init__(self, global_config.communication)
        ConfigurationManager.__init__(self, self, global_config)

        self.stream_list = StreamList([cs.get_name() for cs in collectables])
        self.images: defaultdict[str, EnrichedFrame | None] = defaultdict(lambda: None)
        self.constant_config = get_singleton_node(ConstantConfig)
        self.image_lock = Lock()
        self.started = False

        self._time_management = TimeManagement(
            len(collectables) * self.config.max_fps_per_camera,
            slowdown_factor=1,
        )

        self.create_thread(
            target=self.work,
            args=(),
            name=NeuralNetwork.__name__,
            start=False,
        )

        if (
            not self.config.demo
            and self.config.active
            and self.config.model_config is not None
        ):
            replacement_map = self.get_mlcvzoo_replacement_map(
                replacement_config_path=Path(self.config.replacement_config_path)
                if self.config.replacement_config_path is not None
                else None
            )

            model_registry: ModelRegistry = ModelRegistry()  # type: ignore[type-arg]

            model: Model = model_registry.init_model(  # type: ignore[type-arg]
                model_config=self.config.model_config,
                string_replacement_map=replacement_map,
            )

            if not isinstance(model, ObjectDetectionModel):
                raise ValueError(
                    "Currently only models that inherit from "
                    "'mlcvzoo_base.api.model.ObjectDetectionModel' "
                    "are allowed in the neural.model_config"
                )

            self.model: ObjectDetectionModel = model  # type: ignore[type-arg]

            self.publish(
                payload=self.model.num_classes,
                topic=f"{NeuralNetwork.__name__}/all/data/num_classes",
                retain=True,
            )

        self.sources = [
            f"CameraReader_{camera_name}"
            for camera_name in self.stream_list.all_streams
        ]

        for collectable in collectables:
            collectable.subscribe(self)

    @staticmethod
    def get_mlcvzoo_replacement_map(
        replacement_config_path: Path | None,
    ) -> dict[str, str]:
        if replacement_config_path is None:
            return {}

        with replacement_config_path.open() as replacement_config:
            return cast(
                Dict[str, str],
                yaml.load(replacement_config, Loader=yaml.FullLoader),
            )

    def select_config(self, global_config: SandConfig) -> NeuralNetworkConfig:
        return global_config.neural

    def push_frame(self, frame: EnrichedFrame) -> None:
        if not self.config.active:
            return

        if not self.started:
            self.started = True
            self.start_all_threads()

        with self.image_lock:
            self.images[frame.camera_name] = frame

    def _wait_for_next_frame(self) -> bool:
        """Wait for the next frame time, if there is a maximum configured.

        Returns:
            True if it's time for the next frame

            False if shutdown occured
        """
        if self.config.max_fps_per_camera > 0:
            return self._time_management.wait_for_next_frame()

        return True

    def work(self) -> None:
        skip_counter = 0
        while not self.shutdown_event.is_set():
            if not self._wait_for_next_frame():
                self.log.d("Shutdown occured...", "work")
                break

            current_camera = self.stream_list.next()
            with self.image_lock:
                enriched_frame = self.images[current_camera]
                self.images[current_camera] = None

            if enriched_frame is None:
                if skip_counter > len(self.stream_list.all_streams):
                    self.log.i(
                        "Skipped all cameras once in a row, sleeping shortly...", "work"
                    )
                    # pause if we skipped all the current frames
                    # otherwise jump straight to the next one
                    skip_counter = 0
                    sleep(self.config.wait_time_no_image_available)
                skip_counter += 1
                continue

            skip_counter = 0

            boxes: list[BoundingBox]

            if self.config.demo:
                data = self.constant_config.demo_boxes[current_camera]

                boxes = [
                    BoundingBox(
                        class_identifier=ClassIdentifier(
                            class_name=box[0],
                            class_id=box[1],
                        ),
                        box=Box(
                            xmin=box[2][0],
                            ymin=box[2][1],
                            xmax=box[2][2],
                            ymax=box[2][3],
                        ),
                        difficult=box[3],
                        occluded=box[4],
                        content=box[5],
                        score=box[6],
                    )
                    for box in data
                ]
                time.sleep(0.5)
            else:
                assert isinstance(self.model, ObjectDetectionModel)
                _, bounding_boxes = self.model.predict(data_item=enriched_frame.frame)
                boxes = bounding_boxes

            box_msg = SandBoxes(
                enriched_frame.id,
                current_camera,
                enriched_frame.timestamp,
                boxes,  # type: ignore[arg-type]
                enriched_frame.width,
                enriched_frame.height,
            )

            self.publish(
                payload=box_msg,
                topic=f"{NeuralNetwork.__name__}/{current_camera}/data/boxes",
            )

            if self.config.log_detections:
                self.log.i(
                    message=f"Detections for n_{current_camera}: {boxes}",
                    tag="work",
                )

            if any(box.class_name.lower() == "person" for box in boxes):
                self.publish(
                    payload=enriched_frame.timestamp,
                    topic=f"{NeuralNetwork.__name__}/{current_camera}/data/interesting",
                    retain=True,
                )
