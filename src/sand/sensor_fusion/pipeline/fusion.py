from __future__ import annotations

from copy import copy
from typing import ClassVar

from sand.config import SandConfig, SensorFusionConfig
from sand.datatypes import Box, LidarPoints, Point, Topic, TransformedBoxes
from sand.datatypes.scale import Scale
from sand.datatypes.third_party.mlcvzoo import BoundingBox
from sand.datatypes.types import Dimensions
from sand.interfaces.config import ConfigurationManager
from sand.interfaces.synchronization import SandNode
from sand.sensor_fusion.checker import Checker
from sand.util.time_management import TimeManagement


class SensorFusion(SandNode, ConfigurationManager[SensorFusionConfig]):
    boxes: ClassVar[dict[Topic, TransformedBoxes]] = {}
    pointcloud2d: ClassVar[dict[str, LidarPoints]] = {}

    def __init__(self, global_config: SandConfig) -> None:
        SandNode.__init__(self, global_config.communication)
        ConfigurationManager.__init__(
            self,
            self,
            global_config,
            update_pattern=f"{ConfigurationManager.__name__}/.*/data/sensor_fusion/.*",
        )

        self._map_scale = Scale(
            self.global_config.cameras[0].transformation_target_resolution,
            Dimensions(
                self.global_config.map_builder.output_width,
                self.global_config.map_builder.output_height,
            ),
        )

        self.time_management = TimeManagement(
            fps=self.config.calc_per_seconds,
            slowdown_factor=1,
            shutdown_event=self.shutdown_event,
        )

        self.checker = Checker(self.config, global_config)

        self.source = ["BoxTransformer", "LidarPacketEnricher"]
        self.subscribe_topic(
            "BoxTransformer/+/data/transformed_boxes", self.push_box_frame
        )
        self.subscribe_topic(
            "LidarPacketEnricher/+/data/pointcloud2d",
            self.push_pointcloud2d,
        )

        self.create_thread(
            target=self.work,
            name=self.__class__.__name__,
            start=True,
        )

    def select_config(self, global_config: SandConfig) -> SensorFusionConfig:
        return global_config.sensor_fusion

    def config_has_updated(self) -> None:
        self.checker.reinit_data(self.config)

    def push_box_frame(self, topic: Topic, payload: TransformedBoxes) -> None:
        self.boxes[topic] = payload

    def push_pointcloud2d(self, topic: Topic, payload: LidarPoints) -> None:
        camera = topic.split("/")[1]
        self.pointcloud2d[camera] = payload

    def check_collision(self, box: Box, class_name: str) -> bool:
        try:
            if class_name == "person":
                return self.checker.test_person(box)
            return self.checker.test_box(box)
        except IndexError:
            self.log.exception("uncatched indexError", "check_collision")
            return False

    def test_box_list(
        self, box_list: list[Box], bounding_boxes: list[BoundingBox]
    ) -> bool:
        collision = False
        box_list_corrected = []
        for index, value in enumerate(box_list):
            # if index not in detections, than theres additional boxes, but not from the neural net. so we ignore them
            box_list_corrected.append(value)
            if index < len(bounding_boxes) and self.check_collision(
                value, bounding_boxes[index].class_name.lower()
            ):
                collision = True
        return collision

    def work(self) -> None:
        fusion_id = 0
        while not self.shutdown_event.is_set():
            # work only every 1/x seconds
            if not self.time_management.wait_for_next_frame():
                self.log.d("shutdown occurred", "work")
                break

            fusion_id += 1
            collisions = 0
            self.checker.reset_collision()

            for _, transformed_boxes in list(self.boxes.items()):
                collisions += int(
                    self.test_box_list(
                        transformed_boxes.transformed_boxes, transformed_boxes.boxes
                    )
                )

            for _, pointcloud in copy(self.pointcloud2d).items():
                for point in pointcloud:
                    point_x = int((point[0] * 100) * self._map_scale.scale_width)
                    point_y = int((point[1] * 100) * self._map_scale.scale_height)
                    self.checker.test_point(Point(point_x, point_y))
            self.publish(
                payload=self.checker.get_collision_map(),
                topic=f"{SensorFusion.__name__}/all/data/collision_map",
            )
            self.publish(
                payload=collisions > 0,
                topic=f"{SensorFusion.__name__}/all/data/collision",
                retain=True,
            )
            self.publish(
                payload=collisions,
                topic=f"{SensorFusion.__name__}/all/data/collision_count",
            )
            for key, heat_map in self.checker.get_heat_maps().items():
                heat_map.calc()
                self.publish(
                    payload=heat_map.current_state,
                    topic=f"{SensorFusion.__name__}/all/data/heat_map_{key}",
                )
