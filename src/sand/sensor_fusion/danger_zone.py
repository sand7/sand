from __future__ import annotations

from pathlib import Path
from xml.dom import minidom

from cv2 import fillPoly
from numpy import array, zeros

from sand.config import SandConfig
from sand.datatypes import Color, Image, Point
from sand.datatypes.scale import Scale


class DangerZone:
    def __init__(self, config: SandConfig, object_color: str, scale: Scale) -> None:
        self.scale = scale
        self.config = config
        config_folder = Path(self.config.config_folder)
        svg_file_name = self.config.sensor_fusion.danger_zones.svg_file
        self.polygons = self.__load_svg(
            config_folder.joinpath(svg_file_name).absolute().as_posix(), object_color
        )
        self.danger_zone: list[list[int]] = self.build_lookup_table(self.polygons)

    def __load_svg(self, svg_path: str, color: str) -> list[list[Point]]:
        doc = minidom.parse(svg_path)
        view_config = doc.getElementsByTagName("sodipodi:namedview")
        unit = view_config[0].getAttribute("units")
        scale = 10 if unit == "cm" else 1

        polygons: list[list[Point]] = []
        for rect in doc.getElementsByTagName("rect"):
            rect_color = rect.getAttribute("style").split(";")[2].split(":")[1]
            if color == rect_color:
                width = int(float(rect.getAttribute("width")) * scale)
                height = int(float(rect.getAttribute("height")) * scale)
                x_pos = int(float(rect.getAttribute("x")) * scale)
                y_pos = int(float(rect.getAttribute("y")) * scale)
                poly: list[Point] = [
                    Point(x_pos, y_pos),
                    Point(x_pos + width, y_pos),
                    Point(x_pos + width, y_pos + height),
                    Point(x_pos, y_pos + height),
                ]
                polygons.append(poly)
        return polygons

    def build_lookup_table(self, polygons: list[list[Point]]) -> list[list[int]]:
        lookup_map = self.get_empty_image()
        for poly in polygons:
            scaled_poly = []
            for point in poly:
                scaled_poly.append(
                    Point(
                        int(point.x * self.scale.scale_width),
                        int(point.y * self.scale.scale_height),
                    )
                )
            lookup_map = fillPoly(
                lookup_map, pts=[array(scaled_poly)], color=Color(255, 0, 0)
            )
        lookup_table = []
        for column in lookup_map:
            rows = []
            for row in column:
                rows.append(1 if row[0] == 255.0 else 0)
            lookup_table.append(rows)
        return lookup_table

    # noinspection PyTypeChecker
    def get_empty_image(self) -> Image:
        return zeros(
            (
                self.config.sensor_fusion.output_height,
                self.config.sensor_fusion.output_width,
                3,
            )
        )
