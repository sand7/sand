from __future__ import annotations

from sand.config import CameraConfig, GroupConfig, SandConfig


def get_camera_config_by_camera_name(
    global_config: SandConfig, camera_name: str
) -> CameraConfig | None:
    for cam in global_config.cameras:
        if cam.name == camera_name:
            return cam
    return None


def get_group_config_by_group_name(
    global_config: SandConfig, group_name: str
) -> GroupConfig | None:
    for group in global_config.groups:
        if group.name == group_name:
            return group
    return None
