from __future__ import annotations

from threading import Event
from time import time


class TimeManagement:
    """Helper class to easily manage the targeted FPS for a component.

    Inherently it does timemanagement with a best effort approach that is not
    intended to be super accurate. Because of that we are using a rather basic
    {Event.wait} approach with the usual accuracy problems.

    If you want to have more accurate control about timing you can adjust
    {sleep_time_fraction}, which basically configures how often we check for the
    time of the next frame.

    If that is not enough, you will have to implement your own timing solution.
    """

    def __init__(
        self,
        fps: int,
        slowdown_factor: int,
        sleep_time_fraction: int = 2,
        shutdown_event: Event | None = None,
    ) -> None:
        """
        Args:
            fps (int): the original targeted fps
            slowdown_factor (int): how much do you want to slowdown from the
                                   targeted fps; think {fps / slowdown_factor}
            sleep_time_fraction (int): configure how often we should wake up to
                                       check the time; higher means more checks,
                                       more busy-waiting, higher accuracy
            shutdown_event (Event): if you have a shutdown_event and want the
                                    waiting to return as soon as the event is
                                    set; otherwise we create our own
        """
        if fps == 0:
            raise ValueError("fps is zero")
        self.shutdown_event = shutdown_event if shutdown_event is not None else Event()
        self.time_between_frames = 1 / (fps / slowdown_factor)
        self.start_time = time()
        self.frame_count = 0
        self.next_frame_time = self.start_time

        self.sleep_time = self.time_between_frames / sleep_time_fraction

    def is_time_for_next_frame(self) -> bool:
        return time() >= self.next_frame_time

    def increase_frame_count(self) -> None:
        self.frame_count += 1
        self.next_frame_time = (
            self.start_time + self.frame_count * self.time_between_frames
        )

    def get_sleep_time(self) -> float:
        return self.sleep_time

    def reset_time(self) -> None:
        self.frame_count = 0
        self.start_time = time()
        self.next_frame_time = self.start_time

    def wait_for_next_frame(self) -> bool:
        """Convenience method to wait until it's time for the next frame.

        It has limited precision that is mainly influenced by
        {sleep_time_fraction}, therefore if you need higher precision you should
        do it yourself.

        :returns
            True if it is time for the next frame

            False if a shutdown occurred
        """
        while not self.shutdown_event.is_set():
            if self.is_time_for_next_frame():
                self.increase_frame_count()
                return True

            self.shutdown_event.wait(self.get_sleep_time())
        return False
