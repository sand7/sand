"""
This module is used to source out data classes that are compatible to the MLCVZoo.
This data classes are needed in order to be decoupled from the MLCVZoo as dependency,
but this be compatible when the MLCVZoo is used. The structure of the data classes in
inspired by from https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo
"""
