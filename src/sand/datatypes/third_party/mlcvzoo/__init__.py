from __future__ import annotations

from .bounding_box import BoundingBox as BoundingBox
from .box import Box as Box
from .class_identifier import ClassIdentifier as ClassIdentifier
