from __future__ import annotations

from sand.config import CameraConfig, SandConfig
from sand.view.stream import StreamServer

from .encoder import FrameEncoder
from .reader import CameraReader


class CameraSystem:
    def __init__(
        self,
        reader_config: CameraConfig,
        sand_config: SandConfig,
        playback: bool,
    ) -> None:
        self.config = reader_config
        self.reader = CameraReader(sand_config, reader_config.name, playback)
        self.encoder = FrameEncoder(
            reader_config,
            sand_config.communication,
            self.reader,
        )

        if self.config.serve_stream:
            self.server = StreamServer(
                reader_config.name,
                reader_config.serve_port,
                reader_config.stream_resolution,
                sand_config.communication,
                self.reader,
            )

    def start(self) -> None:
        self.reader.start()
