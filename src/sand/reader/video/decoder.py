from __future__ import annotations

from multiprocessing.shared_memory import SharedMemory
from time import time

from numpy import uint8, zeros

from sand.config import CameraConfig, CommunicationConfig
from sand.datatypes import EnrichedFrame, Image
from sand.interfaces.synchronization import SandNode
from sand.interfaces.util import CollectAble, EnrichedSubscriber
from sand.util.time import now
from sand.util.time_management import TimeManagement

from .encoder import FrameEncoder


class FrameDecoder(SandNode, CollectAble[EnrichedSubscriber]):
    def __init__(
        self,
        config: CameraConfig,
        communication_config: CommunicationConfig,
    ) -> None:
        SandNode.__init__(self, communication_config)
        CollectAble.__init__(self)

        self.config = config

        self.source = FrameEncoder.__name__
        self.time_management = TimeManagement(1, slowdown_factor=1)
        self.cached_frame = EnrichedFrame(
            self.config.name,
            now(),
            self.__get_fail_image(self.config),
        )

        self._shared_buffer: SharedMemory | None = None
        self.log_broken_frame = self.log.d

        self.create_thread(
            target=self.decode_frames,
            name=f"fd_{self.config.name}",
            start=True,
        )

    @staticmethod
    def __get_fail_image(camera_config: CameraConfig) -> Image:
        image: Image = zeros(
            (
                camera_config.stream_resolution.height,
                camera_config.stream_resolution.width,
                3,
            ),
            dtype=uint8,
        )
        # make image red in BGR
        image[:, :, 2] = 255
        return image

    def _wait_until_shared_memory_available(
        self, timeout: int = 20
    ) -> SharedMemory | None:
        timeout_time = time() + timeout
        while not self.shutdown_event.is_set():
            try:
                return SharedMemory(name=self.config.name, create=False)
            except FileNotFoundError:
                if time() > timeout_time:
                    self.log.d(
                        f"Timeout while waiting for shared_memory for {self.config.name=}",
                        "_wait_until_shared_memory_available",
                    )
                    return None

                self.log.d(
                    f"Still waiting on shared_memory for {self.config.name=}",
                    "_wait_until_shared_memory_available",
                )
                self.shutdown_event.wait(5)  # check timeout; return on timeout

        self.log.i(
            "Shutdown while waiting on shared_memory",
            "_wait_until_shared_memory_available",
        )
        return None

    def shutdown(self) -> None:
        if self._shared_buffer is not None:
            self._shared_buffer.close()

    def __send(self) -> None:
        if self.cached_frame is not None:
            for subscriber in self.subscribers:
                subscriber.push_frame(self.cached_frame)

    def __try_creating_shared_memory(self) -> None:
        if self._shared_buffer is not None:
            return

        self._shared_buffer = self._wait_until_shared_memory_available()
        if self._shared_buffer is None:
            self.log.i("SharedMemory did not come up yet", "decode_frames")

    def decode_frames(self) -> None:
        while not self.shutdown_event.is_set():
            self.__try_creating_shared_memory()

            if not self.time_management.wait_for_next_frame():
                self.log.d("Shutdown for FrameDecoder", "decode_frames")
                break

            if self._shared_buffer is not None:
                try:
                    self.cached_frame = EnrichedFrame.from_bytes(
                        self._shared_buffer.buf
                    )
                    # at least one frame worked, switch log-levels
                    self.log_broken_frame = self.log.exception
                except Exception:
                    self.log_broken_frame(
                        "exception when reading frame from buffer", "decode_frames"
                    )

            self.__send()
