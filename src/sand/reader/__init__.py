"""The reader module provides services to read the unformatted datastreams from anywhere and provides a single point of
access for the system.

As other modules it is divided into, :mod:`lidar` / 3D and :mod:`video` / visual data.
"""
