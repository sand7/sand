"""The view model provides different visualisations for what the system does and more importantly sees.

The module is divided in the usual two parts, :mod:`lidar` for 3D data and :mod:`video` for the visual data.
"""
