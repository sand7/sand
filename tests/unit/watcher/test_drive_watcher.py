from __future__ import annotations

import pickle
from datetime import datetime
from pathlib import Path
from shutil import rmtree
from unittest.mock import MagicMock, patch

import pytest
from sand.config.config import DriveWatcherConfig, SandConfig
from sand.watcher.pipeline.drive_watcher import DriveWatcher


def _create_config() -> DriveWatcherConfig:
    return DriveWatcherConfig(
        memory_remaining_gb=10,
        segment_length_secs=10,
        folders=["folder1"],
    )


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(watcher=_create_config())


@pytest.fixture()
def path_change_message() -> MagicMock:
    msg = MagicMock()
    msg.topic = "DriveWatcher/segment"
    msg.payload = b"testpath"
    return msg


def teardown_function() -> None:
    folder = _create_config().folders[0]
    rmtree(f"{folder}", ignore_errors=True)


def test_should_start_thread_on_init(
    thread_constructor_mock: MagicMock, thread_mock: MagicMock, config: SandConfig
) -> None:
    assert not thread_constructor_mock.called

    DriveWatcher(config)

    assert thread_constructor_mock.called
    assert thread_mock.start.called


def test_should_do_nothing_when_shutdown(config: SandConfig) -> None:
    watcher = DriveWatcher(config)

    watcher.shutdown()
    watcher.get_folder = MagicMock()

    assert not watcher.get_folder.called

    watcher.watch()

    assert not watcher.get_folder.called


@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_return_folder1_if_enough_memory_available(
    disk_usage_mock: MagicMock,
    config: SandConfig,
) -> None:
    higher_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 + 1
    )
    disk_usage_mock.return_value = (
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
    )

    watcher = DriveWatcher(config)

    result = watcher.get_folder()

    assert result == Path(config.watcher.folders[0])


@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_return_none_if_not_enough_memory_available_in_all_folders(
    disk_usage_mock: MagicMock,
    config: SandConfig,
) -> None:
    lower_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 - 1
    )
    disk_usage_mock.return_value = (
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
    )

    watcher = DriveWatcher(config)

    result = watcher.get_folder()

    assert result is None


@pytest.mark.real_threads()
@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_signal_kill_to_own_process_when_no_folder_found(
    disk_usage_mock: MagicMock,
    communication_mock: MagicMock,
) -> None:
    memory_target = 10
    lower_than_memory_target_bytes = memory_target * 1024 * 1024 * 1024 - 1
    disk_usage_mock.return_value = (
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
    )
    watcher = DriveWatcher(
        SandConfig(
            watcher=DriveWatcherConfig(
                active=True,
                memory_remaining_gb=memory_target,
                segment_length_secs=10,
                folders=["folder1"],
            )
        )
    )
    watcher.shutdown()

    assert watcher.get_folder() is None
    assert communication_mock.publish.called


@patch("sand.watcher.pipeline.drive_watcher.now")
@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_should_create_sub_folder_with_datetime_as_name(
    disk_usage_mock: MagicMock,
    now_mock: MagicMock,
    config: SandConfig,
) -> None:
    higher_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 + 1
    )
    disk_usage_mock.return_value = (
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
    )
    current_date = datetime.fromtimestamp(0)
    now_mock.return_value = current_date

    watcher = DriveWatcher(config)
    watcher.shutdown_event = MagicMock()
    watcher.shutdown_event.is_set.side_effect = [False, True]

    watcher.watch()

    assert Path(
        f"{config.watcher.folders[0]}/recordings/uncategorized/{current_date.strftime('%Y-%m-%dT%H:%M:%S')}"
    ).exists()


@patch("sand.watcher.pipeline.drive_watcher.now")
@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_should_notify_subscriber_on_folder_change(
    disk_usage_mock: MagicMock,
    now_mock: MagicMock,
    config: SandConfig,
    communication_mock: MagicMock,
) -> None:
    higher_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 + 1
    )
    disk_usage_mock.return_value = (
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
    )
    current_date = datetime.fromtimestamp(0)
    now_mock.return_value = current_date
    current_folder = Path(
        f"{config.watcher.folders[0]}/recordings/uncategorized/{current_date.strftime('%Y-%m-%dT%H:%M:%S')}"
    )

    watcher = DriveWatcher(config)
    watcher.shutdown_event = MagicMock()
    watcher.shutdown_event.is_set.side_effect = [False, True]

    watcher.watch()

    assert communication_mock.publish.call_count == 2

    checked_segment = False
    checked_moment = False
    for _, publish_call_kwargs in communication_mock.publish.call_args_list:
        if publish_call_kwargs["topic"] == "DriveWatcher/all/data/segment":
            checked_segment = True

            assert (
                pickle.loads(publish_call_kwargs["payload"])
                == current_folder.absolute().as_posix()
            )
            assert publish_call_kwargs["retain"] is True
            assert publish_call_kwargs["qos"] == 1

        elif publish_call_kwargs["topic"] == "DriveWatcher/all/data/moment":
            checked_moment = True

            assert (
                pickle.loads(publish_call_kwargs["payload"])
                == current_folder.parent.parent.joinpath("moments")
                .absolute()
                .as_posix()
            )
            assert publish_call_kwargs["retain"] is True
            assert publish_call_kwargs["qos"] == 0

    assert checked_segment
    assert checked_moment


@patch("sand.watcher.pipeline.drive_watcher.now")
@patch("sand.watcher.pipeline.drive_watcher.disk_usage")
def test_should_reorder_folders_if_second_folder_is_chosen(
    disk_usage_mock: MagicMock,
    now_mock: MagicMock,
    config: SandConfig,
) -> None:
    higher_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 + 1
    )
    lower_than_memory_target_bytes = (
        config.watcher.memory_remaining_gb * 1024 * 1024 * 1024 - 1
    )

    disk_util_result_higher = (
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
        higher_than_memory_target_bytes,
    )
    disk_util_result_lower = (
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
        lower_than_memory_target_bytes,
    )

    folders = {
        "folder1": disk_util_result_lower,
        "folder2": disk_util_result_higher,
    }

    config.watcher.folders = list(folders.keys())

    disk_usage_mock.side_effect = lambda folder: folders[folder]
    current_date = datetime.fromtimestamp(0)
    now_mock.return_value = current_date

    watcher = DriveWatcher(config)

    result1 = watcher.get_folder()

    assert result1 == Path("folder2")

    # setting both folders to have memory again
    folders = {
        "folder1": disk_util_result_higher,
        "folder2": disk_util_result_higher,
    }

    result2 = watcher.get_folder()

    assert result2 == Path("folder2")


def test_worker_should_ignore_exceptions_in_subscribers(
    communication_mock: MagicMock, config: SandConfig
) -> None:
    subscriber = MagicMock()
    subscriber.path_change.side_effect = Exception("something happened")

    sut = DriveWatcher(config)

    try:
        sut.current_folder = Path("something_new")
    except Exception as e:
        pytest.fail(f"Exception {e} was raised")

    assert communication_mock.publish.called


@pytest.mark.skip(reason="keeps hanging in the CI pipeline")
def test_worker_should_kill_system_when_no_permission_to_create_folders(
    communication_mock: MagicMock,
    config: SandConfig,
    tmp_path: Path,
) -> None:
    target_path = tmp_path.joinpath("somewhere")
    target_path.mkdir(mode=555, parents=True)
    config.watcher.folders = [target_path.absolute().as_posix()]

    sut = DriveWatcher(config)

    assert communication_mock.publish.call_count == 0

    sut.watch()

    assert communication_mock.publish.call_count == 1
