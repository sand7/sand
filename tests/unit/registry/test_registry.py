from __future__ import annotations

from unittest.mock import MagicMock, patch

import pytest
from sand.registry import RegisterAble, get_node_count, get_nodes, get_singleton_node


@patch("sand.registry.registry._nodes", new=[])
def test_register_able_adds_nodes() -> None:
    # import only after patch is through, otherwise we have wrong instance
    from sand.registry.registry import _nodes

    register_able = RegisterAble()

    assert len(_nodes) == 1
    assert _nodes[0] is register_able


@patch("sand.registry.registry._nodes", new=[])
def test_register_able_adds_node_if_executed_later() -> None:
    from sand.registry.registry import _nodes

    sut = MagicMock()

    assert len(_nodes) == 0

    RegisterAble.__init__(sut)

    assert len(_nodes) == 1
    assert _nodes[0] is sut


@patch("sand.registry.registry._nodes", new=[])
def test_get_empty_list_if_nothing_is_in_the_list() -> None:
    from sand.registry.registry import _nodes

    assert len(_nodes) == 0

    ret_val = get_nodes(type(object))

    assert ret_val == []


@patch("sand.registry.registry._nodes", new=[])
def test_get_list_with_result_if_found() -> None:
    sut = MagicMock()
    RegisterAble.__init__(sut)

    result = get_nodes(MagicMock)

    assert len(result) == 1
    assert result[0] is sut


@patch("sand.registry.registry._nodes", new=[])
def test_get_empty_list_if_specific_type_is_not_in_list() -> None:
    sut = MagicMock()
    RegisterAble.__init__(sut)

    result = get_nodes(RegisterAble)

    assert len(result) == 0


@patch("sand.registry.registry._nodes", new=[])
def test_get_singleton_node_gets_a_single_node() -> None:
    sut = MagicMock()
    RegisterAble.__init__(sut)

    result = get_singleton_node(MagicMock)

    assert result is sut


@patch("sand.registry.registry._nodes", new=[])
def test_get_singleton_node_raises_if_no_node_is_there() -> None:
    sut = MagicMock()
    RegisterAble.__init__(sut)

    with pytest.raises(ValueError, match=str(RegisterAble)):
        get_singleton_node(RegisterAble)


@patch("sand.registry.registry._nodes", new=[])
def test_node_count_should_get_correct_length() -> None:
    from sand.registry.registry import _nodes

    assert len(_nodes) == 0
    assert get_node_count() == 0

    sut = MagicMock()
    RegisterAble.__init__(sut)

    assert len(_nodes) == 1
    assert get_node_count() == 1
