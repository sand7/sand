from __future__ import annotations

from datetime import datetime
from typing import NamedTuple
from unittest.mock import MagicMock

import pytest
from numpy import zeros
from pytest_mock import MockerFixture
from sand.datatypes import EnrichedFrame


class MqttConstructorMocks(NamedTuple):
    shutdown_able: MagicMock
    communication: MagicMock


@pytest.fixture(autouse=True)
def mqtt_mock(mocker: MockerFixture) -> MqttConstructorMocks:
    return MqttConstructorMocks(
        shutdown_able=mocker.patch("sand.interfaces.shutdown.shutdown_able.Client"),
        communication=mocker.patch("sand.interfaces.communication.mqtt_util.Client"),
    )


@pytest.fixture()
def shutdown_communication_mock(mqtt_mock: MqttConstructorMocks) -> MagicMock:
    return mqtt_mock.shutdown_able.return_value


@pytest.fixture()
def communication_mock(mqtt_mock: MqttConstructorMocks) -> MagicMock:
    return mqtt_mock.communication.return_value


@pytest.fixture(autouse=True)
def thread_constructor_mock(
    request: pytest.FixtureRequest, mocker: MockerFixture
) -> MagicMock | None:
    if "real_threads" in request.keywords:
        return None
    return mocker.patch("sand.interfaces.synchronization.node.Thread")


@pytest.fixture()
def thread_mock(thread_constructor_mock: MagicMock) -> MagicMock:
    return thread_constructor_mock.return_value


class SharedMemoryMocks(NamedTuple):
    encoder: MagicMock
    decoder: MagicMock


@pytest.fixture(autouse=True)
def shared_memory_mock(mocker: MockerFixture) -> SharedMemoryMocks:
    return SharedMemoryMocks(
        encoder=mocker.patch("sand.reader.video.encoder.SharedMemory"),
        decoder=mocker.patch("sand.reader.video.decoder.SharedMemory"),
    )


@pytest.fixture()
def shared_encoder_mock(shared_memory_mock: SharedMemoryMocks) -> MagicMock:
    return shared_memory_mock.encoder


@pytest.fixture()
def shared_decoder_mock(shared_memory_mock: SharedMemoryMocks) -> MagicMock:
    return shared_memory_mock.decoder


@pytest.fixture()
def camera_name() -> str:
    return "f1_l1_v1"


@pytest.fixture()
def enriched_frame(camera_name: str) -> EnrichedFrame:
    return EnrichedFrame(
        camera_name,
        datetime.utcfromtimestamp(0),
        zeros((100, 100, 3)),
    )
