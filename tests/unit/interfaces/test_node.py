from __future__ import annotations

from threading import Thread
from unittest.mock import ANY, MagicMock, call

import pytest
from sand.config import CommunicationConfig
from sand.interfaces.synchronization import SandNode


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


def test_creates_no_thread_per_default(
    communication_config: CommunicationConfig,
) -> None:
    node = SandNode(communication_config)

    assert len(node._threads) == 0


def test_create_thread_without_starting_it(
    thread_constructor_mock: MagicMock, communication_config: CommunicationConfig
) -> None:
    mocked_thread = thread_constructor_mock.return_value

    sut = SandNode(communication_config)
    sut.add_thread = MagicMock()

    callable_mock = MagicMock()
    callable_mock.__name__ = "callable_name"

    sut.create_thread(target=callable_mock, start=False)

    assert thread_constructor_mock.call_count == 1
    assert sut.add_thread.call_count == 1
    assert not mocked_thread.start.called

    thread_constructor_mock.assert_has_calls(
        [call(target=callable_mock, args=ANY, name="SandNode", daemon=False)]
    )
    sut.add_thread.assert_has_calls([call(mocked_thread)])


def test_create_thread_with_starting_it(
    thread_constructor_mock: MagicMock, communication_config: CommunicationConfig
) -> None:
    mocked_thread = thread_constructor_mock.return_value

    sut = SandNode(communication_config)
    sut.add_thread = MagicMock()

    callable_mock = MagicMock()
    callable_mock.__name__ = "callable_name"

    sut.create_thread(target=callable_mock, start=True)

    assert thread_constructor_mock.call_count == 1
    assert sut.add_thread.call_count == 1
    assert mocked_thread.start.call_count == 1

    thread_constructor_mock.assert_has_calls(
        [call(target=callable_mock, args=ANY, name="SandNode", daemon=False)]
    )
    sut.add_thread.assert_has_calls([call(mocked_thread)])


def test_create_thread_returns_created_thread(
    thread_constructor_mock: MagicMock, communication_config: CommunicationConfig
) -> None:
    mocked_thread = thread_constructor_mock.return_value

    sut = SandNode(communication_config)
    sut.add_thread = MagicMock()

    callable_mock = MagicMock()
    callable_mock.__name__ = "callable_name"

    thread = sut.create_thread(target=callable_mock, start=True)

    assert thread is mocked_thread


def test_create_thread_names_thread_to_given_value(
    thread_constructor_mock: MagicMock,
    communication_config: CommunicationConfig,
) -> None:
    test_name = "test_name"

    sut = SandNode(communication_config)
    sut.add_thread = MagicMock()

    callable_mock = MagicMock()
    callable_mock.__name__ = "callable_name"

    sut.create_thread(target=callable_mock, start=True, name=test_name)

    thread_constructor_mock.assert_has_calls(
        [call(target=callable_mock, args=ANY, name=test_name, daemon=False)]
    )


def test_add_thread_to_threads(communication_config: CommunicationConfig) -> None:
    thread_mock = MagicMock(spec=Thread)

    sut = SandNode(communication_config)

    assert len(sut._threads) == 0

    sut.add_thread(thread_mock)

    assert len(sut._threads) == 1
    assert sut._threads[0] is thread_mock


def test_add_thread_returns_exact_thread(
    communication_config: CommunicationConfig,
) -> None:
    thread_mock = MagicMock(spec=Thread)

    sut = SandNode(communication_config)

    ret_val = sut.add_thread(thread_mock)

    assert ret_val is thread_mock


def test_get_thread_with_correct_name(
    communication_config: CommunicationConfig,
) -> None:
    test_name = "thread-1"
    thread_mock = MagicMock(spec=Thread)
    thread_mock.name = test_name

    sut = SandNode(communication_config)
    sut.add_thread(thread_mock)

    assert len(sut._threads) == 1

    result = sut.get_thread(test_name)

    assert result is thread_mock


def test_get_thread_results_in_none_if_not_found(
    communication_config: CommunicationConfig,
) -> None:
    test_name = "thread-1"
    thread_mock = MagicMock(spec=Thread)
    thread_mock.name = test_name

    sut = SandNode(communication_config)
    sut.add_thread(thread_mock)

    assert len(sut._threads) == 1

    result = sut.get_thread("invalid_name")

    assert result is None


def test_start_all_threads(communication_config: CommunicationConfig) -> None:
    thread_mock_1 = MagicMock(spec=Thread)
    thread_mock_2 = MagicMock(spec=Thread)

    sut = SandNode(communication_config)
    sut.add_thread(thread_mock_1)
    sut.add_thread(thread_mock_2)

    assert len(sut._threads) == 2
    assert not thread_mock_1.start.called
    assert not thread_mock_2.start.called

    sut.start_all_threads()

    assert thread_mock_1.start.called
    assert thread_mock_2.start.called


def test_shutdown_sets_shutdown_event(
    communication_config: CommunicationConfig,
) -> None:
    sut = SandNode(communication_config)

    assert not sut.shutdown_event.is_set()

    sut.shutdown()

    assert sut.shutdown_event.is_set()


def test_shutdown_joins_alive_threads(
    communication_config: CommunicationConfig,
) -> None:
    thread_mock = MagicMock(spec=Thread)
    thread_mock.is_alive.return_value = True

    sut = SandNode(communication_config)
    sut.add_thread(thread_mock)

    assert not thread_mock.is_alive.called
    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.is_alive.call_count == 1
    assert thread_mock.join.call_count == 1


def test_shutdown_does_not_join_dead_threads(
    communication_config: CommunicationConfig,
) -> None:
    thread_mock = MagicMock(spec=Thread)
    thread_mock.is_alive.return_value = False

    sut = SandNode(communication_config)
    sut.add_thread(thread_mock)

    assert not thread_mock.is_alive.called
    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.is_alive.call_count == 1
    assert not thread_mock.join.called


def test_start_all_threads_should_be_callable_more_than_once_without_crashing(
    communication_config: CommunicationConfig,
) -> None:
    thread = Thread()

    sut = SandNode(communication_config)
    sut.add_thread(thread)

    try:
        sut.start_all_threads()
        sut.start_all_threads()
        sut.start_all_threads()
    except RuntimeError as e:
        pytest.fail(f"Cannot start_all_threads multiple times: {e}")
