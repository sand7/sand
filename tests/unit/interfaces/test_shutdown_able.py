from __future__ import annotations

import contextlib
from typing import Any
from unittest.mock import ANY, MagicMock, call, patch

import pytest
from pytest_mock import MockerFixture
from sand.config import CommunicationConfig
from sand.interfaces.shutdown import ShutdownAble
from sand.registry import RegisterAble


@pytest.fixture()
def config() -> MagicMock:
    communication_config = MagicMock()

    communication_config.host = "127.0.0.1"

    return communication_config


@pytest.fixture()
def uuid(mocker: MockerFixture) -> str:
    uuid_function = mocker.patch("sand.interfaces.shutdown.shutdown_able.uuid4")
    uuid_function.return_value = "test_uuid"
    return uuid_function.return_value


class ExampleShutdownAbleClass(ShutdownAble):
    def __init__(self, config: CommunicationConfig) -> None:
        ShutdownAble.__init__(self, config)

    def shutdown(self) -> None:
        pass


def test_shutdown_able_should_execute_register_able_init(config: MagicMock) -> None:
    with patch.object(RegisterAble, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            ExampleShutdownAbleClass(config)

        assert init_mock.called


def test_shutdown_able_should_provide_an_unset_shutdown_event(
    config: MagicMock,
) -> None:
    sut = ExampleShutdownAbleClass(config)

    assert not sut.shutdown_event.is_set()


def test_shutdown_able_should_register_on_mqtt_shutdown_event(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
    uuid: str,
) -> None:
    assert not shutdown_communication_mock.publish.called

    ExampleShutdownAbleClass(config)

    assert shutdown_communication_mock.publish.called
    shutdown_communication_mock.publish.assert_has_calls(
        [
            call(
                topic=f"ShutdownAble/{uuid}/command/register_shutdown",
                payload=f"ExampleShutdownAbleClass|{uuid}",
                retain=False,
                qos=1,
            )
        ]
    )


def test_shutdown_able_should_set_will_to_unregister(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
    uuid: str,
) -> None:
    assert not shutdown_communication_mock.will_set.called

    ExampleShutdownAbleClass(config)

    assert shutdown_communication_mock.will_set.called
    shutdown_communication_mock.will_set.assert_has_calls(
        [
            call(
                topic=f"ShutdownAble/{uuid}/command/finished_shutdown",
                payload=f"ExampleShutdownAbleClass|{uuid}",
                retain=False,
                qos=1,
            )
        ]
    )


def test_shutdown_able_should_subscribes_to_shutdown_topic(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    assert not shutdown_communication_mock.subscribe.called

    ExampleShutdownAbleClass(config)

    assert shutdown_communication_mock.subscribe.called
    shutdown_communication_mock.subscribe.assert_has_calls(
        [call(ShutdownAble.get_shutdown_topic(), qos=1)]
    )


def test_shutdown_able_should_starts_listening_immediately(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    assert not shutdown_communication_mock.loop_start.called

    ExampleShutdownAbleClass(config)

    assert shutdown_communication_mock.loop_start.called


def test_shutdown_able_should_register_callback_on_shutdown_topic(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    assert not shutdown_communication_mock.message_callback_add.called

    ExampleShutdownAbleClass(config)

    assert shutdown_communication_mock.message_callback_add.called
    shutdown_communication_mock.message_callback_add.assert_has_calls(
        [call(ShutdownAble.get_shutdown_topic(), ANY)]
    )


def test_shutdown_able_callback_should_set_shutdown_event(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    sut = ExampleShutdownAbleClass(config)
    shutdown_lambda = shutdown_communication_mock.message_callback_add.call_args_list[
        0
    ][0][1]

    assert not sut.shutdown_event.is_set()

    shutdown_lambda()

    assert sut.shutdown_event.is_set()


def test_shutdown_able_callback_should_call_shutdown_after_event_is_set(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    sut = ExampleShutdownAbleClass(config)
    sut.shutdown = MagicMock()

    def check_event(*_: Any, **__: Any) -> None:
        assert sut.shutdown_event.is_set()

    sut.shutdown.side_effect = check_event
    shutdown_lambda = shutdown_communication_mock.message_callback_add.call_args_list[
        0
    ][0][1]

    assert not sut.shutdown.called

    shutdown_lambda()

    assert sut.shutdown.called


def test_shutdown_able_callback_should_publish_unregister_message(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
    uuid: str,
) -> None:
    ExampleShutdownAbleClass(config)

    shutdown_lambda = shutdown_communication_mock.message_callback_add.call_args_list[
        0
    ][0][1]
    shutdown_communication_mock.reset_mock()

    assert not shutdown_communication_mock.publish.called

    shutdown_lambda()

    assert shutdown_communication_mock.publish.called
    shutdown_communication_mock.publish.assert_has_calls(
        [
            call(
                topic=f"ShutdownAble/{uuid}/command/finished_shutdown",
                payload=f"ExampleShutdownAbleClass|{uuid}",
                retain=False,
                qos=1,
            )
        ]
    )


def test_shutdown_able_callback_should_stop_listening_and_disconnect(
    config: MagicMock,
    shutdown_communication_mock: MagicMock,
) -> None:
    ExampleShutdownAbleClass(config)

    shutdown_lambda = shutdown_communication_mock.message_callback_add.call_args_list[
        0
    ][0][1]
    shutdown_communication_mock.reset_mock()

    assert not shutdown_communication_mock.loop_stop.called
    assert not shutdown_communication_mock.disconnect.called

    shutdown_lambda()

    assert shutdown_communication_mock.loop_stop.called
    assert shutdown_communication_mock.disconnect.called
