from __future__ import annotations

from unittest.mock import MagicMock, call

import pytest
from sand.config import CommunicationConfig, LidarConfig, SandConfig
from sand.interfaces.communication import Communicator
from sand.interfaces.config import ConfigurationManager
from sand.interfaces.config.configuration_management import find_config


class ExampleConfigManagementClass(ConfigurationManager[LidarConfig]):
    def __init__(self, communicator: Communicator, global_config: SandConfig) -> None:
        ConfigurationManager.__init__(self, communicator, global_config)

    def select_config(self, global_config: SandConfig) -> LidarConfig:
        return global_config.lidars[0]


@pytest.fixture()
def lidar_name() -> str:
    return "f1_l1_la"


@pytest.fixture()
def global_config(lidar_name: str) -> SandConfig:
    return SandConfig(lidars=[LidarConfig(name=lidar_name)])


@pytest.fixture()
def communicator() -> Communicator:
    return Communicator(CommunicationConfig())


def test_configuration_management_should_subscribe_to_correct_topics(
    global_config: SandConfig,
    communicator: Communicator,
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.message_callback_add.called
    assert not communication_mock.subscribe.called

    ExampleConfigManagementClass(communicator, global_config)

    assert communication_mock.message_callback_add.called
    assert communication_mock.subscribe.called
    communication_mock.subscribe.assert_has_calls(
        [call(f"{ConfigurationManager.__name__}/+/data/#", qos=1)]
    )


def test_configuration_management_should_use_select_config_for_property(
    global_config: SandConfig,
    communicator: Communicator,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)
    sut.select_config = MagicMock()

    assert not sut.select_config.called

    actual = sut.config

    assert sut.select_config.called
    assert actual is sut.select_config.return_value


def test_configuration_management_should_set_new_simple_configurations(
    global_config: SandConfig,
    communicator: MagicMock,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)

    assert sut.global_config.publisher.active is False

    sut._set_new_config("ConfigurationManager/all/data/publisher/active", True)

    assert sut.global_config.publisher.active is True


def test_configuration_management_should_set_lidar_device_configurations(
    global_config: SandConfig,
    communicator: MagicMock,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)

    assert sut.global_config.lidars[0].transformation_list == "[0, 0, 0, 0]"

    sut._set_new_config(
        "ConfigurationManager/f1_l1_la/data/lidars/transformation_list", "[1, 1, 1, 1]"
    )

    assert sut.global_config.lidars[0].transformation_list == "[1, 1, 1, 1]"


def test_configuration_management_should_throw_assertion_error_if_non_existant_device(
    global_config: SandConfig,
    communicator: MagicMock,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)

    assert sut.global_config.lidars[0].transformation_list == "[0, 0, 0, 0]"

    with pytest.raises(AssertionError):
        sut._set_new_config(
            "ConfigurationManager/invalid/data/lidars/transformation_list",
            "[1, 1, 1, 1]",
        )

    assert sut.global_config.lidars[0].transformation_list == "[0, 0, 0, 0]"


def test_configuration_management_should_throw_attribute_error_when_path_is_invalid(
    global_config: SandConfig,
    communicator: MagicMock,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)

    with pytest.raises(AttributeError):
        sut._set_new_config(
            "ConfigurationManager/all/data/invalid/transformation_list",
            "[1, 1, 1, 1]",
        )


def test_configuration_management_should_swallow_error_but_change_nothing_if_tail_is_missing(
    global_config: SandConfig,
    communicator: MagicMock,
) -> None:
    sut = ExampleConfigManagementClass(communicator, global_config)

    assert sut.global_config.lidars[0].transformation_list == "[0, 0, 0, 0]"

    sut._set_new_config(
        "ConfigurationManager/f1_l1_la/data/lidars",
        "[1, 1, 1, 1]",
    )

    assert sut.global_config.lidars[0].transformation_list == "[0, 0, 0, 0]"


def test_configuration_management_should_provide_method_to_find_device_specific_config(
    global_config: SandConfig,
    lidar_name: str,
) -> None:
    assert find_config(lidar_name, global_config.lidars) is not None
    assert find_config(lidar_name, global_config.lidars).name == lidar_name

    assert find_config("invalid", global_config.lidars) is None

    assert find_config("irrelevant", []) is None
