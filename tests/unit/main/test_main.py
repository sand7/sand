from __future__ import annotations

from copy import copy
from typing import Any
from unittest.mock import MagicMock, call

import pytest
from pytest_mock import MockerFixture
from sand.main import Main


@pytest.fixture()
def event_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.interfaces.synchronization.node.Event")


@pytest.fixture()
def event_mock(event_class_mock: MagicMock) -> MagicMock:
    return event_class_mock.return_value


@pytest.fixture(autouse=True)
def sleep_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.main.main.sleep")


@pytest.fixture()
def register_message() -> MagicMock:
    msg = MagicMock()

    msg.payload = b"TestModule"
    msg.topic = "ShutdownAble/test_uuid/command/register_shutdown"

    return msg


@pytest.fixture()
def unregister_message(register_message: MagicMock) -> MagicMock:
    msg = copy(register_message)

    msg.topic = "ShutdownAble/test_uuid/command/finished_shutdown"

    return msg


def test_main_spin_does_nothing_when_shutdown() -> None:
    sut = Main(MagicMock())
    sut._shutdown_event.set()
    sut._handle = MagicMock()

    assert not sut._handle.called
    sut.spin()
    assert not sut._handle.called


def test_main_spin_executes_handle_after_short_delay_when_check_run() -> None:
    sut = Main(MagicMock())
    sut._shutdown_event = MagicMock()
    sut._shutdown_event.is_set.side_effect = [False, True]
    sut._handle = MagicMock()

    assert not sut._shutdown_event.wait.called
    assert not sut._handle.called
    sut.spin(check=True)
    assert not sut._shutdown_event.wait.assert_has_calls([call(10)])
    assert sut._handle.called
    sut._handle.assert_has_calls([call(6660, "")])


def test_main_spin_waits_for_an_hour_with_the_shutdown_event() -> None:
    sut = Main(MagicMock())
    sut._shutdown_event = MagicMock()
    sut._shutdown_event.is_set.side_effect = [False, True]
    sut._handle = MagicMock()

    assert not sut._handle.called
    assert not sut._shutdown_event.wait.called
    sut.spin()
    assert not sut._handle.called
    assert sut._shutdown_event.wait.call_count == 1
    sut._shutdown_event.wait.assert_has_calls([call(3600)])


def test_main_spin_calls_handle_when_keyboard_interrupt_is_thrown() -> None:
    sut = Main(MagicMock())
    sut._shutdown_event = MagicMock()
    sut._shutdown_event.is_set.side_effect = [False, True]
    sut._shutdown_event.wait.side_effect = KeyboardInterrupt("Testing")
    sut._handle = MagicMock()

    assert not sut._handle.called
    sut.spin()
    assert sut._handle.called
    sut._handle.assert_has_calls([call(666, "")])


def test_main_handle_sets_own_shutdown_event() -> None:
    sut = Main(MagicMock())
    sut._shutdown_event = MagicMock()
    sut._shutdown_event.is_set.side_effect = [False, True]

    assert not sut._shutdown_event.set.called
    sut._handle(666, "")
    assert sut._shutdown_event.set.called


def test_main_should_shutdown_listener_after_a_short_sleep(
    sleep_mock: MagicMock,
) -> None:
    logger_listener = MagicMock()

    sut = Main(logger_listener)
    sut._shutdown_event.set()

    assert not sleep_mock.called
    assert not logger_listener.shutdown.called

    sut.spin()

    assert sleep_mock.called
    sleep_mock.assert_has_calls([call(0.1)])
    assert logger_listener.shutdown.called


def test_main_should_listen_to_shutdown_registers(
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.subscribe.called

    Main(MagicMock())

    assert communication_mock.subscribe.called
    communication_mock.subscribe.assert_has_calls(
        [call("+/+/command/register_shutdown", qos=1)]
    )


def test_main_should_start_listening_immediately(communication_mock: MagicMock) -> None:
    assert not communication_mock.loop_start.called

    Main(MagicMock())

    assert communication_mock.loop_start.called


def test_main_should_register_on_message_callback(
    communication_mock: MagicMock,
) -> None:
    """Normal subscribe-callbacks do not get the "unset" of retained messages."""
    callback_before = communication_mock.on_message

    Main(MagicMock())

    assert callback_before is not communication_mock.on_message


def test_main_should_save_shutdown_able_registered(
    communication_mock: MagicMock,
    register_message: MagicMock,
) -> None:
    sut = Main(MagicMock())

    assert len(sut._shutdown_ables) == 0

    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1
    assert register_message.payload.decode("utf-8") in sut._shutdown_ables


def test_main_should_remove_shutdown_able_if_unregistered(
    communication_mock: MagicMock,
    register_message: MagicMock,
    unregister_message: MagicMock,
) -> None:
    sut = Main(MagicMock())

    assert len(sut._shutdown_ables) == 0

    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1

    communication_mock.on_message(communication_mock, None, unregister_message)

    assert len(sut._shutdown_ables) == 0


def test_main_should_set_event_when_shutdown_in_progress_and_no_shutdown_ables_left(
    communication_mock: MagicMock,
    register_message: MagicMock,
    unregister_message: MagicMock,
) -> None:
    sut = Main(MagicMock())
    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1
    assert not sut._all_clients_shutdown.is_set()

    sut._shutdown_in_progress = True
    communication_mock.on_message(communication_mock, None, unregister_message)

    assert len(sut._shutdown_ables) == 0
    assert sut._all_clients_shutdown.is_set()


def test_main_should_not_remove_unrecognized_shutdown_able(
    communication_mock: MagicMock,
    register_message: MagicMock,
) -> None:
    sut = Main(MagicMock())
    register_message2 = copy(register_message)
    register_message2.topic = "ShutdownAble/another_test_uuid/command/register_shutdown"
    register_message2.payload = b"another_test_uuid"

    unregister_message = copy(register_message)
    unregister_message.topic = (
        "ShutdownAble/completely_invalid_uuid/command/finished_shutdown"
    )
    unregister_message.payload = b"completely_invalid_uuid"

    assert len(sut._shutdown_ables) == 0

    communication_mock.on_message(communication_mock, None, register_message)
    communication_mock.on_message(communication_mock, None, register_message2)

    assert len(sut._shutdown_ables) == 2

    communication_mock.on_message(communication_mock, None, unregister_message)

    assert len(sut._shutdown_ables) == 2


def test_main_should_give_shutdown_ables_time_to_answer(
    communication_mock: MagicMock,
    register_message: MagicMock,
    unregister_message: MagicMock,
) -> None:
    sut = Main(MagicMock())

    sut._all_clients_shutdown.wait = MagicMock()
    sut._all_clients_shutdown.wait.side_effect = (
        lambda _: communication_mock.on_message(
            communication_mock, None, unregister_message
        )
    )
    sut._all_clients_shutdown.is_set = MagicMock()
    sut._all_clients_shutdown.is_set.return_value = True

    sut._shutdown_event.wait = MagicMock()
    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1

    sut.spin(check=True)

    assert len(sut._shutdown_ables) == 0
    assert sut._all_clients_shutdown.wait.called
    sut._all_clients_shutdown.wait.assert_has_calls([call(30)])


def test_main_should_give_shutdown_ables_time_but_ignore_them_in_time_limit(
    communication_mock: MagicMock,
    register_message: MagicMock,
) -> None:
    sut = Main(MagicMock())

    sut._all_clients_shutdown.wait = MagicMock()
    sut._shutdown_event.wait = MagicMock()
    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1

    sut.spin(check=True)

    assert len(sut._shutdown_ables) == 1
    assert sut._all_clients_shutdown.wait.called
    sut._all_clients_shutdown.wait.assert_has_calls([call(30)])


def test_main_should_catches_keyboardinterrupts_when_shutting_down(
    communication_mock: MagicMock,
    register_message: MagicMock,
) -> None:
    sut = Main(MagicMock())

    first_time = {"value": True}

    def raise_once(*_: Any) -> None:
        if first_time["value"]:
            first_time["value"] = False
            raise KeyboardInterrupt

    sut._all_clients_shutdown.wait = MagicMock()
    sut._all_clients_shutdown.wait.side_effect = raise_once
    sut._all_clients_shutdown.is_set = MagicMock()
    sut._all_clients_shutdown.is_set.return_value = True

    sut._shutdown_event.wait = MagicMock()
    communication_mock.on_message(communication_mock, None, register_message)

    assert len(sut._shutdown_ables) == 1

    sut.spin(check=True)

    assert len(sut._shutdown_ables) == 1
    assert sut._all_clients_shutdown.wait.call_count == 2
    sut._all_clients_shutdown.wait.assert_has_calls([call(30), call(30)])
