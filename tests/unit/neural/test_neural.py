from __future__ import annotations

import contextlib
from pathlib import Path
from unittest.mock import MagicMock, call, patch

import pytest
import yaml
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from pytest_mock import MockerFixture
from sand.config import ConstantConfig, NeuralNetworkConfig, SandConfig
from sand.datatypes import EnrichedFrame, SandBoxes
from sand.interfaces.synchronization import SandNode
from sand.neural.pipeline.neural import NeuralNetwork, StreamList


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        neural=NeuralNetworkConfig(
            active=True,
            demo=True,
        )
    )


@pytest.fixture(autouse=True)
def get_singleton_node_mock(mocker: MockerFixture) -> MagicMock:
    constant_config = MagicMock(spec=ConstantConfig)
    constant_config.subscribe = MagicMock()
    return mocker.patch(
        "sand.neural.pipeline.neural.get_singleton_node", return_value=constant_config
    )


@pytest.fixture()
def constant_config(get_singleton_node_mock: MagicMock) -> MagicMock:
    return get_singleton_node_mock.return_value


@pytest.fixture()
def collectables(mocker: MockerFixture, camera_name: str) -> list[MagicMock]:
    mocker.patch("sand.interfaces.communication.communicator.Client")
    collectable_mock = MagicMock()

    collectable_mock.get_name.return_value = camera_name

    return [collectable_mock]


@pytest.fixture()
def mmdetection_model_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.neural.pipeline.neural.MMDetectionModel")


@pytest.fixture(autouse=True)
def model_registry_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.neural.pipeline.neural.ModelRegistry")


@pytest.fixture(autouse=True)
def pickle_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.interfaces.communication.communicator.pickle")


@pytest.fixture()
def model_mock(
    model_registry_mock: MagicMock,
    enriched_frame: EnrichedFrame,
    mocker: MockerFixture,
) -> MagicMock:
    mocker.patch("sand.neural.pipeline.neural.ObjectDetectionModel", new=MagicMock)

    result = model_registry_mock.return_value.init_model.return_value
    result.num_classes.return_value = 5
    result.predict.return_value = (
        enriched_frame,
        [
            BoundingBox(
                box=Box(
                    xmin=500,
                    ymin=500,
                    xmax=700,
                    ymax=700,
                ),
                class_identifier=ClassIdentifier(
                    class_name="person",
                    class_id=4,
                ),
                difficult=False,
                occluded=False,
                content="",
                score=1.0,
            ),
            BoundingBox(
                box=Box(
                    xmin=1000,
                    ymin=1000,
                    xmax=1200,
                    ymax=1200,
                ),
                class_identifier=ClassIdentifier(
                    class_name="person",
                    class_id=4,
                ),
                difficult=False,
                occluded=False,
                content="",
                score=1.0,
            ),
        ],
    )
    return result


@pytest.fixture(autouse=True)
def sleep_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.neural.pipeline.neural.sleep")


def test_camera_list_should_be_a_ring_buffer() -> None:
    string_list = ["one", "two", "three"]

    sut = StreamList(string_list)

    for index in range(10):
        assert sut.next() == string_list[index % len(string_list)]


def test_neural_calls_sand_node(
    config: SandConfig,
    collectables: list[MagicMock],
) -> None:
    with patch.object(SandNode, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            NeuralNetwork(global_config=config, collectables=collectables)

        assert init_mock.called


def test_neural_creates_thread_in_init_and_not_starts_it(
    config: SandConfig,
    thread_constructor_mock: MagicMock,
    collectables: list[MagicMock],
) -> None:
    assert not thread_constructor_mock.called
    assert not thread_constructor_mock.return_value.start.called

    NeuralNetwork(global_config=config, collectables=collectables)

    assert thread_constructor_mock.called
    assert not thread_constructor_mock.return_value.start.called


def test_neural_subscribes_to_all_camera_system_readers(
    config: SandConfig,
    collectables: list[MagicMock],
) -> None:
    collectables.extend([MagicMock(), MagicMock(), MagicMock()])

    assert not all(cs.subscribe.called for cs in collectables)

    NeuralNetwork(global_config=config, collectables=collectables)

    assert all(cs.subscribe.called for cs in collectables)


@pytest.mark.usefixtures("model_mock")
def test_neural_should_create_model_registry_when_active(
    config: SandConfig,
    collectables: list[MagicMock],
    model_registry_mock: MagicMock,
) -> None:
    config.neural.model_config = {
        "class_type": "mmdetection",
        "constructor_parameters": {
            "from_yaml": "./path/to/yaml",
            "init_for_inference": True,
        },
    }
    config.neural.replacement_config_path = None

    config.neural.demo = False

    assert not model_registry_mock.called
    model_registry_mock.init_model.return_value.__class__ = MagicMock

    NeuralNetwork(global_config=config, collectables=collectables)

    assert model_registry_mock.called
    model_registry_mock.return_value.init_model.assert_called_once_with(
        model_config=config.neural.model_config,
        string_replacement_map={},
    )


def test_neural_should_not_create_model_registry_when_not_active(
    config: SandConfig,
    collectables: list[MagicMock],
    model_registry_mock: MagicMock,
) -> None:
    config.neural.active = False

    assert not model_registry_mock.called

    NeuralNetwork(global_config=config, collectables=collectables)

    assert not model_registry_mock.called


def test_neural_should_not_create_model_registry_when_in_demo_mode(
    config: SandConfig,
    collectables: list[MagicMock],
    model_registry_mock: MagicMock,
) -> None:
    config.neural.demo = False

    assert not model_registry_mock.called

    NeuralNetwork(global_config=config, collectables=collectables)

    assert not model_registry_mock.called


def test_neural_should_not_create_model_registry_when_no_model_config_is_there(
    config: SandConfig,
    collectables: list[MagicMock],
    model_registry_mock: MagicMock,
) -> None:
    config.neural.model_config = None

    assert not model_registry_mock.called

    NeuralNetwork(global_config=config, collectables=collectables)

    assert not model_registry_mock.called


def test_neural_should_start_thread_when_frame_gets_pushed(
    config: SandConfig,
    collectables: list[MagicMock],
    thread_mock: MagicMock,
    enriched_frame: EnrichedFrame,
) -> None:
    sut = NeuralNetwork(global_config=config, collectables=collectables)

    assert not thread_mock.start.called

    sut.push_frame(enriched_frame)

    assert thread_mock.start.called
    assert sut.started


def test_neural_should_not_start_thread_if_not_active(
    config: SandConfig,
    collectables: list[MagicMock],
    thread_mock: MagicMock,
    enriched_frame: EnrichedFrame,
) -> None:
    config.neural.active = False

    sut = NeuralNetwork(global_config=config, collectables=collectables)

    assert not thread_mock.start.called

    sut.push_frame(enriched_frame)

    assert not thread_mock.start.called


def test_neural_should_add_frame_to_moment_representation(
    config: SandConfig,
    collectables: list[MagicMock],
    enriched_frame: EnrichedFrame,
) -> None:
    sut = NeuralNetwork(global_config=config, collectables=collectables)

    assert sut.images[enriched_frame.camera_name] is None

    sut.push_frame(enriched_frame)

    assert sut.images[enriched_frame.camera_name] is enriched_frame


def test_neural_should_do_nothing_when_shutdown_immediately(
    config: SandConfig,
    collectables: list[MagicMock],
) -> None:
    sut = NeuralNetwork(global_config=config, collectables=collectables)
    sut.publish = MagicMock()
    sut.shutdown_event.set()

    assert not sut.publish.called

    sut.work()

    assert not sut.publish.called


def test_neural_should_wait_when_it_checked_for_all_cameras(
    config: SandConfig,
    collectables: list[MagicMock],
    sleep_mock: MagicMock,
) -> None:
    sut = NeuralNetwork(global_config=config, collectables=collectables)
    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables) + 1
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    assert not sleep_mock.called

    sut.work()

    assert sleep_mock.called
    sleep_mock.assert_called_once_with(config.neural.wait_time_no_image_available)


def test_neural_should_evaluate_to_constant_demo_boxes_in_demo_mode(
    config: SandConfig,
    collectables: list[MagicMock],
    camera_name: str,
    enriched_frame: EnrichedFrame,
    constant_config: MagicMock,
) -> None:
    config.neural.demo = True

    constant_config.demo_boxes = {
        camera_name: [
            ("Person", 1, [500, 500, 700, 700], False, False, "", 1.0),
            ("Person", 1, [1000, 1000, 1200, 1200], False, False, "", 1.0),
        ]
    }

    sut = NeuralNetwork(global_config=config, collectables=collectables)
    sut.publish = MagicMock()
    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables)
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    sut.push_frame(enriched_frame)

    assert not sut.publish.called

    sut.work()

    assert sut.publish.called
    print(sut.publish.call_args_list)
    sut.publish.assert_has_calls(
        [
            call(
                payload=SandBoxes(
                    enriched_frame.id,
                    enriched_frame.camera_name,
                    enriched_frame.timestamp,
                    [
                        BoundingBox(
                            box=Box(
                                xmin=500,
                                ymin=500,
                                xmax=700,
                                ymax=700,
                            ),
                            class_identifier=ClassIdentifier(
                                class_name="Person",
                                class_id=1,
                            ),
                            difficult=False,
                            occluded=False,
                            content="",
                            score=1.0,
                        ),
                        BoundingBox(
                            box=Box(
                                xmin=1000,
                                ymin=1000,
                                xmax=1200,
                                ymax=1200,
                            ),
                            class_identifier=ClassIdentifier(
                                class_name="Person",
                                class_id=1,
                            ),
                            difficult=False,
                            occluded=False,
                            content="",
                            score=1.0,
                        ),
                    ],
                    enriched_frame.width,
                    enriched_frame.height,
                ),
                topic="NeuralNetwork/f1_l1_v1/data/boxes",
            )
        ]
    )


def test_neural_should_report_interesting_if_persons_are_found(
    config: SandConfig,
    collectables: list[MagicMock],
    camera_name: str,
    enriched_frame: EnrichedFrame,
    constant_config: MagicMock,
) -> None:
    config.neural.demo = True

    constant_config.demo_boxes = {
        camera_name: [
            ("Person", 1, [500, 500, 700, 700], False, False, "", 1.0),
        ]
    }

    sut = NeuralNetwork(global_config=config, collectables=collectables)
    sut.publish = MagicMock()
    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables)
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    sut.push_frame(enriched_frame)

    assert not sut.publish.called

    sut.work()

    assert sut.publish.called
    sut.publish.assert_has_calls(
        [
            call(
                payload=enriched_frame.timestamp,
                topic="NeuralNetwork/f1_l1_v1/data/interesting",
                retain=True,
            )
        ]
    )


def test_neural_should_not_report_interesting_if_no_persons_are_found(
    config: SandConfig,
    collectables: list[MagicMock],
    camera_name: str,
    enriched_frame: EnrichedFrame,
    constant_config: MagicMock,
) -> None:
    config.neural.demo = True

    constant_config.demo_boxes = {
        camera_name: [
            ("Cat", 4, [500, 500, 700, 700], False, False, "", 1.0),
        ]
    }

    sut = NeuralNetwork(global_config=config, collectables=collectables)
    sut.publish = MagicMock()
    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables)
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    sut.push_frame(enriched_frame)

    assert not sut.publish.called

    sut.work()

    assert sut.publish.called  # report boxes still
    with pytest.raises(AssertionError):
        sut.publish.assert_has_calls(
            [
                call(
                    payload=enriched_frame.timestamp,
                    topic="NeuralNetwork/f1_l1_v1/data/interesting",
                    retain=True,
                )
            ]
        )


def test_neural_should_reset_frame_after_scan(
    config: SandConfig,
    collectables: list[MagicMock],
    enriched_frame: EnrichedFrame,
) -> None:
    config.neural.demo = True

    sut = NeuralNetwork(global_config=config, collectables=collectables)
    sut.publish = MagicMock()
    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables)
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    sut.push_frame(enriched_frame)

    assert sut.images[enriched_frame.camera_name] is not None

    sut.work()

    assert sut.images[enriched_frame.camera_name] is None


def test_neural_should_execute_predict_method_when_not_in_demo_mode(
    config: SandConfig,
    collectables: list[MagicMock],
    enriched_frame: EnrichedFrame,
    model_mock: MagicMock,
) -> None:
    config.neural.demo = False
    config.neural.model_config = {
        "class_type": "mmdetection",
        "constructor_parameters": {
            "from_yaml": "./path/to/yaml",
            "config_root_dir": "./path/to/dir",
            "init_for_inference": True,
        },
    }

    sut = NeuralNetwork(global_config=config, collectables=collectables)

    call_count = {"value": 0}

    def shutdown_check() -> bool:
        result = call_count["value"] > len(collectables)
        call_count["value"] += 1
        return result

    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = shutdown_check

    sut.push_frame(enriched_frame)

    assert not model_mock.predict.called

    sut.work()

    assert model_mock.predict.called


def test_get_mlcvzoo_replacement_map_default() -> None:
    assert NeuralNetwork.get_mlcvzoo_replacement_map(replacement_config_path=None) == {}


def test_get_mlcvzoo_replacement_map(tmp_path: Path) -> None:
    test_replacement_map = {"PROJECT_ROOT_DIR": "TEST_PROJECT_ROOT_DIR"}

    replacement_map_path = tmp_path.joinpath("test_replacement_map.yaml")

    with replacement_map_path.open(mode="w") as replacement_file:
        yaml.dump(data=test_replacement_map, stream=replacement_file)

    assert (
        NeuralNetwork.get_mlcvzoo_replacement_map(
            replacement_config_path=replacement_map_path
        )
        == test_replacement_map
    )
