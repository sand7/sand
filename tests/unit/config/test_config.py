from __future__ import annotations

from ast import literal_eval
from pathlib import Path
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from sand.config import CameraConfig, ConstantConfig, LidarConfig
from sand.datatypes import CalPoints, LidarTransformation, Point
from sand.registry import get_nodes, get_singleton_node

mocked_path = "debug/path"
config_path = Path(__file__).parent.joinpath("test_config.yaml")


@pytest.fixture()
def literal_eval_mock(mocker: MockerFixture) -> MagicMock:
    mock = mocker.patch("sand.config.config.literal_eval")
    mock.side_effect = literal_eval
    return mock


def test_constant_config_gets_added_to_registry() -> None:
    assert len(get_nodes(ConstantConfig)) == 0
    config = ConstantConfig()
    assert len(get_nodes(ConstantConfig)) == 1
    assert get_singleton_node(ConstantConfig) is config


def test_camera_config_should_calc_transformation_correctly() -> None:
    first_points = "[(10, 10), (10, 20), (20, 20), (20, 10)]"
    first_expected = CalPoints(
        source_points=[
            Point(x=10, y=10),
            Point(x=10, y=20),
            Point(x=20, y=20),
            Point(x=20, y=10),
        ],
        target_points=[
            Point(x=10, y=10),
            Point(x=10, y=20),
            Point(x=20, y=20),
            Point(x=20, y=10),
        ],
    )

    sut = CameraConfig()
    sut.transformation_source_points = first_points
    sut.transformation_target_points = first_points
    sut.stream_resolution_str = "2560x1440"
    sut.transformation_source_resolution_str = "2560x1440"

    actual = sut.transformation  # as property access is enough
    assert actual == first_expected

    second_points = "[(20, 20), (10, 20), (30, 30), (20, 10)]"
    sut.transformation_source_points = second_points
    sut.transformation_target_points = second_points
    sut.stream_resolution_str = "2560x1440"
    sut.transformation_source_resolution_str = "3840x2160"
    second_expected = CalPoints(
        source_points=[
            Point(x=13, y=13),
            Point(x=6, y=13),
            Point(x=20, y=20),
            Point(x=13, y=6),
        ],
        target_points=[
            Point(x=20, y=20),
            Point(x=10, y=20),
            Point(x=30, y=30),
            Point(x=20, y=10),
        ],
    )

    actual = sut.transformation
    assert actual == second_expected


def test_datatypes_are_creatable_with_expansion() -> None:
    assert Point(*[1, 2]) == Point(x=1, y=2)
    assert Point(*(1, 2)) == Point(x=1, y=2)

    assert LidarTransformation(*[1, 2, 3, 4]) == LidarTransformation(
        x=1.0, y=2.0, z=3.0, angle=4.0
    )
    assert LidarTransformation(*(1, 2, 3, 4)) == LidarTransformation(
        x=1.0, y=2.0, z=3.0, angle=4.0
    )


def test_lidar_config_should_calculate_transformation_correctly_and_cache_results(
    literal_eval_mock: MagicMock,
) -> None:
    first_transformation = "[1, 1, 1, 1]"
    first_expected = LidarTransformation(x=1.0, y=1.0, z=1.0, angle=1.0)
    sut = LidarConfig(transformation_list=first_transformation)

    assert not literal_eval_mock.called

    actual = sut.transformation

    assert literal_eval_mock.called
    assert actual == first_expected

    literal_eval_mock.reset_mock()

    assert not literal_eval_mock.called

    actual = sut.transformation

    assert not literal_eval_mock.called

    second_transformation = "[2, 2, 2, 2]"
    second_expected = LidarTransformation(x=2.0, y=2.0, z=2.0, angle=2.0)
    sut.transformation_list = second_transformation

    assert not literal_eval_mock.called

    actual = sut.transformation

    assert literal_eval_mock.called
    assert actual == second_expected

    literal_eval_mock.reset_mock()
    sut.transformation_list = first_transformation

    assert not literal_eval_mock.called

    actual = sut.transformation

    assert not literal_eval_mock.called
    assert actual == first_expected
