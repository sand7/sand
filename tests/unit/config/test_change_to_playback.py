from __future__ import annotations

from pathlib import Path
from typing import cast

import pytest
from config_builder import ConfigBuilder
from pytest_mock import MockerFixture
from sand.config import ConstantConfig, SandConfig, change_to_playback_config

test_segment_with_h264 = (
    Path(__file__).parent.joinpath("test_segments").joinpath("2020-12-08T20:00:36")
)
test_segment_without_h264 = (
    Path(__file__).parent.joinpath("test_segments").joinpath("2021-12-08T20:00:36")
)
test_segment_without_f1_l3 = (
    Path(__file__).parent.joinpath("test_segments").joinpath("2022-12-08T20:00:36")
)
test_segment_wrong_filenames = (
    Path(__file__).parent.joinpath("test_segments").joinpath("2023-12-08T20:00:36")
)
test_images = (
    Path(__file__).parent.parent.parent.parent.joinpath("images").joinpath("cameras")
)
config_path = Path(__file__).parent.joinpath("test_config.yaml")


@pytest.fixture()
def sand_config(mocker: MockerFixture) -> SandConfig:
    mocker.patch("sand.registry.registry._nodes", new=[])
    constant_config = ConstantConfig()
    constant_config.demo_boxes.clear()

    config_builder = ConfigBuilder(
        class_type=SandConfig, yaml_config_path=config_path.as_posix()
    )
    return cast(SandConfig, config_builder.configuration)


@pytest.mark.skip(reason="must be fixes because of generalisation of camera names")
def test_should_change_file_path(sand_config: SandConfig) -> None:
    change_to_playback_config(sand_config, test_segment_with_h264)

    for camera_config in sand_config.cameras:
        assert camera_config.stream.endswith(
            f"test_segments/2020-12-08T20:00:36/{camera_config.name}/"
            f"2020-12-08T20:00:36_{camera_config.name}.h264.mp4"
        )


@pytest.mark.skip(reason="must be fixes because of generalisation of camera names")
def test_should_change_file_path_only_when_file_available(
    sand_config: SandConfig,
) -> None:
    unchanged_paths = [
        "f1_l3_v1",
        "f1_l3_v2",
        "f1_l1_la",
        "f1_l1_li",
    ]
    map_before: dict[str, str] = {}
    for camera_config in sand_config.cameras:
        map_before[camera_config.name] = camera_config.stream

    change_to_playback_config(sand_config, test_segment_without_f1_l3)

    at_least_one_camera_unchanged = False
    for camera_config in sand_config.cameras:
        if camera_config.name in unchanged_paths:
            at_least_one_camera_unchanged = True
            assert camera_config.stream == map_before[camera_config.name]
        else:
            assert camera_config.stream.endswith(
                f"test_segments/2022-12-08T20:00:36/{camera_config.name}/"
                f"2022-12-08T20:00:36_{camera_config.name}.mp4"
            )
    assert at_least_one_camera_unchanged

    map_before = {}
    for lidar_config in sand_config.lidars:
        map_before[lidar_config.name] = lidar_config.file_path

    at_least_one_lidar_unchanged = False
    for lidar_config in sand_config.lidars:
        if lidar_config.name in unchanged_paths:
            at_least_one_lidar_unchanged = True
            assert lidar_config.file_path == map_before[lidar_config.name]
        else:
            assert lidar_config.file_path.endswith(
                f"test_segments/2022-12-08T20:00:36/{lidar_config.name}/"
                f"2022-12-08T20:00:36_{lidar_config.name}.velo"
            )
    assert at_least_one_lidar_unchanged


def test_should_change_nothing_cameras_are_not_named_correctly(
    sand_config: SandConfig,
) -> None:
    map_before: dict[str, str] = {}
    for camera_config in sand_config.cameras:
        map_before[camera_config.name] = camera_config.stream
    for lidar_config in sand_config.lidars:
        map_before[lidar_config.name] = lidar_config.file_path

    change_to_playback_config(sand_config, test_segment_wrong_filenames)

    for camera_config in sand_config.cameras:
        assert camera_config.stream == map_before[camera_config.name]

    for lidar_config in sand_config.lidars:
        assert lidar_config.file_path == map_before[lidar_config.name]


@pytest.mark.skip(reason="must be fixes because of generalisation of camera names")
def test_should_also_accept_non_h264_encoded(sand_config: SandConfig) -> None:
    for camera_config in sand_config.cameras:
        assert not camera_config.stream.endswith(
            f"test_segments/2021-12-08T20:00:36/{camera_config.name}/"
            f"2021-12-08T20:00:36_{camera_config.name}.mp4"
        )
    change_to_playback_config(sand_config, test_segment_without_h264)

    for camera_config in sand_config.cameras:
        assert camera_config.stream.endswith(
            f"test_segments/2021-12-08T20:00:36/{camera_config.name}/"
            f"2021-12-08T20:00:36_{camera_config.name}.mp4"
        )
