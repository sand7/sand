from __future__ import annotations

from pathlib import Path
from unittest.mock import MagicMock, call

import pytest
from sand.config import CameraConfig, DangerZone, SandConfig, SensorFusionConfig
from sand.datatypes import Point
from sand.reader.lidar import LidarPacketEnricher
from sand.sensor_fusion.pipeline.fusion import SensorFusion
from sand.transformer.pipeline.box import BoxTransformer


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        sensor_fusion=SensorFusionConfig(
            calc_per_seconds=5,
            output_height=8,
            output_width=7,
            heat_map_cluster_size=10,
            danger_zones=DangerZone(
                svg_file=f"{Path(__file__).parent}/areas.svg",
                svg_color_object="#ff0000",
                svg_color_person="#000080",
            ),
        ),
        cameras=[CameraConfig()],
    )


def test_build_lookup_table(config: SandConfig) -> None:
    sut = SensorFusion(config)
    assert sut.checker.danger_zone["object"].danger_zone == [
        [0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 0, 0],
        [0, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
    ]


def test_test_line(config: SandConfig) -> None:
    sut = SensorFusion(config)
    assert not sut.checker.box_checker.test_line(Point(4000, 0), Point(6000, 0))
    assert not sut.checker.box_checker.test_line(Point(6000, 0), Point(4000, 0))
    assert sut.checker.box_checker.test_line(Point(0, 1000), Point(6000, 1000))
    assert sut.checker.box_checker.test_line(Point(6000, 1000), Point(0, 1000))
    assert sut.checker.box_checker.test_line(Point(6000, 0), Point(0, 6000))
    assert not sut.checker.box_checker.test_line(Point(5000, 6000), Point(0, 6000))
    assert not sut.checker.box_checker.test_line(Point(6000, 0), Point(6000, 3000))
    assert sut.checker.box_checker.test_line(Point(1000, 2000), Point(1000, 1000))
    assert sut.checker.box_checker.test_line(Point(1000, 1000), Point(1000, 1000))


def test_fusion_should_subscribe_to_topics(
    config: SandConfig, communication_mock: MagicMock
) -> None:
    assert not communication_mock.subscribe.called

    SensorFusion(config)

    communication_mock.subscribe.assert_has_calls(
        [
            call(f"{BoxTransformer.__name__}/+/data/transformed_boxes", qos=1),
            call(f"{LidarPacketEnricher.__name__}/+/data/pointcloud2d", qos=1),
        ]
    )
