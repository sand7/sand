from __future__ import annotations

from unittest.mock import MagicMock, call

import pytest
from sand.config import CameraConfig, MapBuilderConfig, SandConfig
from sand.datatypes import Box, Dimensions, Point
from sand.datatypes.scale import Scale
from sand.interfaces.config import ConfigurationManager
from sand.map.pipeline import MapEnricher
from sand.reader.lidar import LidarPacketEnricher
from sand.sensor_fusion import SensorFusion
from sand.transformer.pipeline.box import BoxTransformer
from sand.util.boxes import scale_transformed_boxes


@pytest.fixture()
def transformed_boxes() -> list[Box]:
    return [
        Box(
            lower_right=Point(
                x=100,
                y=100,
            ),
            lower_left=Point(
                x=10,
                y=100,
            ),
            upper_right=Point(
                x=100,
                y=10,
            ),
            upper_left=Point(
                x=10,
                y=10,
            ),
        )
    ]


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        cameras=[CameraConfig()], map_builder=MapBuilderConfig(active=True)
    )


@pytest.fixture()
def map_builder() -> MagicMock:
    return MagicMock()


@pytest.mark.skip()
def test_crane_enricher_should_subscribe_to_sources(
    config: SandConfig,
    map_builder: MagicMock,
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.subscribe.called

    MapEnricher(config, False, map_builder)

    communication_mock.subscribe.assert_has_calls(
        [
            call(f"{ConfigurationManager.__name__}/+/data/#", qos=1),
            call(f"{BoxTransformer.__name__}/+/data/transformed_boxes", qos=1),
            call(
                f"{BoxTransformer.__name__}/+/data/transformed_calibration_points",
                qos=1,
            ),
            call(f"{SensorFusion.__name__}/all/data/collision_map", qos=1),
            call(f"{SensorFusion.__name__}/all/data/heat_map_pointcloud", qos=1),
            call(f"{SensorFusion.__name__}/all/data/heat_map_detection", qos=1),
            call(f"{SensorFusion.__name__}/all/data/collision", qos=1),
            call(f"{LidarPacketEnricher.__name__}/+/data/pointcloud2d", qos=1),
        ]
    )


@pytest.mark.skip()
def test_crane_enricher_should_start_thread_immediately(
    config: SandConfig,
    map_builder: MagicMock,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
) -> None:
    assert not thread_mock.start.called

    sut = MapEnricher(config, False, map_builder)

    assert thread_mock.start.called
    thread_constructor_mock.assert_has_calls(
        [
            call(
                target=sut.work_draw,
                args=(),
                name=MapEnricher.__name__,
                daemon=False,
            )
        ]
    )


@pytest.mark.skip()
def test_crane_enricher_should_keep_track_of_subscribers(
    config: SandConfig,
    map_builder: MagicMock,
) -> None:
    sut = MapEnricher(config, False, map_builder)
    subscriber = MagicMock()

    assert len(sut.subscribers) == 0

    sut.subscribe(subscriber)

    assert len(sut.subscribers) == 1
    assert sut.subscribers[0] is subscriber

    sut.subscribe(subscriber)

    assert len(sut.subscribers) == 2


def test_crane_enricher_should_scale_box_correctly(
    transformed_boxes: list[Box],
) -> None:
    scale = Scale(Dimensions(100, 100), Dimensions(10, 10))
    scaled_boxes = scale_transformed_boxes(
        transformed_boxes, scale.scale_width, scale.scale_height
    )

    assert len(scaled_boxes) == 1
    assert scaled_boxes[0] == Box(
        lower_right=Point(
            x=int(transformed_boxes[0].lower_right.x * scale.scale_width),
            y=int(transformed_boxes[0].lower_right.y * scale.scale_height),
        ),
        lower_left=Point(
            x=int(transformed_boxes[0].lower_left.x * scale.scale_width),
            y=int(transformed_boxes[0].lower_left.y * scale.scale_height),
        ),
        upper_right=Point(
            x=int(transformed_boxes[0].upper_right.x * scale.scale_width),
            y=int(transformed_boxes[0].upper_right.y * scale.scale_height),
        ),
        upper_left=Point(
            x=int(transformed_boxes[0].upper_left.x * scale.scale_width),
            y=int(transformed_boxes[0].upper_left.y * scale.scale_height),
        ),
    )
