from __future__ import annotations

from unittest.mock import MagicMock

import pytest
from sand.config import MapBuilderConfig, SandConfig
from sand.map import MapBuilder


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        map_builder=MapBuilderConfig(
            active=True,
            calc_per_seconds_drawings=33,
            output_width=100,
            output_height=100,
        )
    )


def test_crane_map_should_do_nothing_when_shutdown(config: SandConfig) -> None:
    sut = MapBuilder(config, playback=False)
    sut.queue = MagicMock()
    sut.shutdown()

    assert sut.shutdown_event.is_set()
    assert not sut.queue.get.called

    sut.work_map()

    assert not sut.queue.get.called
