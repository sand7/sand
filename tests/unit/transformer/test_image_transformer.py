from __future__ import annotations

from datetime import datetime

import numpy as np
import pytest
from numpy import ndarray
from sand.config import get_basic_transformer_combination_config
from sand.datatypes import CalPoints, Dimensions, EnrichedFrame, Image, Matrix, Point
from sand.datatypes.scale import Scale
from sand.transformer.focal import FocalNormalizer
from sand.transformer.transformation import Transformation


def _to_cal_points(
    source: list[tuple[int, int]], target: list[tuple[int, int]]
) -> CalPoints:
    return CalPoints(
        source_points=[Point(p[0], p[1]) for p in source],
        target_points=[Point(p[0], p[1]) for p in target],
    )


unit_matrix_points = _to_cal_points(
    [(1, 1), (1, 2), (2, 2), (2, 1)],
    [(1, 1), (1, 2), (2, 2), (2, 1)],
)
scaled_unit_matrix_points = _to_cal_points(
    [(1, 1), (1, 2), (2, 2), (2, 1)],
    [(0, 0), (0, 0), (0, 0), (0, 0)],
)
transformed_matrix_points = _to_cal_points(
    [(1, 1), (1, 2), (2, 2), (2, 1)],
    [(1, 1), (1, 2), (2, 2), (2, 1)],
)

unit_matrix: Matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
scaled_unit_matrix: Matrix = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 1]])
scaled_unit_matrix2: Matrix = np.array([[2, 0, 0], [0, 2, 0], [0, 0, 1]])

default_focal = 1000


@pytest.fixture()
def image() -> Image:
    return np.array(
        [
            [[255.0, 0.0, 0.0], [0.0, 255.0, 0.0], [0.0, 0.0, 255.0]],
            [[0.0, 0.0, 255.0], [255.0, 0.0, 0.0], [0.0, 255.0, 0.0]],
            [[0.0, 255.0, 0.0], [0.0, 0.0, 255.0], [255.0, 0.0, 0.0]],
        ]
    )


@pytest.fixture()
def complex_enriched_frame(camera_name: str, image: Image) -> EnrichedFrame:
    return EnrichedFrame(
        camera_name,
        datetime.utcfromtimestamp(0),
        image,
    )


@pytest.fixture()
def transformation() -> Transformation:
    config = get_basic_transformer_combination_config().camera
    return Transformation(config, config.transformation_target_resolution)


@pytest.fixture()
def focal() -> FocalNormalizer:
    scale = Scale(Dimensions(100, 100), Dimensions(100, 100))
    return FocalNormalizer(default_focal, scale)


def test_transformation_data_object(transformation: Transformation) -> None:
    print()
    print(unit_matrix_points)
    print(transformation.calpoints)
    assert np.array_equal(unit_matrix_points, transformation.calpoints)


def test_transformation_get_matrix(transformation: Transformation) -> None:
    # die Matrixen bei Zahlen kleiner 1 sind nicht absolut identisch.
    # die default Abweichungen die allclose erlaubt reichen uns aus.
    # Abweichungen durch manuelles einmessen der Calibrierungspunkte sind deutlich ungenauer
    assert np.allclose(unit_matrix, transformation.get_matrix())


def test_focal_defaults(focal: FocalNormalizer) -> None:
    assert focal.focal == default_focal
    assert type(focal.map_x) is ndarray
    assert type(focal.map_y) is ndarray
    assert np.array_equal(
        focal.camera_intrinsic_matrix,
        np.array(
            [[1000.0, 0.0, 50.0], [0.0, 1000.0, 50.0], [0.0, 0.0, 1.0]],
            dtype=np.float32,
        ),
    )
    assert np.array_equal(
        focal.distortion_coefficients, np.array([[0], [0], [0], [0]], dtype=np.float32)
    )
