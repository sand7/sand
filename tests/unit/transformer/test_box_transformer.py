from __future__ import annotations

from unittest.mock import MagicMock, call

import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from sand.config import CameraConfig, SandConfig, TransformerConfig
from sand.config.config import TransformerBoxConfig, TransformerImageConfig
from sand.datatypes import SandBoxes
from sand.neural.pipeline.neural import NeuralNetwork
from sand.transformer.pipeline.box import BoxTransformer


@pytest.fixture()
def config(camera_name: str) -> SandConfig:
    return SandConfig(
        cameras=[CameraConfig(name=camera_name, fps=25)],
        transformer=TransformerConfig(
            image=TransformerImageConfig(
                active=True,
                output_width=100,
                output_height=100,
            ),
            box=TransformerBoxConfig(
                active=True,
            ),
        ),
    )


@pytest.fixture()
def sand_boxes() -> SandBoxes:
    bounding_box = MagicMock(spec=BoundingBox)
    bounding_box.box = MagicMock()
    bounding_box.box.xmin = 10
    bounding_box.box.ymin = 10
    bounding_box.box.xmax = 20
    bounding_box.box.ymax = 20

    return SandBoxes(
        boxes=[bounding_box],
        camera_name="1",
        frame_id=1,
        height=100,
        width=100,
        timestamp=MagicMock(),
    )


def test_box_transformer_starts_a_worker_thread_in_init(
    thread_mock: MagicMock,
    camera_name: str,
    config: SandConfig,
) -> None:
    assert not thread_mock.start.called
    BoxTransformer(camera_name, config, playback=False)
    assert thread_mock.start.called

    thread_mock.reset_mock()

    config.transformer.box.active = False
    assert not thread_mock.start.called
    BoxTransformer(camera_name, config, playback=False)
    assert not thread_mock.start.called


def test_box_transformer_push_adds_frame_to_queue(
    camera_name: str,
    config: SandConfig,
    sand_boxes: SandBoxes,
) -> None:
    sut = BoxTransformer(camera_name, config, playback=False)

    assert len(sut.queue) == 0

    sut.push_neural_frame("cool_frame_topic", sand_boxes)

    assert len(sut.queue) == 1


def test_crane_map_should_return_correct_name_and_fps(
    camera_name: str,
    config: SandConfig,
) -> None:
    sut = BoxTransformer(camera_name, config, playback=False)
    assert sut.get_name() == f"bt_{camera_name}"


def test_crane_map_should_subscribe_to_neural_network(
    camera_name: str,
    config: SandConfig,
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.subscribe.called

    BoxTransformer(camera_name, config, playback=False)

    communication_mock.subscribe.assert_has_calls(
        [call(f"{NeuralNetwork.__name__}/{camera_name}/data/boxes", qos=1)]
    )
