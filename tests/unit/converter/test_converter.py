from __future__ import annotations

from datetime import timedelta
from pathlib import Path
from typing import NamedTuple
from unittest.mock import ANY, MagicMock, call

import pytest
from pytest_mock import MockerFixture
from sand.config import ConverterConfig, SandConfig
from sand.converter import Converter
from sand.watcher import DriveWatcher


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        converter=ConverterConfig(
            active=True,
            folders=[],
            scan_interval_sec=1,
            scan_start_offset_sec=1,
            gpu_index=1,
            process_poll_interval_sec=1,
            delete_after_conversion=False,
            speedup_visual=1,
            speedup_thermal=1,
            segment_length_sec=1,
        )
    )


class SubprocessMock(NamedTuple):
    popen: MagicMock
    context: MagicMock
    process: MagicMock


@pytest.fixture()
def subprocess_mock(mocker: MockerFixture) -> SubprocessMock:
    popen_mock = mocker.patch("sand.converter.pipeline.converter.Popen")
    process_mock = MagicMock()
    context_mock = MagicMock()
    process_mock.communicate.return_value = (
        b"stdout",
        b"stderr",
    )
    context_mock.__enter__.return_value = process_mock
    popen_mock.return_value = context_mock
    return SubprocessMock(popen=popen_mock, context=context_mock, process=process_mock)


def test_converter_subscribes_to_drive_watcher_topic(
    communication_mock: MagicMock,
    config: SandConfig,
) -> None:
    assert not communication_mock.subscribe.called

    Converter(config)

    assert communication_mock.subscribe.called
    communication_mock.subscribe.assert_has_calls(
        [call(f"{DriveWatcher.__name__}/all/data/segment", qos=1)]
    )


def test_converter_creates_thread_but_does_not_start_it_automatically(
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
    config: SandConfig,
) -> None:
    assert not thread_constructor_mock.called

    sut = Converter(config)

    assert thread_constructor_mock.call_count == 1
    assert not thread_mock.start.called

    thread_constructor_mock.assert_has_calls(
        [call(target=sut.work, args=ANY, name=ANY, daemon=False)]
    )


def test_converter_path_change_starts_worker_thread(
    thread_mock: MagicMock,
    config: SandConfig,
) -> None:
    sut = Converter(config)

    assert not thread_mock.start.called

    sut.path_change("topic", "/absolute/path")

    assert thread_mock.start.called


def test_converter_offsets_first_scan(
    config: SandConfig,
) -> None:
    config.converter.scan_start_offset_sec = 42

    sut = Converter(config)

    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = True

    sut.work()

    assert sut.shutdown_event.wait.call_count == 1
    sut.shutdown_event.wait.assert_has_calls([call(42)])


def test_converter_shutdown_joins_thread_if_alive(
    thread_mock: MagicMock,
    config: SandConfig,
) -> None:
    sut = Converter(config)

    thread_mock.is_alive.return_value = True
    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.join.called


def test_converter_shutdown_does_not_join_thread_if_not_alive(
    thread_mock: MagicMock,
    config: SandConfig,
) -> None:
    sut = Converter(config)

    thread_mock.is_alive.return_value = False
    assert not thread_mock.join.called

    sut.shutdown()

    assert not thread_mock.join.called


def test_converter_does_nothing_if_shutdown_immediately(
    config: SandConfig,
) -> None:
    config.converter.scan_start_offset_sec = 42

    sut = Converter(config)
    sut.convert = MagicMock()
    # shutdown immediately
    sut.shutdown_event.set()

    assert not sut.convert.called

    sut.work()

    assert not sut.convert.called


def test_converter_does_not_poll_process_if_shutdown(
    config: SandConfig,
) -> None:
    sut = Converter(config)

    process_mock = MagicMock()
    file = Path("file")
    converted = Path("converted")
    timeout = timedelta(seconds=42)
    sut.shutdown_event.set()

    assert not process_mock.poll.called

    sut._poll_process(process_mock, file, converted, timeout)

    assert not process_mock.poll.called


def test_converter_does_return_no_timeout_if_shutdown(
    config: SandConfig,
) -> None:
    sut = Converter(config)

    process_mock = MagicMock()
    file = Path("file")
    converted = Path("converted")
    timeout = timedelta(seconds=42)

    is_timeout, duration = sut._poll_process(process_mock, file, converted, timeout)

    assert not is_timeout


def test_converter_reports_no_timeout_if_poll_successful(
    config: SandConfig,
) -> None:
    sut = Converter(config)

    file = Path("file")
    converted = Path("converted")
    timeout = timedelta(seconds=42)
    process_mock = MagicMock()
    process_mock.poll.return_value = 42

    assert not process_mock.poll.called

    is_timeout, duration = sut._poll_process(process_mock, file, converted, timeout)

    assert process_mock.poll.call_count == 1
    assert not is_timeout


def test_converter_does_nothing_when_no_files_need_conversion(
    subprocess_mock: SubprocessMock,
    config: SandConfig,
) -> None:
    sut = Converter(config)
    sut._get_file_list = MagicMock()

    sut._get_file_list.return_value = []

    assert not subprocess_mock.popen.called

    sut._convert()

    assert not subprocess_mock.popen.called


def test_converter_does_not_removed_if_config_disallowed(
    tmp_path: Path,
    config: SandConfig,
) -> None:
    config.converter.delete_after_conversion = False

    sut = Converter(config)
    sut._get_file_list = MagicMock()
    sut._poll_process = MagicMock()

    expected_file = tmp_path.joinpath("file_should_exist.mp4")
    expected_file.touch()

    sut._get_file_list.return_value = [expected_file]
    sut._poll_process.return_value = (False, timedelta(seconds=39))

    assert expected_file.exists()

    sut._convert()

    assert expected_file.exists()
