from __future__ import annotations

import contextlib
from unittest.mock import MagicMock, call, patch

import pytest
from pytest_mock import MockerFixture
from sand.config import CommunicationConfig
from sand.interfaces.synchronization import SandNode
from sand.interfaces.util.collectable import NamedCollectAble
from sand.recorder.video.pipeline.normalizer import VideoNormalizer

from . import MinimalCollectAble


@pytest.fixture()
def named_collectable() -> NamedCollectAble:
    return MinimalCollectAble()


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


@pytest.fixture()
def writer() -> MagicMock:
    writer_mock = MagicMock()
    writer_mock.config.fps = 25
    writer_mock.config.name = "writer"
    return writer_mock


@pytest.fixture()
def event_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.interfaces.synchronization.node.Event")


@pytest.fixture()
def time_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.recorder.video.pipeline.normalizer.time")


def test_normalizer_calls_sand_node(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
) -> None:
    with patch.object(SandNode, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            VideoNormalizer(communication_config, named_collectable, writer)

        assert init_mock.called


def test_normalizer_creates_thread(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    thread_constructor_mock: MagicMock,
) -> None:
    assert not thread_constructor_mock.called
    VideoNormalizer(communication_config, named_collectable, writer)
    assert thread_constructor_mock.called


def test_normalizer_does_not_start_thread(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    thread_mock: MagicMock,
) -> None:
    assert not thread_mock.start.called
    VideoNormalizer(communication_config, named_collectable, writer)
    assert not thread_mock.start.called


def test_normalizer_sets_thread_onto_work_method(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    thread_constructor_mock: MagicMock,
) -> None:
    assert not thread_constructor_mock.called
    VideoNormalizer(communication_config, named_collectable, writer)
    assert thread_constructor_mock.called


def test_normalizer_subscribes_to_named_collectable_enricher(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
) -> None:
    assert len(named_collectable.subscribers) == 0

    sut = VideoNormalizer(communication_config, named_collectable, writer)

    assert len(named_collectable.subscribers) == 1
    assert named_collectable.subscribers[0] is sut


def test_normalizer_push_starts_thread_if_initial(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    thread_mock: MagicMock,
    enriched_frame: MagicMock,
) -> None:
    sut = VideoNormalizer(communication_config, named_collectable, writer)
    assert not thread_mock.start.called
    sut.push_frame(enriched_frame)
    assert thread_mock.start.called


def test_normalizer_push_replaces_currently_hold_frame(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    enriched_frame: MagicMock,
) -> None:
    sut = VideoNormalizer(communication_config, named_collectable, writer)
    assert sut._frame is not enriched_frame
    sut.push_frame(enriched_frame)
    assert sut._frame is enriched_frame


def test_normalizer_normalize_does_nothing_if_shutdown(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
) -> None:
    sut = VideoNormalizer(communication_config, named_collectable, writer)
    sut.shutdown_event.set()

    assert not writer.push.called
    sut._normalize()
    assert not writer.push.called


def test_normalizer_normalize_does_push_current_frame_to_writer(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    enriched_frame: MagicMock,
) -> None:
    sut = VideoNormalizer(communication_config, named_collectable, writer)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True, True]
    sut._frame = enriched_frame

    assert not writer.push_frame.called
    sut._normalize()
    assert writer.push_frame.called
    writer.push_frame.assert_has_calls([call(enriched_frame)])


def test_normalizer_normalize_waits_between_two_pushes(
    communication_config: CommunicationConfig,
    named_collectable: NamedCollectAble,
    writer: MagicMock,
    enriched_frame: MagicMock,
    time_mock: MagicMock,
) -> None:
    writer.config.fps = 1
    time_mock.side_effect = [0, 0, 1]
    sut = VideoNormalizer(communication_config, named_collectable, writer)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, False, False, True, True]
    sut._frame = enriched_frame

    assert not writer.push_frame.called
    assert not sut.shutdown_event.wait.called
    sut._normalize()
    assert writer.push_frame.call_count == 2
    assert sut.shutdown_event.wait.call_count == 1
    sut.shutdown_event.wait.assert_has_calls([call(1 / 10)])
