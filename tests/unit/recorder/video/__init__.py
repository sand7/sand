from __future__ import annotations

from sand.interfaces.util.collectable import NamedCollectAble
from sand.interfaces.util.subscriber import EnrichedSubscriber


class MinimalCollectAble(NamedCollectAble[EnrichedSubscriber]):
    def __init__(self, name: str = "test") -> None:
        super().__init__()

        self._name = name

    def get_name(self) -> str:
        return self._name
