from __future__ import annotations

from unittest.mock import MagicMock, patch

from sand.config.config import CameraConfig, CommunicationConfig
from sand.recorder.video.pipeline.recorder import VideoRecorder

from . import MinimalCollectAble


@patch("sand.recorder.video.pipeline.recorder.VideoWriter")
@patch("sand.recorder.video.pipeline.recorder.VideoNormalizer")
def test_recorder_combines_writer_and_normalizer(
    normalizer_class_mock: MagicMock, writer_class_mock: MagicMock
) -> None:
    config = CameraConfig(name="test", writer_active=True)
    communication_config = CommunicationConfig()

    assert not normalizer_class_mock.called
    assert not writer_class_mock.called

    VideoRecorder(
        MinimalCollectAble("test"),
        [config],
        communication_config,
        playback=False,
    )

    assert normalizer_class_mock.called
    assert writer_class_mock.called
