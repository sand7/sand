from __future__ import annotations

import contextlib
from datetime import datetime
from pathlib import Path
from shutil import rmtree
from typing import Callable, Generator
from unittest.mock import MagicMock, call, patch

import numpy as np
import pytest
from pytest_mock import MockerFixture
from sand.config import CameraConfig, CommunicationConfig
from sand.datatypes import EnrichedFrame
from sand.interfaces.shutdown import ShutdownAble
from sand.recorder.video.pipeline import VideoWriter


def setup_function() -> None:
    # used for mocking the `now()` function when differences are needed
    global now_called_counter, global_thread_mock
    now_called_counter = 0
    global_thread_mock = MagicMock()


@pytest.fixture()
def config(camera_name: str) -> CameraConfig:
    return CameraConfig(
        writer_active=True,
        fps=25,
        name=camera_name,
        stream="rtsp://link_to_camera",
        focal=1500,
        metric_interval=10,
        is_interesting_before=10,
        is_interesting_after=30,
        interesting_mode="all",
        interesting_source="neural",
    )


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


global_thread_mock = MagicMock()


def _create_thread_mock(
    target: Callable, args: tuple, name: str, daemon: bool
) -> MagicMock:
    # use `global_thread_mock` instead of `extended_thread_mock.return_value`
    # works only if you want the last object / don't need multiple thread
    # objects in your test

    global global_thread_mock
    global_thread_mock.target = target
    global_thread_mock.args = args
    global_thread_mock.name = name
    global_thread_mock.daemon = daemon
    return global_thread_mock


@pytest.fixture(autouse=True)
def extended_thread_mock(thread_constructor_mock: MagicMock) -> MagicMock:
    thread_constructor_mock.side_effect = _create_thread_mock
    return thread_constructor_mock


@pytest.fixture()
def lock_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.recorder.video.pipeline.writer.Lock")


@pytest.fixture(autouse=True)
def now_mock(mocker: MockerFixture) -> tuple[MagicMock, MagicMock]:
    """
    If you want to mock the now function generally use this fixture. In this case this is
    more complicated, as the writer system internally uses a stats object from another file
    and because of pythons importing, namespacing and resulting mocking rules, you need to
    mock the now function twice.

    If you want to use the {_now_side_effect} it is advised to set this side_effect to
    both mocks via:

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect
    """
    return mocker.patch(
        "sand.recorder.video.pipeline.writer.now",
        return_value=datetime.utcfromtimestamp(0),
    ), mocker.patch(
        "sand.recorder.video.pipeline.stats.now",
        return_value=datetime.utcfromtimestamp(0),
    )


@pytest.fixture(autouse=True)
def cv2_writer_constructor_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.recorder.video.pipeline.writer.CV2VideoWriter")


@pytest.fixture()
def cv2_writer_mock(cv2_writer_constructor_mock: MagicMock) -> MagicMock:
    return cv2_writer_constructor_mock.return_value


@pytest.fixture(autouse=True)
def put_text_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.util.image.putText")


@pytest.fixture()
def path_change_message(
    tmp_path: Path, camera_name: str
) -> Generator[Path, None, None]:
    msg = tmp_path.absolute().as_posix()

    subfolder = tmp_path.joinpath(camera_name)
    subfolder.mkdir(parents=True, exist_ok=True)

    yield msg

    rmtree(subfolder)


def test_shutdown_able_calls_register_able_initializer(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    with patch.object(ShutdownAble, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            VideoWriter(config, communication_config, playback=False)

        assert init_mock.called


def test_writer_creates_thread_without_starting(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
) -> None:
    global global_thread_mock

    assert not extended_thread_mock.called
    assert not global_thread_mock.start.called

    VideoWriter(config, communication_config, playback=False)

    assert extended_thread_mock.called
    assert not global_thread_mock.start.called


def test_writer_subscribes_to_drive_watcher_from_registry(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.subscribe.called

    VideoWriter(config, communication_config, playback=False)

    assert communication_mock.subscribe.called

    communication_mock.subscribe.assert_has_calls(
        [call("DriveWatcher/all/data/segment", qos=1)]
    )


def test_writer_sees_everything_as_interesting_if_neural_net_is_off(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    config.interesting_mode = "off"

    sut = VideoWriter(config, communication_config, playback=False)

    assert sut._is_interesting(datetime.now())
    # year 1970
    assert sut._is_interesting(datetime.fromtimestamp(1))
    # year 5000+
    assert sut._is_interesting(datetime.fromtimestamp(1e11))


@patch("sand.recorder.video.pipeline.writer.Path")
def test_writer_path_change_creates_a_directory_for_the_camera_when_path_change_comes_in(
    path_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    sut = VideoWriter(config, communication_config, playback=False)

    assert not path_mock.return_value.joinpath.return_value.mkdir.called

    sut.path_change("topic", path_change_message)

    assert path_mock.return_value.joinpath.return_value.mkdir.called

    path_mock.return_value.joinpath.assert_has_calls([call(config.name)])
    path_mock.return_value.joinpath.return_value.mkdir.assert_has_calls(
        [call(parents=True, exist_ok=True)]
    )


def test_writer_path_change_starts_thread_if_always_interesting_and_not_already_started(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    global global_thread_mock
    thread_mock = global_thread_mock

    config.interesting_mode = "off"
    thread_mock.is_alive.return_value = False

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.start.called

    sut.path_change("topic", path_change_message)

    assert thread_mock.start.called


def test_writer_path_change_does_not_start_thread_if_neural_interesting_is_not_active(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = False

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.start.called

    sut.path_change("topic", path_change_message)

    assert not thread_mock.start.called


@patch("sand.interfaces.synchronization.node.Thread")
def test_writer_path_change_does_replace_and_start_thread_when_alive(
    thread_cl_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    thread_list: list[MagicMock] = []

    def _create_unique_thread(
        target: Callable, args: tuple, name: str, daemon: bool
    ) -> MagicMock:
        unique_thread = MagicMock()
        unique_thread.target = target
        unique_thread.args = args
        unique_thread.name = name
        unique_thread.daemon = daemon
        thread_list.append(unique_thread)
        return unique_thread

    thread_cl_mock.side_effect = _create_unique_thread

    sut = VideoWriter(config, communication_config, playback=False)

    assert thread_cl_mock.call_count == 1
    assert len(thread_list) == 1

    thread_list[0].is_alive.return_value = True

    sut.path_change("topic", path_change_message)

    assert thread_cl_mock.call_count == 2
    assert len(thread_list) == 2
    assert thread_list[1].start.called


def test_writer_path_change_does_replace_frame_queue(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    sut = VideoWriter(config, communication_config, playback=False)

    before = sut.frame_queue

    sut.path_change("topic", path_change_message)

    assert sut.frame_queue is not before


def test_writer_push_adds_frame_to_queue(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    enriched_frame: EnrichedFrame,
) -> None:
    sut = VideoWriter(config, communication_config, playback=False)

    assert sut.frame_queue.qsize() == 0

    sut.push_frame(enriched_frame)

    assert sut.frame_queue.qsize() == 1
    assert sut.frame_queue.get() is enriched_frame


def test_writer_push_remove_removes_frames_when_queue_gets_to_big_in_neural_mode(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_name: str,
) -> None:
    config.is_interesting_before = 2
    config.fps = 1

    sut = VideoWriter(config, communication_config, playback=False)

    frames = [
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(0),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(0),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(0),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(0),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(0),
            np.zeros((3, 3, 3)),
        ),
    ]

    assert sut.frame_queue.qsize() == 0

    for frame in frames:
        sut.push_frame(frame)

        assert sut.frame_queue.qsize() <= config.is_interesting_before + 1

    assert sut.frame_queue.qsize() == config.is_interesting_before + 1

    index = 2
    while sut.frame_queue.qsize() > 0:
        actual_frame = sut.frame_queue.get()

        assert actual_frame is frames[index]
        index += 1

    assert sut.frame_queue.qsize() == 0


def test_writer_shutdown_puts_none_into_queue(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    sut = VideoWriter(config, communication_config, playback=False)

    assert sut.frame_queue.qsize() == 0

    sut.shutdown()

    assert sut.frame_queue.qsize() == 1
    assert sut.frame_queue.get() is None


def test_writer_shutdown_joins_main_thread_if_alive(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.join.called


def test_writer_shutdown_does_not_join_main_thread_if_not_alive(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = False

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.join.called

    sut.shutdown()

    assert not thread_mock.join.called


def test_writer_shutdown_joins_all_old_threads_if_alive(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    sut = VideoWriter(config, communication_config, playback=False)
    sut.path_change("topic", path_change_message)

    assert extended_thread_mock.call_count == 2
    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.join.call_count == 2


def test_writer_shutdown_joins_no_old_thread_if_not_alive(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    sut = VideoWriter(config, communication_config, playback=False)
    sut.path_change("topic", path_change_message)

    assert extended_thread_mock.call_count == 2
    assert not thread_mock.join.called

    thread_mock.is_alive.return_value = False

    sut.shutdown()

    assert not thread_mock.join.called


def test_writer_set_interesting_starts_thread_if_not_alive(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = False

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.start.called

    sut.set_interesting(datetime.utcfromtimestamp(10))

    assert thread_mock.start.called


def test_writer_set_interesting_does_not_start_thread_if_already_alive(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    global global_thread_mock

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    sut = VideoWriter(config, communication_config, playback=False)

    assert not thread_mock.start.called

    sut.set_interesting(datetime.utcfromtimestamp(10))

    assert not thread_mock.start.called


def test_writer_set_interesting_just_expands_time_frame_if_interesting_timestamp_is_in_range(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    sut = VideoWriter(config, communication_config, playback=False)

    sut.set_interesting(datetime.utcfromtimestamp(10))

    assert sut.interesting_interval.interesting_time_frame == [
        datetime.utcfromtimestamp(8),
        datetime.utcfromtimestamp(12),
    ]

    sut.set_interesting(datetime.utcfromtimestamp(11))

    assert sut.interesting_interval.interesting_time_frame == [
        datetime.utcfromtimestamp(8),
        datetime.utcfromtimestamp(13),
    ]


def test_writer_set_interesting_sets_new_interval_when_timestamp_is_before_start(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    sut = VideoWriter(config, communication_config, playback=False)

    sut.set_interesting(datetime.utcfromtimestamp(10))

    assert sut.interesting_interval.interesting_time_frame == [
        datetime.utcfromtimestamp(8),
        datetime.utcfromtimestamp(12),
    ]

    sut.set_interesting(datetime.utcfromtimestamp(7))

    assert sut.interesting_interval.interesting_time_frame == [
        datetime.utcfromtimestamp(5),
        datetime.utcfromtimestamp(9),
    ]


def test_writer_is_interesting_if_timestamp_in_between_interesting_timeframe(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    sut = VideoWriter(config, communication_config, playback=False)

    sut.set_interesting(datetime.utcfromtimestamp(10))

    assert sut.interesting_interval.interesting_time_frame == [
        datetime.utcfromtimestamp(8),
        datetime.utcfromtimestamp(12),
    ]

    assert sut._is_interesting(datetime.utcfromtimestamp(8))
    assert sut._is_interesting(datetime.utcfromtimestamp(10))
    assert sut._is_interesting(datetime.utcfromtimestamp(12))

    assert not sut._is_interesting(datetime.utcfromtimestamp(7))
    assert not sut._is_interesting(datetime.utcfromtimestamp(13))


def test_writer_get_font_scale_gets_specified_scales() -> None:
    assert VideoWriter.get_font_scale(2560) == 2

    assert VideoWriter.get_font_scale(1280) == 1


def test_writer_should_log_intermediary_statistics_logs_more_first_minute(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    config.fps = 1

    sut = VideoWriter(config, communication_config, playback=False)

    assert sut.intermediary_log_config.should_log(1)
    assert sut.intermediary_log_config.should_log(59)

    assert not sut.intermediary_log_config.should_log(60)


now_called_counter = 0


def _now_side_effect() -> datetime:
    global now_called_counter
    now_called_counter += 1
    return datetime.utcfromtimestamp(now_called_counter)


def test_writer_write_does_not_create_writer_if_shutdown_immediately(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    cv2_writer_mock: MagicMock,
    now_mock: tuple[MagicMock, MagicMock],
    path_change_message: str,
) -> None:
    cv2_video_writer_mock = cv2_writer_mock.return_value

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=False)
    sut.path_change("topic", path_change_message)
    sut.shutdown()

    assert sut.frame_queue.qsize() == 1
    assert not cv2_writer_mock.called
    assert not cv2_video_writer_mock.write.called
    assert not cv2_video_writer_mock.release.called

    sut.write()

    assert not cv2_writer_mock.called


def test_writer_write_creates_cv2_writer_and_writes_first_frame_and_releases_it_if_shutdown_after_first_frame(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    cv2_writer_constructor_mock: MagicMock,
    cv2_writer_mock: MagicMock,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=False)

    sut.set_interesting(enriched_frame.timestamp)

    video_format = enriched_frame.frame.shape[1], enriched_frame.frame.shape[0]
    sut.path_change("topic", path_change_message)
    sut.push_frame(enriched_frame)
    sut.shutdown()

    assert sut.frame_queue.qsize() == 2
    assert not cv2_writer_constructor_mock.called
    assert not cv2_writer_mock.write.called
    assert not cv2_writer_mock.release.called

    sut.write()

    assert cv2_writer_constructor_mock.called

    cv2_writer_constructor_mock.assert_any_call(
        f"{path_change_message}/{config.name}/1970-01-01T00:00:05_f1_l1_v1.mp4",
        sut.codec,
        config.fps,
        video_format,
    )

    assert cv2_writer_mock.write.called
    assert cv2_writer_mock.release.called


def test_writer_write_creates_thread_specific_stats_and_logs_infos_if_shutdown_after_one_frame(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=False)
    sut.set_interesting(enriched_frame.timestamp)
    sut.path_change("topic", path_change_message)
    sut.push_frame(enriched_frame)
    sut.shutdown()

    sut.stats = MagicMock(wraps=sut.stats)

    assert not sut.stats.new_writer_stats.called
    assert not sut.stats.remove_writer_stats.called

    sut.write()

    assert sut.stats.new_writer_stats.called
    assert sut.stats.remove_writer_stats.called


def test_writer_write_puts_timestamp_on_frame(
    communication_config: CommunicationConfig,
    put_text_mock: MagicMock,
    config: CameraConfig,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    config.interesting_mode = "off"
    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=False)

    sut.path_change("topic", path_change_message)

    sut.push_frame(enriched_frame)

    sut.shutdown()

    assert not put_text_mock.called

    sut.write()

    assert put_text_mock.call_count == 1

    assert np.array_equal(put_text_mock.call_args[0][0], enriched_frame.frame)
    assert put_text_mock.call_args[0][1] == str(enriched_frame.timestamp)

    assert put_text_mock.call_args[0][2] == (0, 50)  # (x, y) where on the frame
    assert put_text_mock.call_args[0][3] == 0  # FONT_HERSHEY_SIMPLEX
    assert put_text_mock.call_args[0][4] == enriched_frame.width / 1280
    assert put_text_mock.call_args[0][5] == (255, 255, 255)  # white
    assert put_text_mock.call_args[0][6] == 2  # bold


def test_writer_write_writes_frame(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    cv2_writer_mock: MagicMock,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    config.interesting_mode = "off"
    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=False)

    sut.path_change("topic", path_change_message)

    sut.push_frame(enriched_frame)

    sut.shutdown()

    assert not cv2_writer_mock.write.called

    sut.write()

    print(f"{cv2_writer_mock.write.call_args[0][0]=}, {enriched_frame.frame=}")
    assert cv2_writer_mock.write.call_count == 1
    np.testing.assert_array_equal(
        cv2_writer_mock.write.call_args[0][0], enriched_frame.frame
    )


def test_writer_write_does_not_write_frame_when_playback(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    cv2_writer_mock: MagicMock,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    config.interesting_mode = "off"
    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=True)
    sut.path_change("topic", path_change_message)

    sut.push_frame(enriched_frame)

    sut.shutdown()

    assert not cv2_writer_mock.write.called

    sut.write()

    assert not cv2_writer_mock.write.called


def test_writer_write_stops_writing_when_frame_is_not_interesting(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    cv2_writer_mock: MagicMock,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect
    sut = VideoWriter(config, communication_config, playback=True)
    sut.path_change("topic", path_change_message)

    sut.set_interesting(datetime.utcfromtimestamp(10))
    # 10 - 2 = 8 -> 7 is not interesting
    enriched_frame.timestamp = datetime.utcfromtimestamp(7)
    sut.push_frame(enriched_frame)

    assert not cv2_writer_mock.write.called

    sut.write()

    assert not cv2_writer_mock.write.called


def test_writer_write_replaces_thread_when_not_interesting_but_does_not_start_it(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    path_change_message: str,
) -> None:
    global global_thread_mock

    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=True)
    sut.path_change("topic", path_change_message)

    sut.set_interesting(datetime.utcfromtimestamp(10))
    # 10 - 2 = 8 -> 7 is not interesting
    enriched_frame.timestamp = datetime.utcfromtimestamp(7)
    sut.push_frame(enriched_frame)

    extended_thread_mock.reset_mock()
    thread_mock.reset_mock()

    assert not extended_thread_mock.called
    assert not thread_mock.start.called

    sut.write()

    assert extended_thread_mock.called
    assert not thread_mock.start.called


def test_writer_write_replaces_thread_when_getting_not_interesting_but_does_not_start_it(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
    now_mock: tuple[MagicMock, MagicMock],
    camera_name: str,
    path_change_message: str,
) -> None:
    global global_thread_mock

    config.is_interesting_before = 2
    config.is_interesting_after = 2
    config.fps = 1

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=True)
    sut.path_change("topic", path_change_message)

    # interesting interval: [8, 12]
    sut.set_interesting(datetime.utcfromtimestamp(10))

    frames = [
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(10),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(11),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(12),
            np.zeros((3, 3, 3)),
        ),
        EnrichedFrame(
            camera_name,
            datetime.utcfromtimestamp(13),
            np.zeros((3, 3, 3)),
        ),
    ]

    for frame in frames:
        sut.push_frame(frame)

    extended_thread_mock.reset_mock()
    thread_mock.reset_mock()

    assert not extended_thread_mock.called
    assert not thread_mock.start.called

    sut.write()

    assert extended_thread_mock.called
    assert not thread_mock.start.called


def test_writer_write_does_not_replace_thread_when_thread_is_shutdown(
    communication_config: CommunicationConfig,
    extended_thread_mock: MagicMock,
    config: CameraConfig,
    now_mock: tuple[MagicMock, MagicMock],
    enriched_frame: EnrichedFrame,
    tmp_path: Path,
) -> None:
    global global_thread_mock

    config.interesting_mode = "off"

    thread_mock = global_thread_mock
    thread_mock.is_alive.return_value = True

    now_mock[0].side_effect = _now_side_effect
    now_mock[1].side_effect = _now_side_effect

    sut = VideoWriter(config, communication_config, playback=True)

    tmp_path.joinpath(config.name).mkdir(parents=True, exist_ok=True)
    sut.path_change("topic", tmp_path.absolute().as_posix())

    sut.push_frame(enriched_frame)
    sut.push_frame(enriched_frame)
    sut.shutdown()

    extended_thread_mock.reset_mock()
    thread_mock.reset_mock()

    assert not extended_thread_mock.called
    assert not thread_mock.start.called

    sut.write()

    assert not extended_thread_mock.called
    assert not thread_mock.start.called


@patch("sand.interfaces.synchronization.node.Thread")
def test_writer_path_change_does_not_start_thread_again_if_it_stops_immediately(
    thread_constructor_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    tmp_path: Path,
) -> None:
    thread_list: list[MagicMock] = []

    def _create_unique_thread(
        target: Callable, args: tuple, name: str, daemon: bool
    ) -> MagicMock:
        # stop old thread
        with contextlib.suppress(Exception):
            thread_list[-1].is_alive.return_value = False

        unique_thread = MagicMock()
        unique_thread.target = target
        unique_thread.args = args
        unique_thread.name = name
        unique_thread.daemon = daemon
        thread_list.append(unique_thread)
        return unique_thread

    config.interesting_mode = "off"
    thread_constructor_mock.side_effect = _create_unique_thread

    sut = VideoWriter(config, communication_config, playback=False)
    tmp_path.joinpath(config.name).mkdir(parents=True, exist_ok=True)

    assert thread_constructor_mock.call_count == 1
    assert len(thread_list) == 1
    assert not thread_list[0].start.called

    thread_list[0].is_alive.return_value = True

    sut.path_change("topic", tmp_path.absolute().as_posix())

    assert thread_constructor_mock.call_count == 2
    assert len(thread_list) == 2
    assert not thread_list[0].start.called


def test_writer_path_change_starts_thread_once_when_not_started_in_no_neural_mode(
    thread_constructor_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    path_change_message: str,
) -> None:
    thread_list: list[MagicMock] = []

    def _create_unique_thread(
        target: Callable, args: tuple, name: str, daemon: bool
    ) -> MagicMock:
        # stop old thread
        with contextlib.suppress(Exception):
            thread_list[-1].is_alive.return_value = False

        unique_thread = MagicMock()
        unique_thread.target = target
        unique_thread.args = args
        unique_thread.name = name
        unique_thread.daemon = daemon
        thread_list.append(unique_thread)
        return unique_thread

    config.interesting_mode = "off"
    thread_constructor_mock.side_effect = _create_unique_thread

    sut = VideoWriter(config, communication_config, playback=False)

    assert thread_constructor_mock.call_count == 1
    assert len(thread_list) == 1
    assert not thread_list[0].start.called

    thread_list[0].is_alive.return_value = False

    def _start_thread() -> None:
        thread_list[0].is_alive.return_value = True

    thread_list[0].start.side_effect = _start_thread

    sut.path_change("topic", path_change_message)

    assert thread_constructor_mock.call_count == 1
    assert len(thread_list) == 1
    assert thread_list[0].start.called
