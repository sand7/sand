from __future__ import annotations

import contextlib
from pathlib import Path
from unittest.mock import MagicMock, call, patch

import pytest
from sand.config import CommunicationConfig, LidarConfig
from sand.datatypes import LidarPacket
from sand.interfaces.synchronization import SandNode
from sand.reader.lidar import LidarSystem
from sand.recorder.lidar.pipeline.recorder import LidarRecorder
from sand.watcher import DriveWatcher


@pytest.fixture()
def config() -> LidarConfig:
    return LidarConfig()


@pytest.fixture()
def lidar_system() -> LidarSystem:
    return MagicMock()


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


@pytest.fixture()
def lidar_packet(config: LidarConfig) -> LidarPacket:
    return LidarPacket(
        config.name,  # hack to make util.time work not with negative things
        b"asdf" * 300 + b"\x00\x00\x00\x00\x00\x00",
    )


def test_recorder_should_call_sand_node_init(
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
) -> None:
    with patch.object(SandNode, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            LidarRecorder(lidar_system, config, communication_config, playback=False)

        assert init_mock.called


def test_recorder_should_subscribe_to_drive_watcher(
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
    communication_mock: MagicMock,
) -> None:
    assert not communication_mock.subscribe.called

    LidarRecorder(lidar_system, config, communication_config, playback=False)

    communication_mock.subscribe.assert_has_calls(
        [call(f"{DriveWatcher.__name__}/all/data/segment", qos=1)]
    )


def test_recorder_should_create_thread_but_not_starting_it(
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
) -> None:
    assert not thread_constructor_mock.called

    sut = LidarRecorder(lidar_system, config, communication_config, playback=False)

    assert thread_constructor_mock.called
    thread_constructor_mock.assert_has_calls(
        [call(target=sut.writer, args=(), name="lw_lidar", daemon=False)]
    )
    assert not thread_mock.start.called


def test_recorder_pushes_packet_into_queue(
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
    lidar_packet: LidarPacket,
) -> None:
    sut = LidarRecorder(lidar_system, config, communication_config, playback=False)

    assert sut.queue.qsize() == 0

    sut.push_packet(lidar_packet)

    assert sut.queue.qsize() == 1


@patch("sand.recorder.lidar.pipeline.recorder.Path")
def test_recorder_path_change_creates_folder(
    path_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
) -> None:
    sut = LidarRecorder(lidar_system, config, communication_config, playback=False)

    assert not path_mock.called

    sut.path_change("topic", "/absolute/test/path")

    assert path_mock.called
    assert path_mock.return_value.joinpath.called
    assert path_mock.return_value.joinpath.return_value.mkdir.called

    path_mock.assert_has_calls([call("/absolute/test/path")])
    path_mock.return_value.joinpath.assert_has_calls([call("lidar")])
    path_mock.return_value.joinpath.return_value.mkdir.assert_has_calls(
        [call(parents=True, exist_ok=True)]
    )


def test_recorder_path_change_starts_thread_if_not_alive(
    communication_config: CommunicationConfig,
    config: LidarConfig,
    lidar_system: LidarSystem,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
    tmp_path: Path,
) -> None:
    sut = LidarRecorder(lidar_system, config, communication_config, playback=False)
    config.writer_active = True
    thread_mock.name = "lw_lidar"
    thread_mock.is_alive.return_value = False

    assert thread_constructor_mock.call_count == 1
    assert not thread_mock.start.called

    sut.path_change("topic", tmp_path.absolute().as_posix())

    assert thread_mock.start.called
