from __future__ import annotations

import contextlib
from unittest.mock import ANY, MagicMock, call, patch

import pytest
from pytest_mock import MockerFixture
from sand.config import PublisherConfig, SandConfig
from sand.interfaces.shutdown import ShutdownAble
from sand.view.frontend import Publisher


@pytest.fixture()
def config() -> SandConfig:
    return SandConfig(
        publisher=PublisherConfig(
            active=True,
            image_scale=0.2,
        )
    )


@pytest.fixture()
def flask_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.view.frontend.publisher.Flask")


def test_publisher_calls_shutdown_able_initializer(config: SandConfig) -> None:
    with patch.object(ShutdownAble, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            Publisher(config)

        assert init_mock.called


def test_publisher_creates_thread_and_starts_it(
    config: SandConfig,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
) -> None:
    assert not thread_constructor_mock.called
    assert not thread_mock.start.called

    Publisher(config)

    assert thread_constructor_mock.called
    assert thread_mock.start.called


def test_publisher_creates_thread_in_daemon_mode(
    config: SandConfig, thread_constructor_mock: MagicMock
) -> None:
    assert not thread_constructor_mock.called

    Publisher(config)

    assert thread_constructor_mock.called
    thread_constructor_mock.assert_has_calls(
        [call(target=ANY, args=ANY, name=ANY, daemon=True)]
    )


def test_publisher_adds_root_url_endpoint_to_index(
    config: SandConfig,
    flask_mock: MagicMock,
) -> None:
    assert not flask_mock.return_value.add_url_rule.called

    sut = Publisher(config)

    assert flask_mock.return_value.add_url_rule.called
    flask_mock.return_value.add_url_rule.assert_has_calls(
        [call("/", "index", sut.index)]
    )


def test_publisher_shutdown_does_not_join_thread_even_if_alive(
    config: SandConfig,
    thread_mock: MagicMock,
) -> None:
    """
    Servers are not stoppable, therefore no joining threads for shutdown, just killing...
    """
    sut = Publisher(config)
    thread_mock.is_alive.return_value = True

    assert not thread_mock.join.called

    sut.shutdown()

    assert not thread_mock.join.called
