from __future__ import annotations

from datetime import datetime
from unittest.mock import MagicMock, patch

from sand.util.time import now


@patch("sand.util.time.datetime")
def test_now_returns_current_datetime_in_utc(datetime_mock: MagicMock) -> None:
    expected = datetime.fromtimestamp(0)
    datetime_mock.now.return_value = expected

    assert now() == expected
