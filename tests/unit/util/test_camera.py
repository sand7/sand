from __future__ import annotations

from pathlib import Path

from sand.util.camera import get_camera_name, get_path_from_camera_name


def test_get_camera_name() -> None:
    assert get_camera_name("asdf1_l1_v1asdf.mp4") == "asdf1_l1_v1asdf"
    assert get_camera_name("asdf1_l") == "asdf1_l"
    assert not get_camera_name("")


def test_get_path_from_camera_name() -> None:
    assert get_path_from_camera_name("f1_l1_v1") == Path("images/cameras/f1_l1_v1.jpg")
    assert get_path_from_camera_name("f1_l1_v1", file_ending="mkv") == Path(
        "images/cameras/f1_l1_v1.mkv"
    )

    assert get_path_from_camera_name("f1_l1_v1", folder=Path()) == Path("f1_l1_v1.jpg")
