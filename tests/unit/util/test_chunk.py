from __future__ import annotations

from itertools import chain
from typing import Sequence

import pytest
from sand.util import chunk


@pytest.fixture()
def chunk_list() -> Sequence[int]:
    return [1, 2, 3, 4, 5]


def test_chunk_should_return_full_range_if_gpus_is_lower_than_zero(
    chunk_list: Sequence[int],
) -> None:
    result = chunk(chunk_list, chunk_count=-1, chunk_index=-1)

    assert list(result) == [0, 1, 2, 3, 4]


def test_chunk_should_return_full_range_if_gpus_is_one(
    chunk_list: Sequence[int],
) -> None:
    result = chunk(chunk_list, chunk_count=1, chunk_index=-1)

    assert list(result) == [0, 1, 2, 3, 4]


def test_chunk_should_return_first_half_range_if_gpu_index_is_zero_and_two_gpus(
    chunk_list: Sequence[int],
) -> None:
    result = chunk(chunk_list, chunk_count=2, chunk_index=0)

    assert list(result) == [0, 1]


def test_chunk_should_return_second_half_range_if_gpu_index_is_one_and_two_gpus(
    chunk_list: Sequence[int],
) -> None:
    result = chunk(chunk_list, chunk_count=2, chunk_index=1)

    assert list(result) == [2, 3, 4]


def test_chunk_should_combine_to_full_range_if_combined_for_all_indexes(
    chunk_list: Sequence[int],
) -> None:
    first_half = chunk(chunk_list, chunk_count=2, chunk_index=0)
    second_half = chunk(chunk_list, chunk_count=2, chunk_index=1)

    assert list(chain(first_half, second_half)) == [0, 1, 2, 3, 4]


def test_chunk_should_give_correct_ranges_for_every_index() -> None:
    chunk_list = [1, 2, 3, 4]

    # we get an Iterable so only one list is possible, afterwards the iterator is exhausted
    first_range = list(chunk(chunk_list, chunk_count=4, chunk_index=0))
    second_range = list(chunk(chunk_list, chunk_count=4, chunk_index=1))
    third_range = list(chunk(chunk_list, chunk_count=4, chunk_index=2))
    fourth_range = list(chunk(chunk_list, chunk_count=4, chunk_index=3))

    assert first_range == [0]
    assert second_range == [1]
    assert third_range == [2]
    assert fourth_range == [3]

    assert list(
        chain(
            first_range,
            second_range,
            third_range,
            fourth_range,
        )
    ) == [0, 1, 2, 3]


def test_chunk_should_raise_value_error_if_count_is_higher_than_length() -> None:
    with pytest.raises(
        ValueError,
        match="chunk_count=10 cannot be higher than camera_count=3",
    ):
        chunk([1, 2, 3], chunk_count=10, chunk_index=0)


def test_chunk_should_handle_uneven_chunks_correctly(chunk_list: Sequence[int]) -> None:
    first_range = list(chunk(chunk_list, chunk_count=4, chunk_index=0))
    second_range = list(chunk(chunk_list, chunk_count=4, chunk_index=1))
    third_range = list(chunk(chunk_list, chunk_count=4, chunk_index=2))
    fourth_range = list(chunk(chunk_list, chunk_count=4, chunk_index=3))

    assert first_range == [0]
    assert second_range == [1]
    assert third_range == [2]
    assert fourth_range == [3, 4]

    assert list(
        chain(
            first_range,
            second_range,
            third_range,
            fourth_range,
        )
    ) == [0, 1, 2, 3, 4]
