from __future__ import annotations

from unittest.mock import MagicMock

import pytest
from sand.config import LidarConfig, SandConfig
from sand.reader.lidar.pipeline.collector import Vlp16Collector


@pytest.fixture()
def lidar_name() -> str:
    return "lidar"


@pytest.fixture()
def lidar_config(lidar_name: str) -> LidarConfig:
    return LidarConfig(name=lidar_name, active=True)


@pytest.fixture()
def sand_config(lidar_config: LidarConfig) -> SandConfig:
    return SandConfig(lidars=[lidar_config])


def test_collector_starts_a_worker_thread_in_init(
    thread_constructor_mock: MagicMock,
    lidar_name: str,
    sand_config: SandConfig,
) -> None:
    reader = MagicMock()
    assert not thread_constructor_mock.return_value.start.called
    Vlp16Collector(reader, lidar_name, sand_config)
    assert thread_constructor_mock.return_value.start.called


def test_collector_does_not_start_a_worker_thread_in_init_if_inactive(
    thread_constructor_mock: MagicMock,
    lidar_name: str,
    sand_config: SandConfig,
    lidar_config: LidarConfig,
) -> None:
    reader = MagicMock()
    lidar_config.active = False

    assert not thread_constructor_mock.return_value.start.called
    Vlp16Collector(reader, lidar_name, sand_config)
    assert not thread_constructor_mock.return_value.start.called


def test_collector_subscribes_reader_in_init(
    lidar_name: str,
    sand_config: SandConfig,
) -> None:
    reader = MagicMock()
    assert not reader.subscribe.called
    Vlp16Collector(reader, lidar_name, sand_config)
    assert reader.subscribe.called


def test_collector_packet_queue(
    lidar_name: str,
    sand_config: SandConfig,
) -> None:
    reader = MagicMock()
    packet = MagicMock()
    sut = Vlp16Collector(reader, lidar_name, sand_config)
    assert sut.queue.qsize() == 0
    sut.push_packet(packet)
    assert sut.queue.qsize() == 1
