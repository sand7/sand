from __future__ import annotations

from datetime import datetime, timedelta
from unittest.mock import MagicMock, call, patch

import numpy as np
import pytest
from cv2 import CAP_PROP_BUFFERSIZE
from sand.config import CameraConfig, SandConfig
from sand.interfaces.shutdown import ShutdownAble
from sand.reader.video.reader import CameraReader


@pytest.fixture()
def config(camera_name: str) -> SandConfig:
    return SandConfig(
        cameras=[
            CameraConfig(
                name=camera_name,
                stream="rtsp://f1_l1_v1.automodal.de/axis-media/media.amp?resolution=2560x1440",
            )
        ]
    )


def test_reader_should_be_shutdown_able(config: SandConfig, camera_name: str) -> None:
    sut = CameraReader(config, camera_name, playback=False)

    assert isinstance(sut, ShutdownAble)


@patch("sand.interfaces.shutdown.shutdown_able.Event")
def test_reader_creates_shutdown_event(
    event_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    assert not event_class_mock.called

    CameraReader(config, camera_name, playback=False)

    assert event_class_mock.called


def test_reader_creates_worker_thread(
    thread_constructor_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    assert not thread_constructor_mock.called

    sut = CameraReader(config, camera_name, playback=False)

    assert thread_constructor_mock.call_count == 1
    thread_constructor_mock.assert_has_calls(
        [
            call(
                target=sut._open_camera,
                args=(),
                name=f"r_{config.cameras[0].name}",
                daemon=False,
            )
        ]
    )


def test_reader_does_not_start_worker_in_initializer(
    thread_constructor_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    assert not thread_constructor_mock.return_value.start.called

    CameraReader(config, camera_name, playback=False)

    assert not thread_constructor_mock.return_value.start.called


def test_start_starts_the_worker_thread(
    thread_constructor_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    sut = CameraReader(config, camera_name, playback=False)

    assert not thread_constructor_mock.return_value.start.called

    sut.start()

    assert thread_constructor_mock.return_value.start.call_count == 1


def test_subscribe_adds_object_to_subscriber_list(
    config: SandConfig, camera_name: str
) -> None:
    subscriber = MagicMock()
    sut = CameraReader(config, camera_name, playback=False)

    assert len(sut.subscribers) == 0

    sut.subscribe(subscriber)

    assert len(sut.subscribers) == 1
    assert sut.subscribers[0] is subscriber


@patch("sand.interfaces.shutdown.shutdown_able.Event")
def test_shutdown_sets_shutdown_event(
    event_class_mock: MagicMock,
    thread_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    thread_mock.is_alive.return_value = True
    shutdown_event = event_class_mock.return_value

    sut = CameraReader(config, camera_name, playback=False)

    assert not shutdown_event.set.called

    sut.shutdown()

    assert shutdown_event.set.called


def test_shutdown_joins_worker_thread_if_alive(
    config: SandConfig,
    camera_name: str,
    thread_mock: MagicMock,
) -> None:
    thread_mock.is_alive.return_value = True

    sut = CameraReader(config, camera_name, playback=False)

    assert not thread_mock.join.called

    sut.shutdown()

    assert thread_mock.join.called


def test_shutdown_does_not_join_worker_thread_if_not_alive(
    thread_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    thread_mock.is_alive.return_value = False

    sut = CameraReader(config, camera_name, playback=False)

    assert not thread_mock.join.called

    sut.shutdown()

    assert not thread_mock.join.called


@patch("sand.reader.video.reader.VideoCapture")
def test_record_does_nothing_when_shutdown_immediately(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = True

    assert not video_capture_class_mock.called

    sut._open_camera()

    assert not video_capture_class_mock.called


@patch("sand.reader.video.reader.VideoCapture")
def test_opens_camera_stream(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._record_stream = MagicMock()
    sut._record_stream.return_value = True, True

    assert not video_capture_class_mock.called

    sut._open_camera()

    assert video_capture_class_mock.called
    video_capture_class_mock.assert_has_calls([call(config.cameras[0].stream)])


@patch("sand.reader.video.reader.VideoCapture")
def test_tries_to_create_stream_again_when_not_open(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, True]
    sut._record_stream = MagicMock()
    video_capture_class_mock.return_value.isOpened.return_value = False

    assert not video_capture_class_mock.called
    assert not video_capture_class_mock.return_value.isOpened.called

    sut._open_camera()

    assert video_capture_class_mock.return_value.isOpened.call_count == 2
    assert video_capture_class_mock.call_count == 2
    video_capture_class_mock.assert_has_calls(
        [
            call(config.cameras[0].stream),
            call().isOpened(),
            call(config.cameras[0].stream),
            call().isOpened(),
        ],
        any_order=True,
    )


@patch("sand.reader.video.reader.VideoCapture")
def test_waits_one_second_between_two_tries_via_shutdown_event(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, True]
    sut._record_stream = MagicMock()
    video_capture_class_mock.return_value.isOpened.return_value = False

    assert not video_capture_class_mock.called
    assert not sut.shutdown_event.called

    sut._open_camera()

    assert video_capture_class_mock.call_count == 2
    assert sut.shutdown_event.wait.call_count == 2
    sut.shutdown_event.wait.assert_has_calls([call(1), call(1)])


@patch("sand.reader.video.reader.VideoCapture")
def test_sets_stream_buffer_size_on_capture_to_one(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    capture = video_capture_class_mock.return_value
    capture.isOpened.return_value = True

    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._record_stream = MagicMock()
    sut._record_stream.return_value = True, True

    assert not capture.set.called

    sut._open_camera()

    assert capture.set.called
    capture.set.assert_has_calls([call(CAP_PROP_BUFFERSIZE, 1)])


@patch("sand.reader.video.reader.VideoCapture")
def test_records_opened_stream(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    capture = video_capture_class_mock.return_value
    capture.isOpened.return_value = True

    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._record_stream = MagicMock()
    sut._record_stream.return_value = True, True

    assert not sut._record_stream.called

    sut._open_camera()

    assert sut._record_stream.called
    sut._record_stream.assert_has_calls([call(capture)])


@patch("sand.reader.video.reader.VideoCapture")
def test_increases_dropped_frames_if_stream_closes_unexpectedly(
    video_capture_class_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    capture = video_capture_class_mock.return_value
    capture.isOpened.return_value = True

    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._record_stream = MagicMock()
    sut._record_stream.return_value = False, False

    assert sut._stats.dropped_frames == 0

    sut._open_camera()

    assert sut._stats.dropped_frames == 1


def test_logs_metrics_after_recording(config: SandConfig, camera_name: str) -> None:
    sut = CameraReader(config, camera_name, playback=False)
    sut._stats = MagicMock()
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = True

    assert not sut._stats.log_metric.called

    sut._open_camera()

    assert sut._stats.log_metric.called


@patch("sand.reader.video.reader.now")
def test_adds_correct_logs_when_recording(
    now_mock: MagicMock, config: SandConfig, camera_name: str
) -> None:
    now_mock.return_value = datetime(1970, 1, 1, 0, 0)
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut._should_log_intermediary_stats = MagicMock()
    sut._should_log_intermediary_stats.return_value = False
    sut._stats = MagicMock()
    sut._stats.time_for_one_frame = -1

    assert not sut._stats.add_grab_time.called
    assert not sut._stats.add_retrieve_time.called
    assert not sut._stats.add_frame.called
    assert sut._stats.time_for_one_frame == -1

    sut._record_stream(stream)

    assert sut._stats.add_grab_time.called
    assert sut._stats.add_retrieve_time.called
    assert sut._stats.add_frame.called
    assert sut._stats.time_for_one_frame == timedelta(0)


@patch("sand.reader.video.reader.now")
def test_logs_intermediary_stats_when_needed(
    now_mock: MagicMock, config: SandConfig, camera_name: str
) -> None:
    now_mock.return_value = datetime(1970, 1, 1, 0, 0)
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut._should_log_intermediary_stats = MagicMock()
    sut._should_log_intermediary_stats.return_value = True
    sut._stats = MagicMock()

    assert not sut._stats.log_statistics.called

    sut._record_stream(stream)

    assert sut._stats.log_statistics.call_count == 1


@patch("sand.reader.video.reader.now")
def test_does_not_log_intermediary_stats_when_not_needed(
    now_mock: MagicMock, config: SandConfig, camera_name: str
) -> None:
    now_mock.return_value = datetime(1970, 1, 1, 0, 0)
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut._should_log_intermediary_stats = MagicMock()
    sut._should_log_intermediary_stats.return_value = False
    sut._stats = MagicMock()

    assert not sut._stats.log_statistics.called

    sut._record_stream(stream)

    assert not sut._stats.log_statistics.called


def test_does_nothing_if_shutdown(
    config: SandConfig,
    camera_name: str,
) -> None:
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (True, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = True

    assert not stream.retrieve.called

    sut._record_stream(stream)

    assert not stream.retrieve.called


def test_returns_correct_value_when_shutdown(
    config: SandConfig, camera_name: str
) -> None:
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = True

    ret_val = sut._record_stream(stream)

    assert ret_val == (True, True)


def test_returns_error_value_when_stream_returns_false(
    config: SandConfig, camera_name: str
) -> None:
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)
    stream.grab.return_value = False

    sut = CameraReader(config, camera_name, playback=False)

    ret_val1, ret_val2 = sut._record_stream(stream)

    assert not ret_val1
    assert not ret_val2


def test_pushes_retrieved_frame_to_subscribers(
    config: SandConfig, camera_name: str
) -> None:
    subscriber = MagicMock()
    stream = MagicMock()
    frame = np.eye(3)
    stream.retrieve.side_effect = [(True, frame), (False, None)]

    sut = CameraReader(config, camera_name, playback=False)
    sut.subscribe(subscriber)

    assert not subscriber.push_image.called

    sut._record_stream(stream)

    assert subscriber.push_frame.call_count == 1


def test_should_not_push_to_subscribers_when_return_value_is_false(
    config: SandConfig,
    camera_name: str,
) -> None:
    subscriber = MagicMock()
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut.subscribe(subscriber)

    assert not subscriber.push.called

    sut._record_stream(stream)

    assert not subscriber.push.called


@patch("sand.reader.video.reader.now")
def test_logs_metric_and_sets_next_time(
    now_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    # y m d h m s
    now_mock.return_value = datetime(1970, 1, 1, 0, 0, 10)

    # log metric immediately next_metric is always < now()
    config.cameras[0].metric_interval = -10
    stream = MagicMock()
    frame = MagicMock()
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=False)
    sut._next_metric = 7
    sut._should_log_intermediary_stats = MagicMock()
    sut._should_log_intermediary_stats.return_value = False
    sut._stats = MagicMock()

    assert not sut._stats.log_metric.called
    assert sut._next_metric == 7

    sut._record_stream(stream)

    assert sut._stats.log_metric.called
    assert sut._next_metric == datetime(1970, 1, 1, 0, 0, 0)  # -10 secs from now_mock


@patch("sand.reader.video.reader.time")
@patch("sand.reader.video.reader.now")
def test_waits_between_two_frames_when_in_playback_mode(
    now_mock: MagicMock,
    time_mock: MagicMock,
    config: SandConfig,
    camera_name: str,
) -> None:
    # y m d h m s
    now_mock.return_value = datetime.fromtimestamp(0)
    time_mock.side_effect = [0, 1]

    config.cameras[0].fps = 1
    stream = MagicMock()
    frame = np.zeros((100, 100, 3), dtype=np.uint8)
    stream.retrieve.return_value = (False, frame)

    sut = CameraReader(config, camera_name, playback=True)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.return_value = False

    assert not sut.shutdown_event.wait.called
    assert not time_mock.called

    sut._record_stream(stream)

    assert time_mock.called
    assert sut.shutdown_event.wait.call_count == 1
