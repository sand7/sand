from __future__ import annotations

from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from sand.config import CameraConfig, SandConfig
from sand.reader.video import CameraSystem


@pytest.fixture()
def config() -> CameraConfig:
    return CameraConfig()


@pytest.fixture()
def sand_config() -> SandConfig:
    return SandConfig()


@pytest.fixture()
def reader_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.reader.video.camera_system.CameraReader")


@pytest.fixture()
def reader_mock(reader_class_mock: MagicMock) -> MagicMock:
    return reader_class_mock.return_value


@pytest.fixture()
def encoder_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("sand.reader.video.camera_system.FrameEncoder")


def test_camera_system_combines_enricher_and_reader_and_encoder(
    config: MagicMock,
    reader_class_mock: MagicMock,
    encoder_class_mock: MagicMock,
    sand_config: SandConfig,
) -> None:
    assert not reader_class_mock.called
    assert not encoder_class_mock.called
    CameraSystem(config, sand_config, playback=False)
    assert reader_class_mock.called
    assert encoder_class_mock.called


def test_camera_system_start_starts_reader(
    config: MagicMock,
    reader_mock: MagicMock,
    sand_config: SandConfig,
) -> None:
    sut = CameraSystem(config, sand_config, playback=False)

    assert not reader_mock.start.called
    sut.start()
    assert reader_mock.start.called
