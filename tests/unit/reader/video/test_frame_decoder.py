from __future__ import annotations

import contextlib
from datetime import datetime
from unittest.mock import MagicMock, call, patch

import pytest
from numpy import array, uint8, zeros
from numpy.testing import assert_array_equal
from sand.config import CameraConfig, CommunicationConfig
from sand.datatypes import EnrichedFrame
from sand.interfaces.synchronization import SandNode
from sand.reader.video import FrameDecoder


@pytest.fixture()
def config(camera_name: str) -> CameraConfig:
    return CameraConfig(name=camera_name)


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


@pytest.fixture()
def complex_enriched_frame(camera_name: str) -> EnrichedFrame:
    return EnrichedFrame(
        camera_name,
        datetime.utcfromtimestamp(7200),
        zeros((3, 3, 3), dtype=uint8),
        frame_id=42,
    )


def test_frame_decoder_calls_node_init(
    communication_config: CommunicationConfig,
    config: CameraConfig,
) -> None:
    with patch.object(SandNode, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            FrameDecoder(config, communication_config)

        assert init_mock.called


def test_frame_decoder_should_keep_track_of_subscribers(
    communication_config: CommunicationConfig, config: CameraConfig
) -> None:
    subscriber = MagicMock()

    sut = FrameDecoder(config, communication_config)

    assert len(sut.subscribers) == 0
    sut.subscribe(subscriber)
    assert len(sut.subscribers) == 1
    sut.subscribe(subscriber)
    assert len(sut.subscribers) == 2


def test_frame_decoder_should_create_thread_and_start_it(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
    camera_name: str,
) -> None:
    assert not thread_constructor_mock.called
    assert not thread_mock.start.called

    sut = FrameDecoder(config, communication_config)

    assert thread_constructor_mock.called
    thread_constructor_mock.assert_has_calls(
        [
            call(
                target=sut.decode_frames,
                args=(),
                daemon=False,
                name=f"fd_{camera_name}",
            )
        ]
    )

    assert thread_mock.start.called


def test_frame_decoder_should_get_existing_shared_memory_but_not_create_it(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_name: str,
    shared_decoder_mock: MagicMock,
) -> None:
    sut = FrameDecoder(config, communication_config)
    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, True]

    assert not shared_decoder_mock.called
    sut.decode_frames()
    assert shared_decoder_mock.call_count == 1
    shared_decoder_mock.assert_has_calls([call(name=camera_name, create=False)])


def test_frame_decoder_should_close_and_not_unlink_shared_memory_on_shutdown(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    shared_decoder_mock: MagicMock,
) -> None:
    sut = FrameDecoder(config, communication_config)

    sut.shutdown_event = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, True]
    sut.decode_frames()

    assert not shared_decoder_mock.return_value.unlink.called
    assert not shared_decoder_mock.return_value.close.called

    sut.shutdown()

    assert not shared_decoder_mock.return_value.unlink.called
    assert shared_decoder_mock.return_value.close.called


@patch("sand.interfaces.shutdown.shutdown_able.Event")
def test_frame_decoder_should_wait_for_shared_memory_if_not_exists_yet_and_waits_in_between(
    event_constructor_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    shared_decoder_mock: MagicMock,
) -> None:
    expected_count = 5
    expected = MagicMock()

    shared_decoder_mock.side_effect = [
        FileNotFoundError("failure"),
        FileNotFoundError("failure"),
        FileNotFoundError("failure"),
        FileNotFoundError("failure"),
        expected,
    ]
    shutdown_event = event_constructor_mock.return_value

    sut = FrameDecoder(config, communication_config)
    shutdown_event.is_set.side_effect = [*([False] * 10), True]

    assert not shared_decoder_mock.called
    assert not shutdown_event.wait.called

    sut.decode_frames()

    assert shared_decoder_mock.call_count == expected_count
    assert sut._shared_buffer is expected

    assert shutdown_event.wait.call_count == 4
    shutdown_event.wait.assert_has_calls(
        [
            call(5),
            call(5),
            call(5),
            call(5),
        ]
    )


def test_frame_decoder_should_decode_frame(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameDecoder(config, communication_config)
    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._shared_buffer = MagicMock()
    sut._shared_buffer.buf = bytearray(complex_enriched_frame.to_bytes())

    subscriber = MagicMock()
    sut.subscribe(subscriber)

    assert sut.cached_frame.id != complex_enriched_frame.id

    sut.decode_frames()

    assert subscriber.push_frame.called
    pushed_frame: EnrichedFrame = subscriber.push_frame.call_args_list[0][0][0]

    assert pushed_frame.id == complex_enriched_frame.id
    assert pushed_frame.camera_name == complex_enriched_frame.camera_name
    assert pushed_frame.timestamp == complex_enriched_frame.timestamp
    assert_array_equal(pushed_frame.frame, complex_enriched_frame.frame)


def test_frame_decoder_should_push_frame_to_subscriber(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameDecoder(config, communication_config)
    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._shared_buffer = MagicMock()
    sut._shared_buffer.buf = bytearray(complex_enriched_frame.to_bytes())

    subscriber_one = MagicMock()
    subscriber_two = MagicMock()
    sut.subscribe(subscriber_one)
    sut.subscribe(subscriber_two)

    assert not subscriber_one.push_frame.called
    assert not subscriber_two.push_frame.called

    sut.decode_frames()

    assert subscriber_one.push_frame.called
    assert subscriber_two.push_frame.called


@patch("sand.reader.video.decoder.time")
def test_frame_decoder_should_push_error_frame_when_it_times_out(
    time_mock: MagicMock,
    communication_config: CommunicationConfig,
    config: CameraConfig,
    shared_decoder_mock: MagicMock,
) -> None:
    time_mock.side_effect = [0, 30, 30]
    sut = FrameDecoder(config, communication_config)
    sut.shutdown_event.is_set = MagicMock()
    sut.shutdown_event.is_set.side_effect = [False, False, True]
    shared_decoder_mock.side_effect = FileNotFoundError

    subscriber_one = MagicMock()
    sut.subscribe(subscriber_one)

    assert not subscriber_one.push_frame.called

    sut.decode_frames()

    assert subscriber_one.push_frame.call_count == 1
    error_image: EnrichedFrame = subscriber_one.push_frame.call_args_list[0][0][0]
    expected_color = array([0, 0, 255])
    assert (error_image.frame[:][:] == expected_color).all()
