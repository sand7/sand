from __future__ import annotations

import contextlib
from datetime import datetime
from unittest.mock import MagicMock, call, patch

import pytest
from numpy import uint8, zeros
from sand.config import CameraConfig, CommunicationConfig
from sand.datatypes import Dimensions, EnrichedFrame
from sand.interfaces.synchronization import SandNode
from sand.reader.video import FrameEncoder


@pytest.fixture()
def resolution() -> Dimensions:
    return Dimensions(2560, 1440)


@pytest.fixture()
def complex_enriched_frame(camera_name: str, resolution: Dimensions) -> EnrichedFrame:
    return EnrichedFrame(
        camera_name,
        datetime.utcfromtimestamp(7200),
        zeros((resolution.height, resolution.width, 3), dtype=uint8),
        frame_id=42,
    )


@pytest.fixture()
def config(camera_name: str, resolution: Dimensions) -> CameraConfig:
    return CameraConfig(
        name=camera_name,
        stream_resolution_str=f"{resolution.width}x{resolution.height}",
    )


@pytest.fixture()
def communication_config() -> CommunicationConfig:
    return CommunicationConfig()


@pytest.fixture()
def camera_reader() -> MagicMock:
    return MagicMock()


def test_encoder_calls_sand_node(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
) -> None:
    with patch.object(SandNode, "__init__") as init_mock:
        with contextlib.suppress(Exception):
            FrameEncoder(config, communication_config, camera_reader)

        assert init_mock.called


def test_encoder_should_unlink_shared_memory_on_shutdown(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    shared_encoder_mock: MagicMock,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)
    sut.push_frame(complex_enriched_frame)

    assert shared_encoder_mock.called
    assert not shared_encoder_mock.return_value.unlink.called

    sut.shutdown()

    assert shared_encoder_mock.return_value.unlink.called


def test_encoder_should_try_to_unlink_shared_memory_but_be_resilient_against_file_not_found_errors(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    shared_encoder_mock: MagicMock,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)
    sut.push_frame(complex_enriched_frame)

    assert shared_encoder_mock.called
    assert not shared_encoder_mock.return_value.unlink.called

    sut.shutdown()

    assert shared_encoder_mock.return_value.unlink.called


def test_encoder_should_subscribe_reader(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
) -> None:
    assert not camera_reader.subscribe.called

    sut = FrameEncoder(config, communication_config, camera_reader)

    assert camera_reader.subscribe.called
    camera_reader.subscribe.assert_has_calls([call(sut)])


def test_encoder_should_create_a_worker_thread_but_not_start_it(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_name: str,
    camera_reader: MagicMock,
    thread_constructor_mock: MagicMock,
    thread_mock: MagicMock,
) -> None:
    assert not thread_constructor_mock.called
    assert not thread_mock.start.called

    sut = FrameEncoder(config, communication_config, camera_reader)

    assert thread_constructor_mock.called
    thread_constructor_mock.assert_has_calls(
        [
            call(
                target=sut.encode_frames,
                args=(),
                name=f"fe_{camera_name}",
                daemon=False,
            )
        ]
    )
    assert not thread_mock.start.called


def test_encoder_should_not_do_anything_if_no_frame_in_queue(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)
    sut.shutdown_event.is_set = MagicMock()

    sut.shutdown_event.is_set.side_effect = [False, True]

    sut._shared_buffer = MagicMock()
    expected = sut._shared_buffer.buf[:]

    assert len(sut.queue) == 0

    sut.encode_frames()

    assert len(sut.queue) == 0
    assert sut._shared_buffer.buf[:] == expected


def test_encoder_should_set_buffer_to_byte_frame(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)
    sut.shutdown_event.is_set = MagicMock()

    sut.shutdown_event.is_set.side_effect = [False, True]
    sut._shared_buffer = MagicMock()
    sut._shared_buffer.buf = bytearray(1000)  # don't care about exact size

    sut.push_frame(complex_enriched_frame)

    assert sut._shared_buffer.buf != complex_enriched_frame.to_bytes()

    sut.encode_frames()

    assert sut._shared_buffer.buf == complex_enriched_frame.to_bytes()


def test_encoder_should_not_initialize_shared_memory_immediately(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    shared_encoder_mock: MagicMock,
) -> None:
    assert not shared_encoder_mock.called

    FrameEncoder(config, communication_config, camera_reader)

    assert not shared_encoder_mock.called


def test_encoder_should_initialize_shared_memory_when_first_frame_gets_pushed(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    shared_encoder_mock: MagicMock,
    complex_enriched_frame: EnrichedFrame,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)

    assert not shared_encoder_mock.called

    sut.push_frame(complex_enriched_frame)

    assert shared_encoder_mock.called


def test_encoder_should_initialize_shared_memory_with_correct_size(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    shared_encoder_mock: MagicMock,
    complex_enriched_frame: EnrichedFrame,
    resolution: Dimensions,
    camera_name: str,
) -> None:
    color_count = 3
    frame_offset = 60
    expected_size = resolution.height * resolution.width * color_count + frame_offset

    sut = FrameEncoder(config, communication_config, camera_reader)

    assert not shared_encoder_mock.called

    sut.push_frame(complex_enriched_frame)

    assert shared_encoder_mock.called
    shared_encoder_mock.assert_called_with(
        name=camera_name,
        create=True,
        size=expected_size,
    )


def test_encoder_should_start_thread_on_first_push(
    communication_config: CommunicationConfig,
    config: CameraConfig,
    camera_reader: MagicMock,
    complex_enriched_frame: EnrichedFrame,
    thread_mock: MagicMock,
) -> None:
    sut = FrameEncoder(config, communication_config, camera_reader)

    assert not thread_mock.start.called

    sut.push_frame(complex_enriched_frame)

    assert thread_mock.start.called
