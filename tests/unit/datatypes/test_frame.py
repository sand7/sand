from __future__ import annotations

from datetime import datetime
from pathlib import Path

import pytest
from cv2 import imread
from numpy.testing import assert_array_equal
from sand.datatypes import EnrichedFrame, Image


@pytest.fixture()
def image_path() -> Path:
    return (
        Path(__file__)
        .parent.parent.parent.parent.joinpath(
            "config/example/calibration/camera/f1_l3_v1.jpg"
        )
        .absolute()
    )


@pytest.fixture()
def image(image_path: Path) -> Image:
    return imread(image_path.as_posix())


@pytest.fixture()
def timestamp() -> datetime:
    return datetime.utcfromtimestamp(7200)


def test_frame_should_transform_to_correct_round_trip(
    image: Image,
    timestamp: datetime,
) -> None:
    expected_camera_name = "camera_name"
    expected_timestamp = timestamp
    expected_frame_id = 42
    expected_frame = image

    sut = EnrichedFrame(
        expected_camera_name,
        expected_timestamp,
        expected_frame,
        frame_id=expected_frame_id,
    )

    result = EnrichedFrame.from_bytes(memoryview(sut.to_bytes()))

    assert result.id == expected_frame_id
    assert result.camera_name == expected_camera_name
    assert result.timestamp == expected_timestamp
    assert_array_equal(result.frame, expected_frame)
