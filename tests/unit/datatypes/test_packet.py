from __future__ import annotations

from datetime import datetime

import pytest
from sand.config import LidarConfig
from sand.datatypes import LidarPacket


@pytest.fixture()
def config() -> LidarConfig:
    return LidarConfig()


@pytest.fixture()
def lidar_packet(config: LidarConfig) -> LidarPacket:
    return LidarPacket(
        config.name,  # hack to make util.time work not with negative things
        b"asdf" * 300 + b"\x00\x00\x00\x00\x00\x00",
    )


def test_lidar_packet(lidar_packet: LidarPacket) -> None:
    # unix time 0
    assert lidar_packet.timestamp_in_bytes == b"\x00\x00\x00\x00\x00\x00"
    assert lidar_packet.lidar_name == "lidar"
    assert lidar_packet.timestamp == datetime(1970, 1, 1, 0, 0)
    assert lidar_packet.packet == b"asdf" * 300 + b"\x00\x00\x00\x00\x00\x00"
