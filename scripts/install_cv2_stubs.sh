#!/bin/bash

if [[ $1 == "pyright" ]]; then
    # pyright wants it in root/typings
    target_folder="typings/cv2/"
    mkdir -p typings/cv2/

    # additional files
    curl -sSL https://raw.githubusercontent.com/microsoft/python-type-stubs/main/stubs/cv2-stubs/py.typed \
        -o "$target_folder/py.typed"
    curl -sSL https://github.com/microsoft/python-type-stubs/blob/main/stubs/cv2-stubs/Error.pyi \
        -o "$target_folder/Error.pyi"

    main_file="$target_folder/__init__.pyi"
else
    target_folder="$(poetry run python -c "import cv2, os; print(os.path.dirname(cv2.__file__))")"
    main_file="$target_folder/cv2.pyi"
fi

echo "Adding stubs to cv2 at path: $target_folder"

curl -sSL https://raw.githubusercontent.com/microsoft/python-type-stubs/main/stubs/cv2-stubs/__init__.pyi \
    -o "$main_file"
