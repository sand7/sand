#!/usr/bin/env bash

MMCV_FULL_VERSION="$(poetry show mmcv-full | grep version | awk '{print $3}')"
NUMPY_VERSION="$(poetry show numpy | grep version | awk '{print $3}')"

# NOTE: Install numpy in advance with correct version, because the install
#       of mmcv-full alone could install numpy in a wrong version

set -e

poetry run python -m \
    pip install \
        --find-links https://download.openmmlab.com/mmcv/dist/cu111/torch1.9.1/index.html \
        mmcv-full=="$MMCV_FULL_VERSION" \
        numpy=="$NUMPY_VERSION"

poetry install -E full

poetry run python -m \
    pip install \
        --find-links https://download.pytorch.org/whl/torch_stable.html \
        torch==1.9.1+cu111 \
        torchvision==0.10.1+cu111
