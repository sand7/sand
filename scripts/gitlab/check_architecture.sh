#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NORMAL='\033[0m'

allowedSign="# allowed"
mainModuleFolder="src/sand/"

elementIn() {
    local e match="$1"
    shift
    for e; do [[ $e == "$match" ]] && return 0; done
    return 1
}

beautifyResults() {
    echo "$allResults" |
        sort |
        uniq |
        sed '/^$/d' |
        sed "s|^|$mainModuleFolder|"
}

allowedResults() {
    allResults="$1"
    echo "$allResults" | grep -E "(from|import) sand\..*${allowedSign}\$"
}

disallowedResults() {
    allResults="$1"
    echo "$allResults" | grep -v -E "(from|import) sand\..*${allowedSign}\$"
}

checkResults() {
    allResults="$1"

    [ -z "$(disallowedResults "$allResults")" ] && return 0 || return 1
}

pushd "$mainModuleFolder" >/dev/null 2>&1 || exit 1

# modules that are allowed anywhere
# should have no dependencies externally or internally
skipModules=(
    "config/"
    "datatypes/"
    "interfaces/"
    "logger/"
    "registry/"
    "util/"
)
modules=(*/)

allResults=""
for module in "${modules[@]}"; do
    if [[ $module == "__pycache__/" ]]; then continue; fi
    if elementIn "$module" "${skipModules[@]}"; then continue; fi

    moduleName="$(basename "$module")"
    moduleResults="$(grep -nRE "^(from|import) sand\.$moduleName" ./*/)"
    realResults="$(echo "$moduleResults" | grep -v "^$moduleName/")"
    relativeResults="$(grep -nRE "^(from|import) \.\." ./*/)"

    allResults="$allResults
$relativeResults
$realResults"
done
popd >/dev/null 2>&1 || exit 1

beautifiedResults="$(beautifyResults "$allResults")"

echo -e "${GREEN}\nAllowed Results:\n$(allowedResults "$beautifiedResults")${NORMAL}\n"
if checkResults "$beautifiedResults"; then
    echo -e "${GREEN}All good!${NORMAL}"
    exit 0
else
    echo -e "${RED}Found disallowed things:
$(disallowedResults "$beautifiedResults")

To determine if it's okay for those, think about if there is something that could break if not all
dependencies/extras are there, especially 'neural' things
${NORMAL}"

    exit 1
fi
