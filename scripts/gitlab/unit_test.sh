#!/usr/bin/env bash

# designed to be run from project-root

python -m pytest \
    -v \
    --cov-branch \
    --cov-report=term \
    --cov-report=html \
    --cov-report=xml \
    --junitxml xunit-reports/xunit-result-unit.xml \
    --cov=src \
    tests/unit "$@"
