#!/usr/bin/env bash

# designed to be run from project-root

python -m pytest \
    -v \
    --junitxml xunit-reports/xunit-result-integration.xml \
    tests/integration "$@"
