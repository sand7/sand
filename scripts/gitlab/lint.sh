#!/usr/bin/env bash

# designed to be run from project-root

EXIT_CODE=0

echo -e "\n########## source ##########\n"

ruff src || EXIT_CODE=$?

echo -e "\n########## tests  ##########\n"

# the lintings in tests are less important
ruff tests

exit "$EXIT_CODE"
