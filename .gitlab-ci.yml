---
variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

default:
    image: registry.gitlab.com/sand7/sand

    before_script: &default_before_script
        - poetry install --all-extras

cache: &global_cache
    paths:
        - .venv
        - .cache/pip
    policy: pull

workflow:
    rules:
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        - if: $CI_COMMIT_BRANCH == "main"
        - if: $CI_COMMIT_TAG

black:
    image: pyfound/black
    before_script: []
    script:
        - black --check --diff */
    cache: []
    needs: []

install:
    cache:
        <<: *global_cache
        policy: pull-push
    before_script: []
    script: *default_before_script
    needs: []
    rules:
        - changes:
            - poetry.lock
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        - if: $CI_COMMIT_BRANCH == "main"
        - if: $CI_COMMIT_TAG

poetry:
    cache: []
    before_script: []
    script:
        - poetry lock --check
    needs: []

typing:
    script:
        - poetry run mypy
    cache:
        - *global_cache
        - key: MYPY_CACHE
          paths:
            - .mypy_cache
    needs:
        - job: install
          optional: true

lint:
    script:
        - poetry run scripts/gitlab/lint.sh
    needs:
        - job: install
          optional: true

test:
    script:
        - poetry run scripts/gitlab/unit_test.sh
        - poetry run coverage xml
    cache:
        - *global_cache
        - key: PYTEST_CACHE
          paths:
            - .pytest_cache
    artifacts:
        reports:
            junit: xunit-reports/xunit-result-unit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml
        expire_in: 1 hour
    needs:
        - job: install
          optional: true

build:
    script:
        - poetry build
    artifacts:
        paths:
            - dist
        expire_in: 1 days
    needs:
        - poetry
        - typing
        - test
        - job: install
          optional: true

release:
    rules:
        - if: $CI_COMMIT_TAG
    before_script:
        - *default_before_script
        - poetry run pip install twine
    script:
        - poetry run twine upload dist/* --username __token__ --password $PYPI_TOKEN
          --verbose
    needs:
        - build
